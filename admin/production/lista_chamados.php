<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lista de Chamados </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
     <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
     <script>
       function deletarChamado(chamadoId){
         $.ajax({
           type: "post",
           url: "../../back/api/api.php",
           data: {
             type: 'deletar-chamado',
             chamado_id: chamadoId
           },
           success: function (response) {
             var res = JSON.parse(response);
              if(res['error']){
                swal('ops',res['msg'], 'error');
                return;
              }else{
                swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  location.reload();
                }
              });
              }
            },error: function (err){
              swal('Erro','Verifique sua conexão','error');
              return;
            }
         });
       }
     </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Lista de Chamados</span></a>
            </div>

            <div class="clearfix"></div>
             <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
          <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              </div>
              <div class="col-md-6">
			    <div class="input-group h2">
                  <input name="data[search]" class="form-control" id="search" type="text" placeholder="Pesquisar Itens">
                   <span class="input-group-btn">
                     <button class="btn btn-primary" type="submit">
                       <span class="glyphicon glyphicon-search"></span>
                     </button>
                   </span>
                </div>
              </div>
			  <div class="col-md-3">
               <a href="novo_chamado.php" class="btn btn-primary pull-right h2">Novo chamado</a>
              </div>
            </div>

			<div class="clearfix"></div>

			<div id="list" class="row">
				<div class="table-responsive col-md-12">
					<table class="table table-striped" cellspacing="0" cellpadding="0">
					  <thead>
						<tr>
							<th>Chamado</th>
							<th>Local</th>
							<th>Equipe Trabalhando</th>
							<th>Situação</th>
							<th class="actions">Ações</th>
						</tr>
					  </thead>
					  <tbody id="lista_de_chamados">
              <?php 
                $chamado = new Chamado();
                $proprio = new Proprio();
                $equipe = new Equipe();
                $equipe_propria = new Equipe_propria();
                $equipes = $equipe->listarTrabalhandoNoMomento();
                $chamados = $chamado->listarTudo();
                for($i = 0; $i < count($chamados); $i++){
              ?>
                <tr>
                  <td><?= $chamados[$i]['id']; ?></td>
                  <td><?= $proprio->listar($chamados[$i]['proprio_id'])['nome']; ?></td>
                  <td><?= $equipe_propria->procurarChamado($chamados[$i]['id']) ? $equipe->listar($equipe_propria->listarPorChamado($chamados[$i]['id'])['equipe_id'])['nome'] : '...'; ?></td>
                  <td><?= verSituacao($chamados[$i]['situacao']); ?></td>
                  <td class="actions">
                  <a class="btn btn-success btn-xs" href="visu_chamado.php?chamado_id=<?= $chamados[$i]['id']; ?>">Visualizar</a>
                  <?php if($chamados[$i]["situacao"] != 3 && $chamados[$i]["situacao"] != 4) { ?><a class="btn btn-warning btn-xs" href="editar_chamado.php?chamado_id=<?= $chamados[$i]['id']; ?>">Editar</a><?php } ?>
                  <a class="btn btn-danger btn-xs"  href="#" data-toggle="modal" data-target="#delete-modal-<?= $chamados[$i]['id']; ?>">Excluir</a>
                  </td>
                </tr>
                <!-- Modal -->
                      <div class="modal fade" id="delete-modal-<?= $chamados[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
                          </div>
                          <div class="modal-body">
                              Deseja realmente excluir este item?
                          </div>
                          <div class="modal-footer">
                          <button type="button" class="btn btn-primary" onclick="deletarChamado('<?= $chamados[$i]['id']; ?>');">Sim</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                          </div>
                        </div>
                      </div>
                      </div> 
                <!-- /.modal -->
              <?php } ?>
					  </tbody>
					</table>
				</div>
			</div>
	
		  </div>
        </div>
      </div>
    </div>
       
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

        
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>