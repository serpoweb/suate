<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>

<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inicio </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col  menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><span>Início</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
              <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- tópicos superiores -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count"><center>
              <span class="count_top"><i class="fa fa-user"></i>Chamados Totais</span>
              <div class="count" id="c_tot"></div></center>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count"><center>
              <span class="count_top"><i class="fa fa-clock-o"></i> Chamados Atendidos</span>
              <div class="count" id="c_a"></div></center>   
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count"><center>
              <span class="count_top"><i class="fa fa-user"></i> Equipe(s) trabalhando</span>
              <div class="count" id="e_t"></div></center>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count"><center>
              <span class="count_top"><i class="fa fa-user"></i> Manutenções próximas</span>
              <div class="count" id="m_p"></div></center>
            </div>
          </div>
          <!-- fim tópicos superiores -->
         
          <br />
          <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" style="background: #33adff !important;">
                      <h2>Mapa</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="dashboard-widget-content">
                        <div>
                          <div id="map" class="col-md-8 col-sm-12 col-xs-12" style="height:500px; width: 100%" ></div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="row">
            <!-- fim do card mapa -->
          <!-- API Google Maps -->
         <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdyPhBNG8vxfr5lFv_AT0lwKslCVIDNws&callback=initMap" type="text/javascript"></script>
        <!-- JavaScript Mapa -->  
    <script src="js/mapa.js" type = "text/javascript"></script>

            <!-- começo do card de equipes-->
          <div class="col-md-4 col-sm-6">
                <div class="x_panel">
                  <div class="x_title" style="background: #22a911 !important;">
                    <h2>Equipe(s) trabalhando no momento</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <article class="media event" id="card_de_equipes_no_momento"></article>
                   </div>
                </div>
          </div>
            
        <!-- fim do card de equipes -->
      <!-- começo do card de alertas -->      
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="x_panel">
                  <div class="x_title" style="background: #e82c45 !important;">
                    <h2> Chamados</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <?php 
                    $c = new Chamado();
                    $p = new Proprio();
                    $alertas = $c->alertas();
                    $manut_prox = $c->manutencoesProximas();
                    $manut_prox_2 = $c->manutencoesProximasCorretiva();
//                     var_dump($manut_prox);
                    for($i=0;$i<count($alertas);$i++){
                  ?>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month"><?= strftime("%b", strtotime($alertas[$i]['data_abertura'])); ?></p>
                        <p class="day"><?= strftime("%d", strtotime($alertas[$i]['data_abertura'])); ?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?= $p->listar($alertas[$i]['proprio_id'])['nome']; ?></a>
                        <p><?= $alertas[$i]['atividade']; ?></p>
                      </div>
                    </article>
                    <?php } ?>
                   </div>
                </div>
              </div>
      <!-- fim do card de alertas-->
      <!-- começo do card de manutenções-->
              <div class="row">
			 <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" style="background: #14a3ea !important;">
                    <h2>Manutenções Próximas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <h4 style="color:#000;"> Próximas manutenções preventivas </h4>
                    <?php for($j=0;$j< count($manut_prox);$j++){ ?>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month"><?= strftime("%b", strtotime($manut_prox[$j]['data_abertura'])); ?></p>
                        <p class="day"><?= strftime("%d", strtotime($manut_prox[$j]['data_abertura'])); ?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"> <?= $manut_prox[$j]['atividade']; ?> </a>
                        <p><?= $p->listar($manut_prox[$j]['proprio_id'])['nome']; ?></p>
                      </div>
                    </article>
                    <?php } ?>
                  </div>
                </div>
              </div>
			  </div>
              <div class="row">
			          <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" style="background: #eac514 !important;">
                    <h2>Manutenções Próximas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <h4 style="color:#000;"> Próximas manutenções corretivas </h4>
                    <?php for($j=0;$j<count($manut_prox_2);$j++){ ?>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month"><?= strftime("%b", strtotime($manut_prox_2[$j]['data_abertura'])); ?></p>
                        <p class="day"><?= strftime("%d", strtotime($manut_prox_2[$j]['data_abertura'])); ?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"> <?= $manut_prox_2[$j]['atividade']; ?> </a>
                        <p><?= $p->listar($manut_prox_2[$j]['proprio_id'])['nome']; ?></p>
                      </div>
                    </article>
                    <?php } ?>
                  </div>
                </div>
              </div>
                   <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="color:#000;">Demanda de Chamados</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id="echart_pie" style="height:350px;"></div>

                  </div>
                </div>
              </div>
              
             

             
			  </div>
              <div class="row">
                 <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2 style="color:#000;">Acompanhamento</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div id="echart_donut" style="height: 350px; -webkit-tap-highlight-color: transparent; user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;" _echarts_instance_="1539662504088"><div style="position: relative; overflow: hidden; width: 300px; height: 350px;"><div data-zr-dom-id="bg" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none;"></div><canvas width="300" height="350" data-zr-dom-id="0" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas width="300" height="350" data-zr-dom-id="1" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas width="300" height="350" data-zr-dom-id="_zrender_hover_" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.5); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font-family: Arial, Verdana, sans-serif; padding: 5px; left: 100px; top: 123px;">Access to the resource <br>Search Engine : 1,548 (60.42%)</div></div></div>

                </div>
              </div>
            </div>
                 <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2 style="color:#000;">Ocorrências</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div id="echart_pie2" style="height: 350px; -webkit-tap-highlight-color: transparent; user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;" _echarts_instance_="1539659150793"><div style="position: relative; overflow: hidden; width: 300px; height: 350px;"><div data-zr-dom-id="bg" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none;"></div><canvas width="300" height="350" data-zr-dom-id="0" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas width="300" height="350" data-zr-dom-id="1" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas width="300" height="350" data-zr-dom-id="_zrender_hover_" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 300px; height: 350px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.5); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font-family: Arial, Verdana, sans-serif; padding: 5px; left: 98px; top: 164px;">Area Mode <br>rose5 : 20 (18.18%)</div></div></div>

                </div>
              </div>
            </div>
              </div>
          <!-- fim do card de manutenções -->
        
        </div>
         <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
	</div>
	</div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
     <script src="../vendors/echarts/dist/echarts.min.js"></script>
    <script src="../vendors/echarts/map/js/world.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script>
      $(() => {
        $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: {
            type: 'todas-as-informacoes-estatisticas'
          },
          success: function (response) {
             console.log(response);
              var res = JSON.parse(response);
              if(res['error']){
                swal('ops',res['msg'], 'error');
                return;
              }else{
                // swal('Sucesso',res['msg'],'success');
                $("#c_tot").html(res['estatisticas']['ocorrencias_totais']);
                $("#c_a").html(res['estatisticas']['ocorrencias_atendidas']);
                $("#m_p").html(res['estatisticas']['manutencoes_proximas']);
              }
            },error: function (err){
              swal('Erro','Verifique sua conexão','error');
              return;
            }
        });
        $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: {
            type: 'listar-equipes-trabalhando-no-momento'
          },
          success: function (response) {
             console.log(response);
              var res = JSON.parse(response);
              if(res['error']){
                //swal('ops',res['msg'], 'error');
                return;
              }else{
                // swal('Sucesso',res['msg'],'success');
                $("#e_t").html(res['equipes'].length);
                for(var i = 0; i < res['equipes'].length; i++){
                  $("#card_de_equipes_no_momento").append('<div class="media-body"><a class="title" href="equipe_obras.php">'+res['equipes'][i]['nome']+'</a></div>');
                }
              }
            },error: function (err){
              swal('Erro','Verifique sua conexão','error');
              return;
            }
        });
          
      });
    </script>
  </body>
</html>