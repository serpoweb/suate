<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Editar Chamado </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a class="site_title"><span>Editar Chamado</span></a>
            </div>

                <div class="clearfix"></div>
             <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
        <!-- top navigation -->
        <?php // require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
             <div class="page-title">
              <div class="title_left">
              </div>  
             </div>
			  <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Chamado </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate id="edicao_de_chamado">
                   
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Situação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php //var_dump($c); ?>
                         <select class="form-control col-md-7 col-xs-12" required="required" id="chamado_situacao">
            
                           <?php listarTodasAsSituacoes(); ?>
                         </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="datetime">Data de abertura <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="datetime" name="data_a"placeholder="informe a data de abertura" required="required" class="form-control col-md-7 col-xs-12" id="chamado_data_de_abertura">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="datetime">Data de finalização
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="datetime" id="data_f" placeholder="informe a data de finalização (se já finalizado)" class="form-control col-md-7 col-xs-12" id="chamado_data_de_finalizacao">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Classe de Manutenção
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control col-md-7 col-xs-12" id="chamado_classe_de_manutencao">
                         <?php listarTodasAsClassManut(); ?>
                         </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="periodicidade"> Periodicidade 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number"  name="periodicidade"  class="form-control col-md-7 col-xs-12" id="chamado_periodicidade">
                        </div>
                      </div>
					              <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Atividade(s)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input  type="text" name="atividade"  class="form-control col-md-7 col-xs-12" id="chamado_atividade">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Observações
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="observacoes"  class="form-control col-md-7 col-xs-12"  id="chamado_observacao">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="proprio">Próprio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select  class="form-control col-md-7 col-xs-12" required="required" id="chamado_proprio">
                             <?php //listarTodosOsPropriosPorCategoria($pro->pegarSecretaria($c['proprio_id'])['id']); ?>
                             <?php if(isset($_GET["proprio_id"]) && $_GET["proprio_id"] != ""){ $p = new Proprio(); $z = $p->listar($_GET["proprio_id"]); echo '<option id="'.$z["id"].'">'.$z["nome"].'</option>'; } else{ listarTodosOsProprios(); } ?>
                             
                           </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="text" class="control-label col-md-3">Palavra Chave <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select  class="form-control col-md-7 col-xs-12" required="required" id="chamado_palavra_chave">
                            <?php if(isset($_GET["palavra_chave_id"]) && $_GET["palavra_chave_id"] != ""){ $p = new Proprio(); $z = $p->listarPalavraChave($_GET["palavra_chave_id"]); echo '<option id="'.$z["id"].'">'.$z["descricao"].'</option>'; } else{  listarTodasPalavrasChave(); }?>
                           </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button class="btn bg-red" type="button" onclick="window.location.href = 'lista_chamados.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
			 
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>
      $(function () {
        $("#edicao_de_chamado").submit((e)=>{
          e.preventDefault();
          var ocorrencia_redirect = <?= isset($_GET["ocorrencia_redirect"]) && $_GET["ocorrencia_redirect"] != "" ? "true" : "false"; ?>;
          var OCORRENCIA_I = <?= isset($_GET["ocorrencia_redirect"]) && $_GET["ocorrencia_redirect"] != "" ? $_GET["ocorrencia_redirect"] : "null"; ?>;
          var situacao = $("#chamado_situacao").find(':selected').attr('id');
          var data_abertura = $("#chamado_data_de_abertura").val();
          var data_finalizacao = $("#chamado_data_de_finalizacao").val();
          var classeManutencao = $("#chamado_classe_de_manutencao").find(':selected').attr('id');
          var periodicidade = $("#chamado_periodicidade").val();
          var atividade = $("#chamado_atividade").val();
          var observacao = $("#chamado_observacao").val();
          var proprio = $("#chamado_proprio").find(':selected').attr('id');
          var palavraChave = $("#chamado_palavra_chave").find(':selected').attr('id');
          var data_f = $("#data_f").val();
          var post = {
            type: 'abrir-chamado',
            situacao: situacao,
            data_abertura: data_abertura,
            classe: classeManutencao,
            periodicidade: periodicidade,
            atividade: atividade,
            observacoes: observacao,
            proprio_id: proprio,
            palavra_chave_id: palavraChave,
            data_finalizacao: data_f
          };
          console.log(post);
          
          $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            
            var res = JSON.parse(response);
            console.log(res);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  if(ocorrencia_redirect){
                    $.ajax({
                      type: "post",
                      url: "../../back/api/api.php",
                      data: {type:'atrelar-ocorrencia-ao-chamado','chamado': res.novo_chamado_id, 'ocorrencia': OCORRENCIA_I},
                      success: function (response) {
                        console.log(response);
                      }
                    });
                    location.href = 'atrelar-equipe.php?chamado_redirect='+res.novo_chamado_id;
                  }else{
                    location.href = 'lista_chamados.php';
                  }
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
        });
      });
    </script>
  </body>
</html>