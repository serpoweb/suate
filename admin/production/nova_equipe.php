<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nova Equipe </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
         <div class="left_col scroll-view">
			<div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Visualizar Contrato</span></a>
            </div>

                <div class="clearfix"></div>
             <!-- menu profile quick info -->
          <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
          <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
             <div class="page-title">
              <div class="title_left">
              </div>  
             </div>
			  <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="color:#000 !important;">Nova Equipe </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate id="nova_equipe">
                      

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="equipe_nome" class="form-control col-md-7 col-xs-12"  name="data" required="required" type="text">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user">Usuário 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select  class="form-control col-md-7 col-xs-12" id="equipe_usuario">
                           <?php $e = new Usuarios(); $ee = $e->listarTudo(); for($i=0;$i<count($ee);$i++){ ?>
                            <option id="<?= $ee[$i]['id']; ?>"><?= $ee[$i]['nome']; ?></option>
                           <?php } ?>
                         </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red" onclick="location.href = 'equipe_obras.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
			
					 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="color:#000 !important;">Novo Trabalhador </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate id="novo_trabalhador">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="nt_name" class="form-control col-md-7 col-xs-12"  name="data" required="required" type="text">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nt_cpf" class="form-control col-md-7 col-xs-12"  required="required">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ddd">DDD
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="nt_ddd" type="text" name="ddd" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telefone
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="nt_tel" type="text" name="phone" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="equipe">Equipe 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="nt_equipe" class="form-control col-md-7 col-xs-12" >
                                <?php $e = new Equipe(); $ee = $e->listarTudo(); for($i=0;$i<count($ee);$i++){ ?>
                                    <option id="<?= $ee[$i]['id']; ?>"><?= $ee[$i]['nome']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red"  onclick="location.href = 'equipe_obras.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
			 
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>
      $(function () {
        $("#nova_equipe").submit(function (e) { 
          e.preventDefault();
          var nome = $("#equipe_nome").val();
          var user = $("#equipe_usuario").find(':selected').attr('id');
          if(nome.trim().length == 0){
            swal('Atenção','Preencha o nome da Equipe','info'); return;
          }
          var post = { type: 'adicionar-equipe', equipe_nome:nome, user_id:user};
           $.ajax({
            type: "post",
            url: "../../back/api/api.php",
            data: post,
            success: function (response) {
              console.log(response);
              var res = JSON.parse(response);
              if(res['error']){
                swal('ops',res['msg'], 'error');
                return;
              }else{
                swal({
                  title: 'Sucesso',
                  text: res['msg'],
                  type: 'success',
                  onClose: () =>{
                    location.href = 'equipe_obras.php';
                  }
                });
              }
            },error: function (err){
              swal('Erro','Verifique sua conexão','error');
              return;
            }
         });
        });
//         $("#nova_equipe_propria").submit(function (e) {
//           e.preventDefault();
//           var nep_name = $("#nep_name").val();
//           var nep_user = $("#nep_user").find(':selected').attr('id');
//           var nep_date = $("#nep_date").val();
//           var nep_obs = $("#nep_obs").val();
//           var nep_chamado = $("#nep_chamado").find(':selected').attr('id');
//           var nep_equipe = $("#nep_equipe").find(':selected').attr('id');
          
//           if(nep_name.trim().length == 0){
//             swal('Atenção','Preencha o nome da equipe','info'); return;
//           }
//           if(nep_date.trim().length == 0){
//             swal('Atenção','Digite uma data válida!','info'); return;
//           }
//           var post = { type: 'adicionar-equipe-propria', "chamado_id": nep_chamado, "equipe_id":nep_equipe, "observacoes": nep_obs, "data": nep_date};
//            $.ajax({
//             type: "post",
//             url: "../../back/api/api.php",
//             data: post,
//             success: function (response) {
//               console.log(response);
//               var res = JSON.parse(response);
//               if(res['error']){
//                 swal('ops',res['msg'], 'error');
//                 return;
//               }else{
//                 swal({
//                   title: 'Sucesso',
//                   text: res['msg'],
//                   type: 'success',
//                   onClose: () =>{
//                     location.href = 'equipe_obras.php';
//                   }
//                 });
//               }
//             },error: function (err){
//               swal('Erro','Verifique sua conexão','error');
//               return;
//             }
//          });
//         });
        $("#novo_trabalhador").submit(function (e) {
          e.preventDefault();
          var nt_name = $("#nt_name").val();
          var nt_cpf = $("#nt_cpf").val();
          var nt_ddd = $("#nt_ddd").val();
          var nt_tel = $("#nt_tel").val();
          var nt_equipe = $("#nt_equipe").find(':selected').attr('id');
          if(nt_name.trim().length == 0){
            swal('Atenção','Preencha o nome da equipe','info'); return;
          }
          if(nt_cpf.trim().length == 0){
            swal('Atenção','Preencha o CPF','info'); return;
          }
          
          var post = { type: 'adicionar-trabalhador', "nome": nt_name, "cpf": nt_cpf, "telefone": nt_tel, "ddd": nt_ddd, "equipe_id": nt_equipe};
           $.ajax({
            type: "post",
            url: "../../back/api/api.php",
            data: post,
            success: function (response) {
              console.log(response);
              var res = JSON.parse(response);
              if(res['error']){
                swal('ops',res['msg'], 'error');
                return;
              }else{
                swal({
                  title: 'Sucesso',
                  text: res['msg'],
                  type: 'success',
                  onClose: () =>{
                    location.href = 'equipe_obras.php';
                  }
                });
              }
            },error: function (err){
              swal('Erro','Verifique sua conexão','error');
              return;
            }
         });
        });
      });
    </script>
  </body>
</html>
  