<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Visualizar Contrato </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
		<div class="left_col scroll-view">
			<div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Visualizar Contrato</span></a>
            </div>

                <div class="clearfix"></div>
             <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
          <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       <?php //require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
             <div class="page-title">
              <div class="title_left">
              </div>  
             </div>
			  <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Visualizar Contrato </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" id='editar_contrato'>
                      <?php 
                        $c = new Contrato();
                        $c_id = $_GET['contrato_id'] ?? null;
                        $con = $c->listar($c_id);
                        $ch = new Chamado();
                        $chamado = $ch->listar($con['chamado_id']);
                        $tr = new Tercerizado();
                        $tercerizada = $tr->listar($con["terceirizada_id"]);
                        
                      ?>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="datetime">Data de Contratação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['data_contratacao']; ?>" id="contrato_data_contratacao" class="form-control col-md-7 col-xs-12"  name="data" required="required" type="datetime">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor principal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['valor_principal']; ?>" id="contrato_valor_principal" type="number" name="valor_prin" placeholder="informe o valor do contrato" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor 1
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['valor1']; ?>" id="contrato_valor_1"  type="number" name="valor1" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor 2
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['valor2']; ?>" type="number" id="contrato_valor_2" name="valor2" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor 3
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['valor3']; ?>" type="number" id="contrato_valor_3" name="valor3" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor 4
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" value="<?= $con['valor4']; ?>" name="valor4" id="contrato_valor_4" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Valor 5
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" value="<?= $con['valor5']; ?>" name="valor5" id="contrato_valor_5" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Observações
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?= $con['observacoes']; ?>" type="text" name="observacoes" id="contrato_observacoes"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Chamado <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select  class="form-control col-md-7 col-xs-12" required="required" id="contrato_chamado">
                             <option id="<?= $chamado['id']; ?>"><?= $chamado['id']; ?></option>
                             <?php $chamaad = $ch->listarTudo(); for($i = 0; $i < count($chamaad);$i++){ ?>
                              <option id="<?= $chamaad[$i]['id']; ?>"><?= $chamaad[$i]['id']; ?></option>
                             <?php } ?>
                           </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="text" class="control-label col-md-3">Terceirizada
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select  class="form-control col-md-7 col-xs-12" required="required" id="contrato_tercerizada">
                            <option id="<?= $tercerizada['id']; ?>"><?= $tercerizada['nome']; ?></option>
                            <?php $terc = $tr->listarTudo(); for($i=0;$i<count($terc);$i++){ ?>
                              <option id="<?= $terc[$i]['id']; ?>"><?= $terc[$i]['nome']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red" onclick="window.location.href='contratos.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
			 
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script>
      $(function () {
        $("#editar_contrato").submit(function (e) { 
          e.preventDefault();
          var post = {
            type: 'atualizar-contrato',
            contrato_id: <?= $c_id; ?>,
            valor_principal: $("#contrato_valor_principal").val(),
            valor1: $("#contrato_valor_1").val(),
            valor2: $("#contrato_valor_2").val(),
            valor3: $("#contrato_valor_3").val(),
            valor4: $("#contrato_valor_4").val(),
            valor5: $("#contrato_valor_5").val(),
            observacoes: $("#contrato_observacoes").val(),
            chamado_id: $("#contrato_chamado").find(':selected').attr('id'),
            tercerizada_id: $("#contrato_tercerizada").find(':selected').attr('id')
          };
           $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  location.reload();
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
        });
      });
    </script>
  </body>
</html>