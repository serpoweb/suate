<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nova Secretaria </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
     <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Nova Secretaria</span></a>
            </div>

                <div class="clearfix"></div>
          <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
           <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

     
        <!-- top navigation -->
       <?php require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
             <div class="page-title">
              <div class="title_left">
              </div>  
             </div>
			  <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="color:#000;">Novo Tercerizado </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate id="terc-form">
                      

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tercerizado">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tercerizado-nome" class="form-control col-md-7 col-xs-12"  name="tercerizado" required="required" type="text">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cnpj">CNPJ<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tercerizado-cnpj" type="text" name="cnpj"  required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ddd">DDD
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tercerizado-ddd" type="text" name="ddd"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone"> Telefone
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tercerizado-telefone" type="text" name="phone"   class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="tercerizado-email" type="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user">Usuário
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control col-md-7 col-xs-12" id="tercerizado-usuario">
                            <?php $u = new Usuarios(); $us = $u->listarTudo(); for($i=0,$tam=count($us); $i<$tam;$i++){ ?>
                            <option id="<?=$us[$i]["id"]; ?>"><?=$us[$i]["nome"]; ?></option>
                           <?php } ?>
                        </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red" onclick="window.location.href = 'terceirizados.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
			 
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script>
      $(document).ready(()=>{
        $("#terc-form").submit((e)=>{
          e.preventDefault();
          var nome = $("#tercerizado-nome").val().trim(),
              cnpj = $("#tercerizado-cnpj").val().trim(),
              ddd = $("#tercerizado-ddd").val().trim(),
              telefone = $("#tercerizado-telefone").val().trim(),
              usuario_id = $("#tercerizado-usuario").find(':selected').attr('id');
          if(nome.length == 0 || cnpj.length == 0 || ddd.length == 0 || telefone.length == 0){
            swal('Ops','Preencha todos os campos','info');
            return;
          }
          var post = {
            type: 'adicionar-tercerizado',
            nome : nome,
            cnpj:cnpj,
            ddd : ddd,
            telefone:telefone,
            usuario_id: usuario_id
          };
          $.ajax({
            url: '../../back/api/api.php',
            method: 'post',
            data: post,
            success:(e)=>{
              var data = JSON.parse(e);
              if(data.error){
                swal("Ops",data.msg,'error');
                return;
              }else{
                swal({
                  type : 'success',
                  title : 'Sucesso',
                  text: data.msg,
                  onClose: ()=>{
                    location.href = 'terceirizados.php';
                    return;
                  }
                });
              }
            }
          });
        });
      });
    </script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>