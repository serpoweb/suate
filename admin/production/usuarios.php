<?php
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null;
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Usuários </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
    <script>
      function deletarUsuario(id){
         $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: {type: 'deletar-usuario', usuario_id: id},
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  location.reload();
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
      }
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
             <a class="site_title"> <span>Usuários</span></a>
            </div>

            <div class="clearfix"></div>
  <!-- menu profile quick info -->
          <div class="profile clearfix">

              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />

        <!-- sidebar menu -->
                <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">

            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->

        <!-- page content -->
       <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              </div>
              <div class="col-md-6">
			    <div class="input-group h2">
                  <input name="data[search]" class="form-control" id="search" type="text" placeholder="Pesquisar Itens">
                   <span class="input-group-btn">
                     <button class="btn btn-primary" type="submit">
                       <span class="glyphicon glyphicon-search"></span>
                     </button>
                   </span>
                </div>
              </div>
			  <div class="col-md-3">
               <a href="novo_usuario.php" class="btn btn-primary pull-right h2">Novo usuário</a>
              </div>
            </div>

			<div class="clearfix"></div>
			<div id="list" class="row">
				<div class="table-responsive col-md-12">
					<table class="table table-striped" cellspacing="0" cellpadding="0">
					  <thead>
						<tr>
							<th>Nome</th>
							<th>CPF</th>
							<th>DDD</th>
							<th>Telefone</th>
							<th class="actions">Ações</th>
						</tr>
					  </thead>
					  <tbody>
              <?php $u = new Usuarios(); $usu = $u->listarTudo(); for($i=0;$i<count($usu);$i++){ ?>
						<tr>
							<td><?= $usu[$i]['nome']; ?></td>
							<td><?= $usu[$i]['cpf']; ?></td>
							<td><?= $usu[$i]['ddd']; ?></td>
							<td><?= $usu[$i]['telefone']; ?></td>
							<td class="actions">
							<a class="btn btn-success btn-xs" href="visu_usuarios.php?usuario_id=<?= $usu[$i]['id'] ?>">Visualizar</a>
							<a class="btn btn-warning btn-xs" href="edit_usuarios.php?usuario_id=<?= $usu[$i]['id'] ?>">Editar</a>
							<a class="btn btn-danger btn-xs"  href="#" data-toggle="modal" data-target="#delete-modal-<?= $usu[$i]['id'] ?>">Excluir</a>
							</td>
              <!-- Modal -->
                    <div class="modal fade" id="delete-modal-<?= $usu[$i]['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
                        </div>
                        <div class="modal-body">
                            Deseja realmente excluir este item?
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="deletarUsuario(<?= $usu[$i]['id']; ?>);">Sim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                        </div>
                      </div>
                    </div>
                    </div>
              <!-- /.modal -->
						</tr>
              <?php } ?>
					  </tbody>
					</table>
				</div>
			</div>
		<!-- /#list -->
		    <!-- <div id="bottom" class="row">
				<div class="col-md-12">

					<ul class="pagination">
						<li class="disabled"><a>&lt; Anterior</a></li>
						<li class="disabled"><a>1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li class="next"><a href="#" rel="next">Próximo &gt;</a></li>
					</ul>

				</div>
			</div>  -->
			<!-- /#bottom -->
          </div>
        </div>
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">

          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>



    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>
