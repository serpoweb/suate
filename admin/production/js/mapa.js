var map;
var markers = [];
var styleMap = [

    {
        featureType: 'administrative.province',
        //elementType: 'label.text',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'transit.station.bus',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'transit.station.airport',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'administrative.locality',
       // elementType: 'label.text',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'poi',
        stylers: [{
            visibility: 'off'
        }]
    },
];

function icone(class_manut, situacao){
    
    if(situacao == "1"){
        switch(class_manut){
            case "1":
                return "periodica";
                break;  
            case "2":
                return "corretiva";
                break;
            case "3":
                return "imprevisivel";
                break;
            
                      
        }
    }
}

function CenterControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '4px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click para voltar a posição original';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Centralizar';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function() {
      initMap();
    });

  }





function initMap() {

    var Sorocaba = new google.maps.LatLng(-23.465000, -47.461012);


    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        minZoom: 12,
        center: Sorocaba,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        disableDefaultUI: true,
        styles: styleMap


    });

        // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(centerControlDiv);
    


    $.getJSON('mapa_php/api_json.php', function (pontos) {
        console.log(pontos);

        $.each(pontos, function (index, ponto) {

            var marker = new google.maps.Marker({
                zIndex: parseInt(ponto.id),
                position: new google.maps.LatLng(ponto.lat, ponto.long),
                title: ponto.nome,
                clickable: true,
                icon: {
                    url: "makers/" + ponto.abreviatura +"/" + icone(ponto.class_manut, ponto.situacao)  +".png",
                    scaledSize: new google.maps.Size(28, 36)
                },
                map: map
            });

           
            markers.push(marker);
        });


    });



}