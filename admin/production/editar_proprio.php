<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Editar Próprio </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
      <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Visualizar Próprio</span></a>
            </div>

                <div class="clearfix"></div>
             <!-- menu profile quick info -->
            <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
           <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       <?php //require_once(ROOT.'/utilidades/top-menu.php'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
             <div class="page-title">
              <div class="title_left">
              </div>  
             </div>
			  <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Visualizar Próprio </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate id="edit_proprio_form">
                      <?php 
                        $p_id = $_GET["proprio_id"] ?? null;
                      ?>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_nome" class="form-control col-md-7 col-xs-12"  name="Nome" required="required" type="text">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="endereco">Endereço 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_endereco" type="text" name="endereco" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Número
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_numero" type="text" name="number" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bairro">Bairro
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_bairro" type="text" name="bairro" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="horario">Horário de Abertura
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_horario_abertura" type="text" name="horario" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="horario_f">Horário de Fechamento
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_horario_fechamento" type="text" name="horario_f"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ddd"> DDD 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_ddd" type="text"  name="ddd"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telefone
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_telefone" type="text" name="telefone"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="latitude">Latitude
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_latitude" type="number" name="latitude"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="longitude">Longitude 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_longitude" type="number" name="longitude"  class=" form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3" for="pavimentos">Pavimentos
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_pavimentos" type="number" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="data_c">Data da Construção
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_data_contrucao" type="datetime" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="area_c">Area Construída
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_area_contruida" type="number" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="validade">Validade avcb
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_validade_avcb" type="datetime" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="publico">Público Alvo
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_publico_alvo" type="text" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="bene_m">Beneficiários na parte da manhã
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_benif_manha" type="number" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					    <div class="item form-group">
                        <label class="control-label col-md-3" for="bene_t">Beneficiários na parte da tarde
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_benif_tarde" type="number" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					    <div class="item form-group">
                        <label class="control-label col-md-3" for="bene_n">Beneficiários na parte noite
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="proprio_benif_noite" type="number" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					    <div class="item form-group">
                        <label class="control-label col-md-3" for="responsavel">Responsável Local
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control col-md-7 col-xs-12" id="proprio_usuario_id">
                            <?php $p = new Proprio(); $pr = $p->listar($p_id); $u = new Usuarios(); $user = $u->listar($pr['usuario_id']); $users = $u->listarTudo(); ?>
                            <option id="<?= $user['id']; ?>"><?= $user['nome']; ?></option>
                            <?php for($l = 0; $l < count($users); $l++){ ?>
                              <option id="<?= $users[$l]['id']; ?>"><?= $users[$l]['nome']; ?></option>
                            <?php } ?>
                        </select>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="cidade">Cidade
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control col-md-7 col-xs-12" id="proprio_cidade_id">
                            <?php $c = new Cidade(); $cit = $c->listarTudo(); $cc = $c->listar($pr['cidade_id']); ?>
                            <option id="<?= $cc['id']; ?>"><?= $cc['nome']; ?></option>
                            <?php for($p=0; $p < count($cit); $p++){ ?>
                              <option id="<?= $cit[$p]['id'] ?>"><?= $cit[$p]['nome'] ?></option>
                            <?php } ?>
                        </select>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3" for="secretaria">Secretaria
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control col-md-7 col-xs-12" id="proprio_secretaria_id">
                           <?php 
                            $s = new Secretaria();
                            $sec = $s->listar($pr['secretaria_id']);
                            $secr = $s->listarTudo();
                           ?>
                           <option id="<?= $sec['id']; ?>"><?= $sec['nome']; ?></option>
                           <?php for($e=0; $e < count($secr); $e++){ ?>
                              <option id="<?= $secr[$e]['id']; ?>"><?= $secr[$e]['nome']; ?></option>
                           <?php } ?>
						</select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red" onclick="window.location.href='lista_proprios.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Salvar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
			 
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>

       $(function () {
        $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: {
            type: 'listar-proprio',
            proprio_id: <?= $p_id; ?>
          },
          success: function (response) {
            // console.log(response);
            var res = JSON.parse(response);
            console.log(res);
            
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              // swal({
              //   title: 'Sucesso',
              //   text: res['msg'],
              //   type: 'success'
              // });
              $("#proprio_nome").val(res.proprio['nome']);
              $("#proprio_endereco").val(res.proprio['endereco']);
              $("#proprio_numero").val(res.proprio['numero']);
              $("#proprio_bairro").val(res.proprio['bairro']);
              $("#proprio_horario_abertura").val(res.proprio['horario_abert']);
              $("#proprio_horario_fechamento").val(res.proprio['horario_fech']);
              $("#proprio_ddd").val(res.proprio['ddd']);
              $("#proprio_telefone").val(res.proprio['telefone']);
              $("#proprio_latitude").val(res.proprio['latitude']);
              $("#proprio_longitude").val(res.proprio['longitude']);
              $("#proprio_pavimentos").val(res.proprio['pavimentos']);
              $("#proprio_data_construcao").val(res.proprio['data_construcao']);
              $("#proprio_area_contruida").val(res.proprio['area_const']);
              $("#proprio_validade_avcb").val(res.proprio['validade_avcb']);
              $("#proprio_publico_alvo").val(res.proprio['publico_alvo']);
              $("#proprio_benif_manha").val(res.proprio['benif_manha']);
              $("#proprio_benif_tarde").val(res.proprio['benif_tarde']);
              $("#proprio_benif_noite").val(res.proprio['benif_noite']);
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
        $("#edit_proprio_form").submit(function (e) { 
          e.preventDefault();
          var post = {
            type : 'editar-proprio',
            proprio_id: <?= $p_id; ?>,
              nome: $("#proprio_nome").val(),
              endereco: $("#proprio_endereco").val(),
              numero: $("#proprio_numero").val(),
              bairro: $("#proprio_bairro").val(),
              horario_abert: $("#proprio_horario_abertura").val(),
              horario_fech: $("#proprio_horario_fechamento").val(),
              ddd: $("#proprio_ddd").val(),
              telefone: $("#proprio_telefone").val(),
              latitude: $("#proprio_latitude").val(),
              longitude: $("#proprio_longitude").val(),
              pavimentos: $("#proprio_pavimentos").val(),
              data_construcao: $("#proprio_data_construcao").val(),
              area_const: $("#proprio_area_contruida").val(),
              validade_avcb: $("#proprio_validade_avcb").val(),
              publico_alvo: $("#proprio_publico_alvo").val(),
              benif_manha: $("#proprio_benif_manha").val(),
              benif_tarde: $("#proprio_benif_tarde").val(),
              benif_noite: $("#proprio_benif_noite").val(),
              usuario_responsavel: $("#proprio_usuario_id").find(':selected').attr('id'),
              cidade_id: $("#proprio_cidade_id").find(':selected').attr('id'),
              secretaria_id: $("#proprio_secretaria_id").find(':selected').attr('id')
          }; 
           $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  location.href = 'lista_proprios.php';
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
        });
       });
    </script>
  </body>
</html>
</html>