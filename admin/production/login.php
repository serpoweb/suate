<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  isset($_SESSION['logado']) || @$_SESSION['logado'] == true ? header('Location: index.php') : null; 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="uft-8">
    <meta name="viewport" content="width=device-width, inicial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="login/css/style.css">
    <link rel="stylesheet" type="text/css" href="login/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
</head>
<body>
    <div class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
            </div>
            <div class="item">
            </div>
            <div class="item">
            </div>
        </div>
    </div>
    <div class="serpo">
        <form id="admin-login-form">
            <img src="images/logo.png" style="max-width: 160px;">
            <div class="login">
                <img src="login/PSD/ic_account_grey600_48dp.png">
                <input type="text" placeholder="Usuário" id="email-admin">
            </div>
            <br>
            <div class="login">
                <img src="login/PSD/ic_key_grey600_48dp.png">
                <input type="password" placeholder="Senha" id="senha-admin">
            </div>
            <br>
            <div class="botao">
                <button type="submit">Acessar a minha conta</button>
            </div>
        </form>
        <br>
    </div>    
</body>
</html>
<script src="login/js/bootstrap.js"></script>
<script src="login/js/jquery-1.11.1.min.js"></script>
<script>
    var n = 0;
    function trocaImg(){
        if(n%2==0)
            $(".serpo").css("background","url(login/PSD/back02.jpg)");
        else
            $(".serpo").css("background","url(login/PSD/back_03.jpg)");
        n++;
    }

        setInterval(trocaImg,2500);
</script>
   <script>
      $("#admin-login-form").submit((e)=>{
        e.preventDefault();
        var email = $("#email-admin").val();
        var senha = $("#senha-admin").val();
        if(email.trim().length == 0 || senha.trim().length == 0){
          swal('Atenção','Preencha todos os campos','info');
          return;
        }
        var post = {
          type: 'admin-login',
          email: email,
          senha:senha
        };
        $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: ()=>{
                  location.reload();
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
      });
    </script>