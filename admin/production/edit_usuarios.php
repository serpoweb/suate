<?php 
  require_once(dirname(dirname(dirname(__FILE__))).'/back/api/api.php');
  !isset($_SESSION['logado']) || $_SESSION['logado'] == false ? header('Location: login.php') : null; 
?>
<!DOCTYPE html>

<html lang="pt">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Editar Usuário</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <?php require_once(ROOT.'/utilidades/html-import.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a class="site_title"><span>Editar Usuário</span></a>
            </div>

            <div class="clearfix"></div>

             <!-- menu profile quick info -->
         <div class="profile clearfix">
             
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?= $_SESSION["usuario_nome"]; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
          <?php require_once(ROOT.'/utilidades/left-menu.php'); ?>
            <!-- /sidebar menu -->
			<!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              </div>
       
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Usuário </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" id="editar_usuario">
                      <span class="section">Editar usuário</span>
                      <?php $u = new Usuarios(); $usu = $u->listar($_GET["usuario_id"] ?? null); ?>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="usuario_nome" value="<?= $usu['nome']; ?>" class="form-control col-md-7 col-xs-12" name="Nome"  required="required" type="text">
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="usuario_cpf" value="<?= $usu['cpf']; ?>" name="cpf" required="required" d class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="email" id="usuario_email" value="<?= $usu['email']; ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Telefone <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="usuario_telefone"  value="<?= $usu['telefone']; ?>" name="numero" required="required" d class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
						<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ddd">DDD
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="usuario_ddd" value="<?= $usu['ddd']; ?>" name="ddd"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo"> Tipo
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select  class="form-control col-md-7 col-xs-12" id="usuario_tipo">
                               <option id="<?= $usu['tipo']; ?>"><?= verTipoDeUsuario($usu['tipo']); ?></option>
                               <?php listarTodosOsTiposDeUsuarios(); ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Senha antiga<span class="required">*</span>
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="usuario_senha_antiga" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Nova senha<span class="required">*</span>
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="usuario_nova_senha" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>
					   <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12"> Confirme a senha<span class="required">*</span>
						</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="usuario_confirmar_nova_senha" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="button" class="btn bg-red" onclick="window.location.href='usuarios.php';">Cancelar</button>
                          <button id="send" type="submit" class="btn btn-success">Cadastrar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
		  <!-- footer content -->
        <footer>
          <div class="pull-right">
 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="../vendors/validator/validator.js"></script>

    <!-- Custom Theme Scripts -->
    <!-- <script src="../build/js/custom.min.js"></script> -->
    <script>
      $(function () {
        $("#editar_usuario").submit((e) =>{ 
          e.preventDefault();
          var nome = $("#usuario_nome").val();    
          var cpf = $("#usuario_cpf").val();    
          var email = $("#usuario_email").val();    
          var telefone = $("#usuario_telefone").val();    
          var ddd = $("#usuario_ddd").val();    
          var tipo = $("#usuario_tipo").find(':selected').attr('id');    
          var senhaAntiga = $("#usuario_senha_antiga").val();    
          var novasenha = $("#usuario_nova_senha").val();    
          var confirmarnovasenha = $("#usuario_confirmar_nova_senha").val();    
          if(novasenha != confirmarnovasenha){
            swal('Atenção','Suas senhas não correspondem','info');
            return;
          }
          if(senhaAntiga.trim().length != 0 && novasenha.trim().length != 0 && confirmarnovasenha.trim().length != 0 && novasenha.trim() == confirmarnovasenha.trim()){
                    $.ajax({
                      type: "post",
                      url: "../../back/api/api.php",
                      data: {
                        type: 'solicitar-troca-de-senha',
                        senha: senhaAntiga,
                        nova_senha: novasenha,
                        usuario_id: <?= $_GET["usuario_id"] ?? null; ?>
                      },
                      success: function (response) {
                        var res = JSON.parse(response);
                        if(res.error){
                          swal('Erro',res.msg,'error');
                        }else{
                          swal({
                            title: 'Sucesso',
                            text: res.msg,
                            type: 'success',
                            onClose: ()=>{
                              location.href = 'usuarios.php';
                            }
                          });
                        }
                      }
                    });
                    return;
                  }
          var post = {
            type: 'atualizar-administrador',
            usuario_id: <?= $_GET["usuario_id"] ?? null; ?>,
            nome: nome,
            cpf:cpf,
            email: email,
            ddd:ddd,
            telefone:telefone,
              tipo:tipo
          };
         
           $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });
        
        });
        
      });
    </script>
  </body>
</html>