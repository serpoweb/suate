<?php


require("conn.php");

// Seleciona todos os proprios do banco
$query = "SELECT 
proprios.nome, 
proprios.id,
proprios.endereco,
proprios.latitude,
proprios.longitude, 
proprios.numero,
proprios.secretaria_id,
secretarias.abreviatura,
chamados.situacao,
chamados.class_manut
FROM proprios 
INNER JOIN secretarias 
ON secretarias.id = proprios.secretaria_id
INNER JOIN chamados
ON proprios.id = chamados.proprio_id";

$result = mysqli_query($conn, $query);
if (!$result) {
    die('Invalid query: ');
}



while ($row = @mysqli_fetch_assoc($result)) {
    
    $proprios[] = array(
       
        'id' => $row['id'],
        'nome' => ($row['nome']),
        'endereco' => ($row['endereco']),
        'lat' => $row['latitude'],
        'long' => $row['longitude'],
        'numero' => $row['numero'],
        'sec_id' => $row['secretaria_id'],
        'abreviatura' => $row['abreviatura'],
        'situacao' => $row['situacao'],
        'class_manut' => $row['class_manut']
    );
       

    
}

echo json_encode($proprios);
mysqli_close($conn);

?>