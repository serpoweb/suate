-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 09/11/2018 às 11:44
-- Versão do servidor: 10.2.16-MariaDB
-- Versão do PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u507389394_certa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(6) NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blacklist`
--

INSERT INTO `blacklist` (`id`, `cpf`) VALUES
(1, 'CPF Criptografado'),
(3, 'CPF 2 Criptografado');

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamados`
--

CREATE TABLE `chamados` (
  `id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `situacao` int(4) NOT NULL,
  `data_abertura` date NOT NULL DEFAULT current_timestamp(),
  `data_finalizacao` date DEFAULT NULL,
  `class_manut` int(4) DEFAULT NULL,
  `periodicidade` int(4) DEFAULT NULL,
  `atividade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprio_id` int(6) NOT NULL,
  `palavra_chave_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `chamados`
--

INSERT INTO `chamados` (`id`, `situacao`, `data_abertura`, `data_finalizacao`, `class_manut`, `periodicidade`, `atividade`, `observacoes`, `proprio_id`, `palavra_chave_id`) VALUES
('SMSECULTUR1', 4, '2018-10-16', NULL, 1, 1, 'testetefvegfte', '', 250, 27),
('SMSECULTUR2', 1, '2018-10-16', NULL, 1, 1, 'hgdfgfdsfsdgcdsz', '', 250, 27),
('SMSEDU11', 4, '2018-10-16', NULL, 1, 0, 'Problema no forro', 'falsa', 173, 8),
('SMSEDU12', 3, '2018-10-16', NULL, 1, 0, 'Ajuste feito', '', 87, 32),
('SMSEDU13', 2, '2018-10-16', NULL, 2, 1, 'Arrumar o piso', '', 216, 6),
('SMSEDU15', 1, '2018-10-16', NULL, 1, 0, 'Barra com defeito', '', 90, 17),
('SMSEDU2', 1, '2018-10-16', NULL, 2, 0, 'Arrumar o telhado', '', 117, 5),
('SMSEDU4', 3, '2018-10-16', NULL, 3, 0, 'Pintar o exterior da EM', 'Achar a tonalidade original do próprio', 157, 19),
('SMSEDU7', 4, '2018-10-16', NULL, 3, 0, 'Queda de forro', 'falsa', 159, 8),
('SMSEDU9', 1, '2018-10-16', NULL, 2, 1, 'Troca de lampadas', '', 61, 14),
('SMSEMA1', 1, '2018-10-16', NULL, 3, 1, 'Retirada das árvores caídas', '', 264, 20),
('SMSEMA2', 4, '2018-10-16', NULL, 1, 0, 'Pintura da biodiversidade', '', 264, 19),
('SMSEMA3', 1, '2018-10-16', NULL, 1, 0, 'Limpeza', '', 265, 38),
('SMSEMA4', 1, '2018-10-16', NULL, 1, 0, 'Pintura 2', '', 265, 38),
('SMSEMA5', 1, '2018-10-16', NULL, 1, 0, 'Trinca', '', 264, 2),
('SMSEMA6', 1, '2018-10-16', NULL, 1, 0, 'CONSERTAR O CHÃO', '', 264, 22),
('SMSEMA7', 1, '2018-10-16', NULL, 2, 0, 'Arrumar o telhado 2', '', 264, 5),
('SMSEMA8', 1, '2018-10-16', NULL, 1, 1, 'fvgtshhjy', '', 264, 3),
('SMSEMES1', 1, '2018-10-16', NULL, 3, 0, 'Blindex quebrado', 'Fachada destruida', 234, 42),
('SMSERPO1', 1, '2018-10-16', NULL, 1, 0, 'Luzes queimadas', '', 19, 27),
('SMSERPO2', 1, '2018-10-16', NULL, 1, 0, '', '', 1, 1),
('SMSERPO3', 2, '2018-10-16', NULL, 2, 1, 'Teste', 'Teste', 4, 2),
('SMSERPO4', 1, '2018-10-16', NULL, 1, 0, 'y', '', 1, 1),
('SMSESDEC1', 1, '2018-10-16', NULL, 1, 0, 'yu', '', 257, 3),
('SMSIAS1', 1, '2018-10-16', NULL, 2, 0, 'Porta quebrada', '', 289, 3),
('SMSIAS2', 1, '2018-10-16', NULL, 1, 0, 'Revestimento da Parede', '', 289, 7),
('SMSIAS3', 1, '2018-10-16', NULL, 3, 0, 'O telhamento precisa ser corrigido para que o prédio possa voltar a funcionar', '', 295, 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cidades`
--

CREATE TABLE `cidades` (
  `id` int(6) NOT NULL,
  `nome` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cidades`
--

INSERT INTO `cidades` (`id`, `nome`, `estado_id`) VALUES
(1, 'Sorocaba', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contratos`
--

CREATE TABLE `contratos` (
  `id` int(6) NOT NULL,
  `data_contratacao` date NOT NULL,
  `valor_principal` decimal(11,2) NOT NULL,
  `valor1` decimal(11,2) DEFAULT NULL,
  `valor2` decimal(11,2) DEFAULT NULL,
  `valor3` decimal(11,2) DEFAULT NULL,
  `valor4` decimal(11,2) DEFAULT NULL,
  `valor5` decimal(11,2) DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `terceirizada_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contratos`
--

INSERT INTO `contratos` (`id`, `data_contratacao`, `valor_principal`, `valor1`, `valor2`, `valor3`, `valor4`, `valor5`, `observacoes`, `chamado_id`, `terceirizada_id`) VALUES
(8, '2018-10-16', '1500.00', '150.00', '100.00', '50.00', '30.00', '0.00', 'não sei', 'SMSEDU2', 6);

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipes`
--

CREATE TABLE `equipes` (
  `id` int(6) NOT NULL,
  `nome` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `equipes`
--

INSERT INTO `equipes` (`id`, `nome`, `usuario_id`) VALUES
(5, 'Equipe A', 165),
(23, 'Equipe B', 19),
(27, 'Equipe D', 168);

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipes_propria`
--

CREATE TABLE `equipes_propria` (
  `id` int(6) NOT NULL,
  `data` date DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipe_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `equipes_propria`
--

INSERT INTO `equipes_propria` (`id`, `data`, `observacoes`, `chamado_id`, `equipe_id`) VALUES
(14, '2018-10-16', 'Levar todo o material necessario', 'SMSEDU2', 5),
(15, '2018-10-16', 'O material levado não foi suficiente', 'SMSEDU13', 23);

-- --------------------------------------------------------

--
-- Estrutura para tabela `estados`
--

CREATE TABLE `estados` (
  `id` int(6) NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `estados`
--

INSERT INTO `estados` (`id`, `nome`) VALUES
(1, 'São Paulo');

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens_chamado`
--

CREATE TABLE `itens_chamado` (
  `id` int(8) NOT NULL,
  `descricao` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `unidade` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantidade` double DEFAULT NULL,
  `observacao` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ocorrencias`
--

CREATE TABLE `ocorrencias` (
  `id` int(12) NOT NULL,
  `situacao` int(4) DEFAULT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `palavra_chave_id` int(6) NOT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprio_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `ocorrencias`
--

INSERT INTO `ocorrencias` (`id`, `situacao`, `cpf`, `descricao`, `foto`, `palavra_chave_id`, `chamado_id`, `proprio_id`) VALUES
(51, 1, '46323322838', 'A chuva de ontem levou metade do telhado embora', '5ca1eedd27fce7b56c78e9627f73ed295bc5f994aaca51539701140.jpg', 5, 'SMSEDU2', 117),
(55, 1, '2222223213', 'Falra de cabo de rede', '9d9759d6228409486a110d218e1698a85bc5fe4ab4bcf1539702346.jpg', 22, 'SMSEMA6', 264),
(62, 1, '234567891', 'Falta de forro na parede', '2a4777035253a3cb6cdc7c764f8a6ed05bc5ff2bcf3d21539702571.jpg', 8, 'SMSEDU11', 173),
(74, 1, '45796695843', 'Algumas luzes estão queimadas', '0d5b1c4c7f720f698946c7f6ab08f6875bc60c27480411539705895.jpg', 27, 'SMSERPO1', 19),
(75, 1, '48875147895', 'A luzes externas estão com problema, pois de noite a escola fica toda escura, possibilitando assaltos.', '629c11d2791b3b4ff9d56b07ac9edb1b5bc60c3b08a661539705915.jpg', 27, NULL, 153),
(76, 1, '45796695843', 'Vento derrubou árvores e está tudo muito sujo', '0d5b1c4c7f720f698946c7f6ab08f6875bc60cf05d7bf1539706096.jpg', 38, 'SMSEMA4', 265),
(77, 1, '21156487114', 'Vazamento de água na entrada. Água limpa.', '4a2902861d02f2bcc8f4940c9679e9975bc60d14ba06c1539706132.jpg', 32, 'SMSEDU12', 87),
(79, 1, '41158793641', 'Árvores caidas.', '17746e9aaa7a14d732a2b059914d929b5bc60df70df481539706359.jpg', 20, 'SMSEMA1', 264),
(80, 1, '333333333', 'Mal acabamento do piso', 'b03ec8f68e75a6b3317733e6126e70e35bc60e22b31971539706402.jpg', 6, 'SMSEDU13', 216),
(81, 1, '12345678', 'Torto', 'c6d7ea4b8dedc26efe3d6881dab74e8a5bc60f1bad8131539706651.jpg', 17, 'SMSEDU15', 90),
(83, 1, '45796695843', 'No ultimo baile da terceira idade um ventilador caiu e saiu casca da parede ', '0d5b1c4c7f720f698946c7f6ab08f6875bc60ffcabecb1539706876.jpg', 7, 'SMSIAS2', 289),
(84, 1, '22222222222', 'Muro pixado', '6d2d85a5c1210ccc67ce9bddce2099e25bc610a53378a1539707045.jpg', 19, 'SMSEMA2', 264),
(85, 1, '32226627741', 'Lampadadas queimadas', '60e0c4af48450f919e5cb941f19088dd5bc6117fa8c671539707263.jpg', 27, 'SMSECULTUR2', 250),
(87, 1, '85694029887', 'Trinca na coluns', 'a8c1b567d4332ea96ca8b68ef0a3c2085bc618a0cc6dc1539709088.jpeg', 2, 'SMSEMA5', 264),
(88, 1, '46129276800', 'Problema na parede', 'd3ab76ead6e8bd91c1fc9750d3173a595bc61ce48293a1539710180.png', 1, NULL, 218),
(89, 1, '85694029887', 'Piso desalinhado ', 'aabc72b1442182be63adedafe5ce650b5bc61d0591d0b1539710213.jpeg', 6, NULL, 33),
(91, 1, '123', 'Caiu uma arvore no telhado', '319626c472cb9e1a9c5e99833a4ff4da5bc628d697dad1539713238.png', 5, 'SMSEMA7', 264),
(92, 1, '1234', 'testeeee', '7ae778adadb1feeb1f791bbb1fae75035bc629fd34cf21539713533.png', 1, NULL, 169),
(93, 1, '123', 'dsfsdfa', '30da51d71899a5ec3df8f98dc8d963c95bc62a8f0de601539713679.png', 3, NULL, 33),
(94, 1, '316.4451', 'hyv gvhh', 'b9fb9d37bdf15a699bc071ce49baea535bc62c6a942451539714154.jpg', 3, 'SMSEMA8', 264),
(95, 1, '4565445', 'cbvyhhuj', 'b9fb9d37bdf15a699bc071ce49baea535bc62ca09caae1539714208.jpg', 4, NULL, 72),
(96, 1, '45796695843', 'O telhamento foi afetado por uma chuva que caiu no dia 15', '30da51d71899a5ec3df8f98dc8d963c95bc62d4d24c501539714381.png', 5, 'SMSIAS3', 295),
(97, 1, '46129276800', 'metade do telhado caiu com a ultima chuva', 'b9fb9d37bdf15a699bc071ce49baea535bc62edb3147e1539714779.jpg', 5, NULL, 178),
(98, 1, '28863969825', 'piso trincado', '44eba1feac84817c1625286f15b94b385bc63486403e81539716230.png', 6, NULL, 34),
(99, 1, '85694029887', 'Vidro quebrado', '7a514687d9a200711fa0a750ca75f3885bc63490c44e21539716240.jpeg', 3, 'SMSESDEC1', 257),
(100, 1, '4663765118468', 'nbkh', '7c59197cfc87736f204b9f3b81e6fc9d5bcf41262e6451540309286.jpg', 2, NULL, 275),
(101, 1, '463.233.228-38', 'A chuva de ontem destruiu o telhado', '5ca1eedd27fce7b56c78e9627f73ed295bd8b8e3913951540929763.jpg', 5, NULL, 159);

-- --------------------------------------------------------

--
-- Estrutura para tabela `palavras_chave`
--

CREATE TABLE `palavras_chave` (
  `id` int(6) NOT NULL,
  `descricao` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `palavras_chave`
--

INSERT INTO `palavras_chave` (`id`, `descricao`) VALUES
(1, 'Fundação'),
(2, 'Estrutura'),
(3, 'Alvenaria e Elementos Divisórios'),
(4, 'Estrutura da Cobertura em Madeira, Forro, Alumínio e Concreto'),
(5, 'Telhamento'),
(6, 'Revestimento em Piso'),
(7, 'Revestimento em Parede'),
(8, 'Forro'),
(9, 'Brise'),
(10, 'Esquadria, Marcenaria e Elementos em Madeira'),
(11, 'Esquadria, Serralheria e Elementos em Ferro'),
(12, 'Esquadria, Serralheria e Elementos em Alumínio'),
(13, 'Esquadria e Elementos em Vidro'),
(14, 'Esquadria e Elemento em Material Especial'),
(15, 'Ferragem Complementar para Esquadrias'),
(16, 'Inserte Metálico'),
(17, 'Barra de Apoio'),
(18, 'Impermeabilização, Proteção e Junta'),
(19, 'Pintura'),
(20, 'Paisagismo e Fechamentos'),
(21, 'Playground e Equipamentos Recreativos'),
(22, 'Entrada de Energia Elétrica e Telefonia'),
(23, 'Quadro e Painel para Energia Elétrica e Telefonia'),
(24, 'Tubulação e Conduto para Energia Elétrica e Telefonia Básica'),
(25, 'Condutor e Enfiação de Energia Elétrica e Telefonia'),
(26, 'Distribuição de Força e Comando de Energia Elétrica e Telefonia'),
(27, 'Iluminação'),
(28, 'Pára-raios para Edificação'),
(29, 'Aparelhos Elétricos, Hidráulicos e a Gás'),
(30, 'Aparelhos e Metais Hidráulicos'),
(31, 'Entrada de Água, Incêndio e Gás'),
(32, 'Tubulação e Condutores para Líquidos e Gases'),
(33, 'Válvulas e Aparelhos de Medição e Controle para Líquidos e Gases'),
(34, 'Reservatório e Tanque para Líquidos e Gases'),
(35, 'Caixa, Ralo, Grelha e Acessório Hidráulico'),
(36, 'Detecção, Combate e Prevenção a Incêndio'),
(37, 'Pavimentação e Passeio'),
(38, 'Limpeza e Arremate'),
(39, 'Conforto Mecânico, Equipamento e Sistema'),
(40, 'Cozinha, Refeitório, Lavanderia Industrial e Equipamento'),
(41, 'Resfriamento e Conservação de Material Perecível'),
(42, 'Segurança, Vigilância e Controle, Equipamentos e Sistema'),
(43, 'Telefonia, Lógica e Transmissão de Dados, Equipamento e Sistema'),
(44, 'Sinalização e Comunicação Visual');

-- --------------------------------------------------------

--
-- Estrutura para tabela `proprios`
--

CREATE TABLE `proprios` (
  `id` int(6) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `horario_abert` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `horario_fech` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `pavimentos` int(4) DEFAULT NULL,
  `data_construcao` date DEFAULT NULL,
  `area_const` double DEFAULT NULL,
  `validade_avcb` date DEFAULT NULL,
  `publico_alvo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benif_manha` int(8) DEFAULT NULL,
  `benif_tarde` int(8) DEFAULT NULL,
  `benif_noite` int(8) DEFAULT NULL,
  `usuario_id` int(6) DEFAULT NULL,
  `cidade_id` int(6) DEFAULT NULL,
  `secretaria_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `proprios`
--

INSERT INTO `proprios` (`id`, `nome`, `endereco`, `numero`, `bairro`, `cep`, `horario_abert`, `horario_fech`, `ddd`, `telefone`, `latitude`, `longitude`, `pavimentos`, `data_construcao`, `area_const`, `validade_avcb`, `publico_alvo`, `benif_manha`, `benif_tarde`, `benif_noite`, `usuario_id`, `cidade_id`, `secretaria_id`) VALUES
(1, 'Cemitério da Saudade', 'Praça Pedro de Toledo', 's/n', 'Além Linha', '18060080', '', '', '15', '32311402', '-23.491917', '-47.459774', 0, NULL, 0, '0000-00-00', '', 0, 0, 0, 20, 1, 2),
(2, 'Cemitério da Aparecida', 'Rua Quirino de Melo', 's/n', 'Aparecidinha', '18087200', '', '', '15', '32254382', '-23.439435', '-47.364709', 0, NULL, 0, '0000-00-00', '', 0, 0, 0, 20, 1, 2),
(3, 'Cemitério da Consolação', 'Rua Alcindo Guanabara', 's/n', 'Vila Haro', '18015225', NULL, NULL, '15', '32279011', '-23.497084', '-47.436939', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 1, 2),
(4, 'Cemitério Santo Antônio', 'Rua Geraldo Soares da Silva', '871', 'Wanel Ville', '18055089', NULL, NULL, '15', '32214448', '-23.486985', '-47.504550', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 1, 2),
(5, 'Centro Operacional I', 'Rua Peru', '425', 'Vila Barcelona', '18025290', NULL, NULL, '15', '32271277', '-23.514627', '-47.436308', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, 1, 2),
(6, 'Centro Operacional II', 'Rua Vidal de Araújo', '277', 'Jardim do Paço', '18087085', NULL, NULL, NULL, NULL, '-23.470982', '-47.418565', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22, 1, 2),
(13, 'Casa do Cidadão Nogueira Padilha', 'Rua Coronel Nogueira Padilha', '1460', 'Vila Hortência', '18020002', '09:00', '17:00', '15', '32377171', '-23.508643', '-47.439319', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(14, 'Casa do Cidadão Brigadeiro Tobias', 'Avenida Bandeirantes', '4155', 'Brigadeiro Tobias', '18108000', '09:00', '17:00', '15', '32364371', '-23.508237', '-47.364506', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(16, 'Casa do Cidadão Éden', 'Rua Bonifácio de Oliveira Cassú', '80', 'Éden', '18103000', '09:00', '17:00', '15', '33353480', '-23.424446', '-47.414219', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(17, 'Casa do Cidadão Itavuvu', 'Avenida Itavuvu', '3415', 'Parque das Laranjeiras', '18078005', '09:00', '17:00', '15', '32269292', '-23.455711', '-47.482754', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(18, 'Casa do Cidadão Ipanema', 'Avenida Ipanema', '3349', 'Vila Helena', '18070631', '09:00', '17:00', '15', '33139440', '-23.467547', '-47.494495', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(19, 'Praça das Sete', 'Rua Dois', '120', 'Parque Vitória Régia', '18078469', '05:00', '18:00', '1', '1', '-23.430533', '-47.461818', 0, NULL, 10, '2022-05-01', 'Todos os Municipes', 120, 40, 100, 19, 1, 2),
(21, 'Casa do Cidadão Paço Municipal', 'Avenida Engenheiro Carlos Reinaldo Mendes', '3041', 'Alto da Boa Vista', '18013280', '09:00', '17:00', '15', '32382646', '-23.478001', '-47.421970', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(22, 'Casa do Cidadão Ipiranga', 'Rua Estado de Israel', '424', 'Jardim Ipiranga', '18055008', '09:00', '17:00', '15', '32292950', '-23.501620', '-47.508786', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(23, 'CEI 02 \"Prof. Marina Grohmann\"', 'Rua Rodrigues Alves', '619', 'Vila Santana', '18080693', NULL, NULL, '15', '32331090', '-23.486778', '-47.453493', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23, 1, 4),
(24, 'CEI 03 \"Dona Zizi de Almeida\"', 'Rua Dr. Luiz Mendes de Almeira', '734', 'Cerrado', '18050602', NULL, NULL, '15', '32211840', '-23.518317', '-47.493159', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 1, 4),
(25, 'CEI 05 \"Antonio Amábile\"', 'Rua Panamá', '186', 'Vila Barcelona', '18025725', NULL, NULL, '15', '32322999', '-23.517033', '-47.438867', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 1, 4),
(26, 'CEI 07 \"Francisca Moura Pereira da Silva\"', 'Praça Pio XII', '10', 'Santa Rosalia', '18090200', NULL, NULL, '15', '32121587', '-23.484870', '-47.442863', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26, 1, 4),
(27, 'CEI 08 \"Prof. Messias Ribeiro de Noronha Cunha\"', 'Praça Vicente Vannunchi', 's/n', 'Vila Progresso', '18081120', NULL, NULL, '15', '32331822', '-23.478040', '-47.446070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27, 1, 4),
(28, 'CEI 09 \"Prof. Fernando Rios\"', 'Rua Nicolau Elias Tiberechmyu', '200', 'Jardim Arco Íris', '18051060', NULL, NULL, '15', '32221744', '-23.512316', '-47.508172', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 28, 1, 4),
(29, 'CEI 10 \"Eglantina Rocco Perli\"', 'Rua Joaquim Machado', '732', 'Aparecidinha', '18087280', NULL, NULL, '15', '32252363', '-23.444742', '-47.372685', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 1, 4),
(30, 'CEI 11 \"Dona Tercília Freire\"', 'Rua Joaquim Gonçalves Gomide', '127', 'Jardim Bermejo', '18044500', NULL, NULL, '15', '32216616', '-23.513459', '-47.478966', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 1, 4),
(31, 'CEI 13 \"Aluisio de Almeida\"', 'Rua Ana Gomes Corrêa', '25', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32366075', '-23.507652', '-47.370301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, 1, 4),
(32, 'CEI 14 \"Eng. Carlos Reinaldo Mendes\"', 'Rua Salvador Leire Marques', '790', 'Éden', '18103040', NULL, NULL, '15', '32252032', '-23.418210', '-47.415690', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 1, 4),
(33, 'CEI 14 \"Eng. Carlos Reinaldo Mendes\" Vinculada', 'Rua Euclides Cassiano de Araújo', 's/n', 'Iporanga', NULL, NULL, NULL, NULL, NULL, '-23.435200', '-47.423278', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 1, 4),
(34, 'CEI 15 \"Prof. Terezinha Lucas Fernandes\"', 'Avenida Pres. Juscelino Kubitscheck de Oliveira', '1166', 'Centro', '18035060', NULL, NULL, '15', '32316295', '-23.504687', '-47.463169', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 33, 1, 4),
(35, 'CEI 16 \"Prof. Beatriz de Moraes Leite Fogaça\"', 'Rua Emma Zacchi Police', '35', 'Vila Carvalho', '18035370', NULL, NULL, '15', '32313199', '-23.494397', '-47.459292', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 4),
(36, 'CEI 17 \"Issa Latuf\"', 'Rua Marechal Castelo Branco', '242', 'Jardim Sandra', '18031270', NULL, NULL, '15', '32312608', '-23.512613', '-47.454244', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 1, 4),
(37, 'CEI 18 \"Miguel Cheda\"', 'Rua Atanásio Soares', '810', 'Vila Fiori', '18075000', NULL, NULL, '15', '32312605', '-23.478318', '-47.471079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 1, 4),
(38, 'CEI 20 \"Victória Salus Lara\"', 'Rua Alcino Guanabara', '379', 'Árvore Grande', '18015225', NULL, NULL, '15', '32273081', '-23.497734', '-47.437198', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 1, 4),
(39, 'CEI 21 \"Aureliano Rodrigues\"', 'Rua Salvador Stefanelli', '506', 'Jardim Zulmira', '18061050', NULL, NULL, '15', '32223748', '-23.498446', '-47.478266', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, 1, 4),
(40, 'CEI 22 \"Dr. Victo Pedroso\"', 'Avenida Betsaida', '49', 'Jardim Betânia', '18071025', NULL, NULL, '15', '32235301', '-23.465814', '-47.500351', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39, 1, 4),
(41, 'CEI 23 \"Dolores Cupiam do Amaral\"', 'Rua José Marchi', '456', 'Jardim dos Estados', '18046070', NULL, NULL, '15', '32224213', '-23.519219', '-47.475630', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 1, 4),
(42, 'CEI 25 \"Jorge Frederico Scherepel\"', 'Rua Tchcoslocaquia', '283', 'Jardim Europa', '18044400', NULL, NULL, '15', '32218843', '-23.512976', '-47.485716', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41, 1, 4),
(43, 'CEI 26 \"Luiz de Sanctis\"', 'Rua Érico Veríssimo', '222', 'Central Parque', '18051100', NULL, NULL, '15', '32213717', '-23.512557', '-47.499270', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, 4),
(44, 'CEI 27 \"Prof. Christina dos Reis\"', 'Rua Manoel Lourenço Rodrigues', '535', 'Vila Barão', '18061230', NULL, NULL, '15', '32174970', '-23.493134', '-47.483579', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43, 1, 4),
(45, 'CEI 28 \"Rauldinéia Esteves Machado\"', 'Rua Alcino oliveira Rosa', '267', 'Parque São Bento', '18072660', NULL, NULL, '15', '32232247', '-23.432938', '-47.509553', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 1, 4),
(46, 'CEI 30 \"Maria Pedroso Bellotti\"', 'Rua Nelson Augusto Gomes', '49', 'Jardim São Paulo', '18051610', NULL, NULL, '15', '32220761', '-23.526818', '-47.495752', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 1, 4),
(47, 'CEI 31 \"Victoria Haddad Sayeg\"', 'Rua José Martinez Y. Martinez', '50', 'Jardim Golçalves', '18046070', NULL, NULL, '15', '32274192', '-23.493930', '-47.425628', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 1, 4),
(48, 'CEI 33 \"Elvira Nani Monteiro\"', 'Rua Maria de Lourdes Ferreira', '1188', 'Jardim Nova Esperança', '18061310', NULL, NULL, '15', '32219295', '-23.491802', '-47.492614', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47, 1, 4),
(49, 'CEI 34 \"Alberto Grosso\"', 'Rua Marcelo Scotto', '70', 'Vila Rica', '18013280', NULL, NULL, '15', '32281677', '-23.485364', '-47.432284', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 4),
(50, 'CEI 35 \"Maria Ondina Soares Vial Brunetto\"', 'Rua Arlinda de Almeira Santos', '138', 'Jardim Itanguá II', '18056000', NULL, NULL, '15', '32228445', '-23.504508', '-47.501164', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(51, 'CEI 36 \"Dra. Abney Medeiros Carneiro\"', 'Rua José de Andrade', '10', 'Parque Ouro Fino', '18055690', NULL, NULL, '15', '32211174', '-23.498117', '-47.505175', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 49, 1, 4),
(52, 'CEI 38 \"Maria Garcia Vecina\"', 'Rua João Gabriel Mendes', '381', 'Bila Gabriel', '18081110', NULL, NULL, '15', '32338782', '-23.480220', '-47.451524', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 1, 4),
(53, 'CEI 39 \"Sha\'ar Hanegev\"', 'Rua Paschoal Túlio', '59', 'Vila Colorau', '18025785', NULL, NULL, '15', '32273704', '-23.517986', '-47.435719', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 1, 4),
(54, 'CEI 40 \"Dona Duzolina Bariolla Pagliato\"', 'Rua Campinas', '260', 'Jardim Iguatemi', '18085400', NULL, NULL, '15', '32282675', '-23.473526', '-47.439902', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 4),
(55, 'CEI 41 \"Antonio Fratti\"', 'Rua Mário Romano', '145', 'Jardim Maria Eugênia', '18074530', NULL, NULL, '15', '32265469', '-23.456140', '-47.491633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, 1, 4),
(56, 'CEI 43 \"Prof. Vera Lúcia Momesso Maldonado\"', 'Rua Vicente Celestino', '201', 'Jardim Gutierres', '18015445', NULL, NULL, '15', '32271311', '-23.507074', '-47.423247', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 1, 4),
(57, 'CEI 44 \"Luiz Ribeiro\"', 'Rua Moacir Nascimento', '475', 'Vila Carvalho', '18060170', NULL, NULL, '15', '32335066', '-23.489975', '-47.468036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55, 1, 4),
(58, 'CEI 45 \"Diva Ferreira Cordeiro\"', 'Avenida Dr. Ulisses Guimarães', '1011', 'Parque das Laranjeiras', '18077391', NULL, NULL, '15', '32264107', '-23.453494', '-47.472247', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 1, 4),
(59, 'CEI 46 \"Ernesto Martins\"', 'Rua Arlete Pimentel Viessi', 's/n', 'Retiro São João', '18085746', NULL, NULL, '15', '32281290', '-23.460306', '-47.439952', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52, 1, 4),
(60, 'CEI 47 \"Prof. Betty Souza Oliveira\"', 'Rodovia Raposo Taveres', 'Km 109', 'Ipanema do Meio', NULL, NULL, NULL, '15', '32215813', '-23.513592', '-47.539278', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 1, 4),
(61, 'CEI 48 \"Frei Achilles Kloeckner\"', 'Rua Juvenal de Paula Souza', '285', 'Cajuru do Sul', NULL, NULL, NULL, '15', '32253466', '-23.394583', '-47.378792', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, 1, 4),
(62, 'CEI 50 \"Prof Alípio Guerra da Cunha\"', 'Rua dos Itaporangueses', '116', 'Jardim Ipiranga', '18055007', NULL, NULL, '15', '32211841', '-23.499437', '-47.508047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 1, 4),
(63, 'CEI 51 \"Rubens Vieira\"', 'Rua José João Mira Domingues', '204', 'Jardim Brasilandia', '18075610', NULL, NULL, '15', '32320423', '-23.472536', '-47.457783', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 1, 4),
(64, 'CEI 52 \"Olga Chibau Fornazieiro\"', 'Rua Francisco Loureiro', '526', 'Vila Malges', '18076555', NULL, NULL, '15', '32326771', '-23.474558', '-47.467524', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, 1, 4),
(65, 'CEI 53 \"Benjamin Felipe Grizzi\"', 'Rua Andrelino de Souza', '610', 'Jardim Maria Antônio Prado', '18076000', NULL, NULL, '15', '32261086', '-23.461151', '-47.473783', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 62, 1, 4),
(66, 'CEI 54 \"Sônia Aparecida Machado\"', 'Rua Diolindo Alves de Luz', '132', 'Bairro dos Morros', NULL, NULL, NULL, '15', '32271355', '-23.516844', '-47.427963', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 63, 1, 4),
(67, 'CEI 57 \"Eng. João Salerno\"', 'Rua José Virgilio da Silva', '307', 'Júlio de Mesquita Filho', '18053280', NULL, NULL, '15', '32210887', '-23.504914', '-47.513136', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 64, 1, 4),
(68, 'CEI 58 \"Prof. Dulce Puppo de Oliveira Pinheiro\"', 'Rua Eliezer Barbosa de Lima', '448', 'Jardim Maria do Carmo', '18081085', NULL, NULL, '15', '32329981', '-23.476553', '-47.451401', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65, 1, 4),
(69, 'CEI 59 \"Eugênio Leite\"', 'Rua Frei Ernesto Buzzi', 's/n', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32366022', '-23.508210', '-47.369273', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, 1, 4),
(70, 'CEI 60 \"Anna Rusconi\"', 'Rua Tocantins', '462', 'Vila Jardini', '18044150', NULL, NULL, '15', '32219358', '-23.511405', '-47.475447', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, 1, 4),
(71, 'CEI 61 \"Yolanda Rizzo\"', 'Avenida Betsaida', 's/n', 'Jardim Betânia', '18071490', NULL, NULL, '15', '32231327', '-23.465512', '-47.500571', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 68, 1, 4),
(72, 'CEI 62 \"Monsenhor Antônio Simon Sola\"', 'Rua Havana', '36', 'Parada do Alto', '18025803', NULL, NULL, '15', '32335042', '-23.524188', '-47.446025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69, 1, 4),
(73, 'CEI 63 \"Reynaldo D\'Alessandro\"', 'Avenida Eng. Carlos Reinaldo Mendes', '3043', 'Alto da Boa Vista', '18013280', NULL, NULL, '15', '32382230', '-23.474891', '-47.422778', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70, 1, 4),
(74, 'CEI 64 \"Joana Simon Sola\"', 'Rua Constantino Spanghero', '106', 'Vila Rica', '18013540', NULL, NULL, '15', '32281953', '-23.485468', '-47.431097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 1, 4),
(75, 'CEI 65 \"Santo Agostinho\"', 'Rua Frederico Harder', '298', 'Jardim Novo Mundo', '18052447', NULL, NULL, '15', '32218621', '-23.538755', '-47.508225', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 72, 1, 4),
(76, 'CEI 66 \"Fraternidade Feminina Cruzeiro do Sul\"', 'Rua Olímpio Loureiro', '155', 'Vila Haro', '18015000', NULL, NULL, '15', '32271496', '-23.501771', '-47.432544', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73, 1, 4),
(77, 'CEI 67 \"Prof. Maria das Graças Arruda Pereira Nardi\"', 'Avenida Pército de Souza Queiroz', '631', 'Vila Barão', '18061190', NULL, NULL, '15', '32216977', '-23.493044', '-47.484902', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 74, 1, 4),
(78, 'CEI 68 \"Gladys Moeckel de Togni Amaral\"', 'Avenida Angélica', '984', 'Vila Angélica', '18065450', NULL, NULL, '15', '32313415', '-23.478521', '-47.476840', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75, 1, 4),
(79, 'CEI 69 \"Prof. Ester Bueno de Camargo Nascimento\"', 'Rua dos Itaporangueses', '142', 'Jardim Ipiranga', '18055007', NULL, NULL, '15', '32218802', '-23.499669', '-47.508142', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 76, 1, 4),
(80, 'CEI 70 \"Prof. Adail Odin Arruda\"', 'Rua João Mattuci', '170', 'Nova Sorocaba', '18070480', NULL, NULL, '15', '32231802', '-23.474538', '-47.482250', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 77, 1, 4),
(81, 'CEI 71 \"Yolanda Prestes Neder\"', 'Rua Paula Maier Cattini', 's/n', 'Jardim Nova Esperança', '18061465', NULL, NULL, '15', '32172114', '-23.492892', '-47.492646', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 78, 1, 4),
(82, 'CEI 72 \"Prof. Sueli Gazzolli Campos\"', 'Rua Seihi Murikame', '180', 'Aparecidinha', NULL, NULL, NULL, '15', '32252766', '-23.441366', '-47.374863', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 79, 1, 4),
(83, 'CEI 73 \"Matilde Gavin\"', 'Rua Nelson Herdy Barbosa', '52', 'Vila Formosa', '18075605', NULL, NULL, '15', '32261173', '-23.465258', '-47.474855', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 80, 1, 4),
(84, 'CEI 74 \"Prof. Maria de Castro Affonso Marins\"', 'Rua Atílio Silvano', '471', 'Jardim Pacaembu', '18074410', NULL, NULL, '15', '32263212', '-23.462812', '-47.484290', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 81, 1, 4),
(85, 'CEI 75 \"Jornalista Alvir Guedes Ribeiro\"', 'Rua Diogo Gomes Filho', 's/n', 'Parque das Laranjeiras', '18077510', NULL, NULL, '15', '32265672', '-23.451830', '-47.479158', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1, 4),
(86, 'CEI 76 \"Menino Jesus\"', 'Rua Pedro Lombardi', '574', 'Mineirão', '18076520', NULL, NULL, '15', '32337449', '-23.467778', '-47.462485', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 83, 1, 4),
(87, 'CEI 77 \"Prof. Olga de Toledo Lara\"', 'Rua Joaquim Roque de Oliveira', '366', 'Vila Astúrias', '18108360', NULL, NULL, '15', '32366441', '-23.499947', '-47.368708', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, 1, 4),
(88, 'CEI 78 \"Ettore Marangoni\"', 'Rua Dionízio Bueno Sampaio', '237', 'Vila Sabiá', NULL, NULL, NULL, '15', '32313897', '-23.522935', '-47.434486', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 85, 1, 4),
(89, 'CEI 79 \"Prof. João Tortello\"', 'Rua Masaharu Tanagushi', '65', 'Jardim Botucatu', '18071044', NULL, NULL, '15', '32232066', '-23.455689', '-47.510944', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86, 1, 4),
(90, 'CEI 80 \"Prof. Ana Rosa Judice Moreira Zanussi de Oliveira\"', 'Rua João Scatena', 's/n', 'Parque Vitória Régia', '18078343', NULL, NULL, '15', '32265201', '-23.433712', '-47.467395', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 87, 1, 4),
(91, 'CEI 81 \"Prof. Edith Del Cistia Santos\"', 'Rua Alcíno Oliveira Rosa', '227', 'Parque São Bento', '18072660', NULL, NULL, '15', '32235323', '-23.432676', '-47.509191', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 4),
(92, 'CEI 82 \"Prof. Benedito Marçal - Didi\"', 'Avenida Dr. Américo Figueiredo', '3180', 'Júlio de Mesquita Filho', '18053000', NULL, NULL, '15', '32024886', '-23.505019', '-47.514018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 89, 1, 4),
(93, 'CEI 83 \"Maria Carmen Rodrigues Sacker\"', 'Rua Valdenito Pereira de Oliveira', '210', 'Jardim Lena', '18103250', NULL, NULL, '15', '33253333', '-23.418255', '-47.418639', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 1, 4),
(94, 'CEI 84 \"Osmar de Almeida\"', 'Rua Aristides de Barros', 's/n', 'Jardim São Guilherme', '18074645', NULL, NULL, '15', '32265993', '-23.449655', '-47.487441', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 91, 1, 4),
(95, 'CEI 85 \"Maria Regina Antolioli Godoy\"', 'Rua José Brandão', '234', 'Jardim Montreal', '18053470', NULL, NULL, '15', '32028526', '-23.493963', '-47.524057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 92, 1, 4),
(96, 'CEI 86 \"Jorge Luís Prestes Del Cistia\"', 'Rua Comendador Orterer', '222', 'Centro', '18060070', NULL, NULL, '15', '32331821', '-23.496083', '-47.458201', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, 1, 4),
(97, 'CEI 87 \"Dr. Cássio Rosa\"', 'Rua Chico Xavier', '45', 'Conj. Hab. Ana Paula Eleutério', '18079720', NULL, NULL, '15', '32397991', '-23.420693', '-47.483045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, 1, 4),
(98, 'CEI 88 \"Prof. Vera Apparecida Guariglia dos Santos\"', 'Estrada Dom José Melhado Campos', '200', 'Jardim Josane', '18087317', NULL, NULL, '15', '32253992', '-23.451730', '-47.373793', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 95, 1, 4),
(99, 'CEI 89 \"Zilda Pereira Aguilera\"', 'Avenida Manoel Camargo Sampaio', '1119', 'Jardim Marcelo Algunsto', '18071195', NULL, NULL, '15', '32235685', '-23.474661', '-47.500178', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 1, 4),
(101, 'CEI 90 \"Hélio Del Cistia Junior\"', 'Rua Clodoaldo Carlos Silva', 's/n', 'Jardim Bonsucesso', NULL, NULL, NULL, '15', '32268552', '-23.425070', '-47.465083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 97, 1, 4),
(102, 'CEI 91 \"Prof. Célia Cangro M. Mendes\" Vinculado CEI 81', 'Rua Michel Amary', '183', 'Jardim Bom Jesus', '18072833', NULL, NULL, '15', '32138478', '-23.440118', '-47.538430', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 1, 4),
(103, 'CEI 92 \"Prof. Dolores Fagundes Pedroso\"', 'Rua Prof. Vera Aparecida Guariglia dos Santos', '35', 'Jardim Santa Esmeralda', '18079121', NULL, NULL, '15', '32175260', '-23.436398', '-47.483044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 1, 4),
(104, 'CEI 93 \"Madre Teresa de Calcutá\"', 'Rua Luiz Gabriotti', '917', 'Wanel Ville', '18055045', NULL, NULL, '15', '32281717', '-23.487445', '-47.503295', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 1, 4),
(105, 'CEI 94 \"Prof. Ana Lúcia Pazini\"', 'Rua Ignácio Loiola Brito', '215', 'Jardim Alegria', '18103000', NULL, NULL, '15', '32336294', '-23.410964', '-47.410251', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, 1, 4),
(106, 'CEI 95 \"Jornalista Ângela Martins Vieira\"', 'Rua Antonieta Mentone Zacariotto', '55', 'Jardim Califórnia', '18071711', NULL, NULL, '15', '32138613', '-23.464440', '-47.506600', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 1, 4),
(107, 'CEI 96 \"Prof. Adelaide Piva de Lima\"', 'Rua José Pereira do Nascimento', 's/n', 'Conj. Hab. Ana Paula Eleutério', NULL, NULL, NULL, '15', '32399770', '-23.418989', '-47.489247', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 102, 1, 4),
(108, 'CEI 97 \"Maria Dorelli de Magalhães\"', 'Rua Vicente Dias', '200', 'Jardim Piazza Di Roma', NULL, NULL, NULL, '15', '32029129', '-23.516805', '-47.514874', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 103, 1, 4),
(109, 'CEI 98 \"Olinda Luz Marthe\"', 'Rua Mário Bacaro', '200', 'Jardim Tropical', '18054015', NULL, NULL, '15', '32027480', '-23.494211', '-47.529696', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 104, 1, 4),
(110, 'CEI 99 \"Larissa de Freitas Borges\"', 'Rua Edite Maganini Mattezi', '70', 'Parque São Bento', '18072062', NULL, NULL, NULL, NULL, '-23.440467', '-47.505355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, 1, 4),
(111, 'CEI 100 \"Mercedes Urquiza Desidério da Silva\"', 'Rua Paschoal Lacava', 's/n', 'Jardim Pacaembu', '18074145', NULL, NULL, '15', '32282111', '-23.463394', '-47.484122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 106, 1, 4),
(112, 'CEI 101 \"Leonilda Cruz Maldonado\"', 'Rua João Martini', '369', 'Vila Formosa', '18076260', NULL, NULL, '15', '32282828', '-23.465203', '-47.474245', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107, 1, 4),
(113, 'CEI 103 \"Prof. Jorge Moyses Betti\"', 'Rua João Rodrigues', '387', 'Sorocaba Park', NULL, NULL, NULL, '15', '32281841', '-23.437516', '-47.463931', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 108, 1, 4),
(114, 'CEI 104 \"Prefeito José Crespo Gonzales\"', 'Rua João Rosa Filho', '128', 'Morada das Flores', NULL, NULL, NULL, '15', '32176332', '-23.443283', '-47.368426', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 109, 1, 4),
(115, 'CEI 105 \"Dra. Maura Roberti\"', 'Rua Diva Forestieri', '90', 'Jardim Nova Ipanema', '18071036', NULL, NULL, '15', '32233527', '-23.457779', '-47.497910', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 110, 1, 4),
(116, 'CEI 106 \"Aurea Paixão Rolim\"', 'Rua Aristides de Barros', '40', 'Jardim São Guilerme', '18074645', NULL, NULL, '15', '32267618', '-23.450041', '-47.487793', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111, 1, 4),
(117, 'CEI 107 \"Arminda da Conceição da Silva Telo\"', 'Rua Izidro Roque da Silva Telo', '320', 'Horto Florestal', NULL, NULL, NULL, '15', '57045869', '-23.441856', '-47.501780', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 112, 1, 4),
(118, 'CEI 108 \"Antonio Bengla Mestre Filho\"', 'Rua José de Andrade', '08', 'Parque Ouro Fino', '18055690', NULL, NULL, '15', '32023186', '-23.498111', '-47.505378', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 113, 1, 4),
(119, 'CEI 109 \"Benedicto Pagliato\"', 'Rua Elias Rodrigues Claro', '474', 'Jardim São Carlos', '18046390', NULL, NULL, '15', '32275911', '-23.527433', '-47.479298', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 114, 1, 4),
(120, 'CEI 110 \"Maria Leopoldina Campolim Godoy Del Bem\"', 'Rua Demanda do Vale Blaseck', '225', 'Vila Barão', '18065580', NULL, NULL, '15', '32138677', '-23.491642', '-47.481421', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 115, 1, 4),
(121, 'CEI 111 \"Ivan Gerbovic\" Vinculado CEI 48', 'Rua Brasil', '50', 'Cajuru do Sul', '18105040', NULL, NULL, '15', '32253542', '-23.405829', '-47.383601', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58, 1, 4),
(122, 'CEI 112 \"Izabel dos Santos Pereira\"', 'Avenida Betsaida', '75', 'Jardim Betânia', '18071490', NULL, NULL, '15', '32232194', '-23.465378', '-47.500949', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 116, 1, 4),
(123, 'CEI 113 Vinculado CEI 10', 'Rua Roberto Vieira Holtz', '95', 'Aparecidinha', NULL, NULL, NULL, '15', '32251435', '-23.444580', '-47.370390', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(124, 'EM \"Dr. Achilles de Almeida\"', 'Rua Manoel Lopes', '250', 'Além Ponte', '18020218', NULL, NULL, '15', '32310199', '-23.506730', '-47.447257', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, 1, 4),
(125, 'EM \"Prof. Amin Cassar\"', 'Rua Prof. Alberto Rossi', 's/n', 'Jardim São Camilo', '18079342', NULL, NULL, '15', '33022101', '-23.444629', '-47.485429', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(126, 'EM \"Profa. Ana Cecilia Falcato Prado Fontes\"', 'Alameda da Felicidade', '100', 'Jardim Renascer', '18079770', NULL, NULL, '15', '32134017', '-23.420672', '-47.483890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 118, 1, 4),
(127, 'EM \"Prof. Ary de Oliveira Seabra\"', 'Rua João Granado', '45', 'Jardim Eliana', NULL, NULL, NULL, '15', '32254100', '-23.392921', '-47.379658', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 119, 1, 4),
(128, 'EM \"Avelino Leite de Camargo\"', 'Avenida Monsenhor Mario Calazanz', '133', 'Jardim Nova Esperança', '18061411', NULL, NULL, '15', '32175552', '-23.489737', '-47.489800', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120, 1, 4),
(129, 'EM \"Prof. Basílio da Costa Daemon\"', 'Rua Ataliba Pontes', 's/n', 'Paineiras', '18078613', NULL, NULL, '15', '33111174', '-23.442613', '-47.475103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 121, 1, 4),
(130, 'EM \"Prof. Benedicto José Nunes\"', 'Rua Padre Domenico Trivi', '129', 'Parque Esmeralda', '18055745', NULL, NULL, '15', '33212880', '-23.491928', '-47.494273', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122, 1, 4),
(131, 'EM \"Benedito Cleto\"', 'Avenida Ipanema', '5353', 'Jardim Novo Horizonte', NULL, NULL, NULL, '15', '32254875', '-23.454955', '-47.505913', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 123, 1, 4),
(132, 'EM \"Comendador Alfredo Metidieri\"', 'Rua Manoel Lourenço Rodrigues', '591', 'Vila Barão', '18061230', NULL, NULL, '15', '32023277', '-23.492784', '-47.484410', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 124, 1, 4),
(133, 'EM \"Profa. Darlene Devasto\"', 'Rua Ary Anunciato', '208', 'Jardim Atílio Silvano', '18077080', NULL, NULL, '15', '32263543', '-23.458105', '-47.478228', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 125, 1, 4),
(134, 'EM \"Duljara Fernandes de Oliveira\"', 'Rua Móbil Lopes de Oliveira', '51', 'Jardim Santo Amaro', '18074681', NULL, NULL, '15', '32394305', '-23.452318', '-47.492738', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 126, 1, 4),
(135, 'EM \"Prof. Edemir Antonio Digiampietri\"', 'Avenida Percito de Souza Queiroz', '555', 'Vila Barão', NULL, NULL, NULL, '15', '32175036', '-23.493320', '-47.484561', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 127, 1, 4),
(136, 'EM \"Bairro do Éden\"', 'Rua Salvador Leite Marques', '700', 'Éden', '18103040', NULL, NULL, '15', '32135877', '-23.418613', '-47.415886', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 128, 1, 4),
(137, 'EM \"Edward Frufru Marciano da Silva\"', 'Rua Ignes Hannel Brenga', '101', 'Jardim Botucatu', '18071045', NULL, NULL, '15', '32231961', '-23.455528', '-47.510523', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 129, 1, 4),
(138, 'EM \"Prof. Flávio de Souza Nogueira\"', 'Rua Benedito Galdino de Barros', '47', 'Jardim Ferreira', '18080660', NULL, NULL, '15', '32121080', '-23.481717', '-47.461373', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 130, 1, 4),
(139, 'EM \"Profa. Genny Kalil Milego\"', 'Rua Vanderlei Felício', '215', 'Herbert de Souza', '18079025', NULL, NULL, '15', '32391477', '-23.435313', '-47.475456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 131, 1, 4),
(140, 'EM \"Dr. Getúlio Vargas\"', 'Avenida Dr. Eugênio Salerno', '298', 'Santa Terezinha', '18035430', NULL, NULL, '15', '32221434', '-23.499975', '-47.468335', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 132, 1, 4),
(141, 'EM \"Dr. Hélio Rosa Baldy\"', 'Rua Tercizo Geraldo Dário', '163', 'Jardim São Guilherme II', '18074660', NULL, NULL, '15', '32262245', '-23.446990', '-47.488451', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 133, 1, 4),
(142, 'EM \"Profa. Inês Rodrigues Cesarotti\"', 'Rua Maria Moreno Trugillano', 's/n', 'Jardim Bonsucesso', '18078430', NULL, NULL, '15', '33024932', '-23.428324', '-47.461409', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 134, 1, 4),
(143, 'EM \"Prof. Irineu Leister\"', 'Rua Odete Nanci Giraldi', '67', 'Jardim Ipiranga', '18055004', NULL, NULL, '15', '32227280', '-23.498664', '-47.508337', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 135, 1, 4),
(144, 'EM \"João Francisco Rosa\"', 'Avenida Ipanema', 's/n', 'Vila Angélica', '18065390', NULL, NULL, '15', '32136903', '-23.481114', '-47.480233', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 136, 1, 4),
(145, 'EM \"Prof. José Carlos Florenzano\"', 'Rua Vitor Cioffi de Luca', '800', 'Jardim Santa Esmeralda', NULL, NULL, NULL, '15', '33023847', '-23.433849', '-47.485144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 137, 1, 4),
(146, 'EM \"José Mendes\"', 'Rua Armando Rizzo', '558', 'Jardim Hungarês', '18075501', NULL, NULL, '15', '32265472', '-23.470500', '-47.468544', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 138, 1, 4),
(147, 'EM \"Profa. Josefina Zilia de Carvalho\"', 'Rua Nhonho Neves', '151', 'Jardim Guadalajara', '18045540', NULL, NULL, '15', '32177291', '-23.524599', '-47.486366', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 139, 1, 4),
(148, 'EM \"Profa. Julica Bierrenbach\"', 'Rua São Miguel Arcanjo', '160', 'Jardim Cruzeiro do Sul', '18013420', NULL, NULL, '15', '32277680', '-23.492886', '-47.438994', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 140, 1, 4),
(149, 'EM \"Profa. Léa Edy Alonso Saliba\"', 'Rua Miguel Stefani', 's/n', 'Jardim Marcelo Augusto', '18071297', NULL, NULL, '15', '32131614', '-23.474288', '-47.499003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 141, 1, 4),
(150, 'EM \"Leda Therezinha Borghesi Rodrigues\"', 'Avenida Ipanema', '5515', 'Ipanema Ville', '18071801', NULL, NULL, '15', '32394762', '-23.453482', '-47.508354', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 142, 1, 4),
(151, 'EM \"Leonor Pinto Thomaz\"', 'Rua XV de Novembro', '390', 'Centro', '18010082', NULL, NULL, '15', '32339911', '-23.500985', '-47.453382', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 143, 1, 4),
(152, 'EM \"Prof. Luiz Almeida Marins\"', 'Avenida Américo Figueiredo', '3500', 'Júlio de Mesquita Filho', '18053000', NULL, NULL, '15', '32212280', '-23.503243', '-47.514960', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 144, 1, 4),
(153, 'EM \"Profa. Maria de Lourdes A. de Moraes\"', 'Rua Vicente Miranda', '300', 'Jardim Santa Marina', '18075000', NULL, NULL, '15', '32266354', '-23.445062', '-47.469148', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 145, 1, 4),
(154, 'EM \"Profa. Maria de Lources Martins Martinez\"', 'Rua Daria Galvão da Silva', '292', 'Jardim Santa Bárbara', '18053365', NULL, NULL, '15', '32227751', '-23.499511', '-47.522993', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 146, 1, 4),
(155, 'EM \"Profa. Maria Domingas Tótara de Goes\"', 'Alameda Laurindo de Brito', '180', 'Vila Carol', '18070295', NULL, NULL, '15', '32114660', '-23.475593', '-47.476199', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 147, 1, 4),
(156, 'EM \"Matheus Maylasky\"', 'Rua Hermelindo Matarazzo', '22', 'Vila Gagliardi', '18080000', NULL, NULL, '15', '32315802', '-23.495198', '-47.456140', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 149, 1, 4),
(157, 'EM \"Profa. Maria Ignez Figueiredo Deluno\"', 'Rua Rubens Pellini', '156', 'Mineirão', '18076540', NULL, NULL, '15', '32314804', '-23.466959', '-47.462810', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 148, 1, 4),
(158, 'EM \"Dr. Milton Leite de Oliveira\"', 'Rua Antônio Moreira Alves', '248', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32178858', '-23.509025', '-47.368536', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 150, 1, 4),
(159, 'EM \"Prof. Ney Oliveira Fogaça - O Quintal\"', 'Avenida Nove de Julho', '585', 'Vila Barão', '18060630', NULL, NULL, '15', '32371456', '-23.494756', '-47.477874', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 151, 1, 4),
(160, 'EM \"Profa. Norma Justa Dall\'Ara\"', 'Rua Profa. Eny Apparecida Garcia Chagas', '35', 'Jardim São Carvalho', '18078440', NULL, NULL, '15', '32266541', '-23.429484', '-47.458667', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 152, 1, 4),
(161, 'EM \"Profa. Odilla Caldini Crespo\"', 'Rua João Pedro Luz', '260', 'Recreio dos Sorocabanos', '18071022', NULL, NULL, '15', '32339619', '-23.456512', '-47.500926', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(162, 'EM \"Prof. Oswaldo de Oliveira\"', 'Rua Flor do Carvalho', '929', 'Jardim Jatobá', '18103125', NULL, NULL, '15', '32254451', '-23.410022', '-47.414634', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 153, 1, 4),
(163, 'EM \"Dr. Oswaldo Duarte\"', 'Rua Luiz Gabriotti', '213', 'Wanel Ville II', '18055089', NULL, NULL, '15', '32172001', '-23.488139', '-47.506390', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 154, 1, 4),
(164, 'EM \"Prof. Paulo Fernando Nóbrega Tortello\"', 'Rua Pedro Carrasco Montalbam', '140', 'Parque das Laranjeiras', '18077430', NULL, NULL, '15', '32265870', '-23.452439', '-47.481093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 155, 1, 4),
(165, 'EM \"Quinzinho de Barros\"', 'Rua Joaquim Rodrigues de Barros', '477', 'Vila Hortência', '18020280', NULL, NULL, '15', '32271330', '-23.509320', '-47.433480', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 156, 1, 4),
(166, 'EM \"Ronaldo Campos de Arruda\"', 'Rua Luiz Almeida Marins', '275', 'Nova Aparecidinha', '18087284', NULL, NULL, '15', '32251435', '-23.447878', '-47.372715', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(167, 'EM \"Rosa Cury\"', 'Rua Octávio Novaes de Carvalho', '36', 'Jardim Vera Cruz', '18055270', NULL, NULL, '15', '32227888', '-23.510066', '-47.489567', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 157, 1, 4),
(168, 'EM \"Sorocaba-Leste\"', 'Rua Cervantes', '678', 'Vila Assis', '18025174', NULL, NULL, '15', '32271213', '-23.511855', '-47.440789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 158, 1, 4),
(169, 'EM \"Tadeusz Jozefczyk\"', 'Estrada do Império', '2455', 'Genebra', NULL, NULL, NULL, '15', '32366420', '-23.495804', '-47.331686', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 159, 1, 4),
(170, 'EM \"Tereza Ciambelli Gianini\"', 'Avenida Rio Claro', '350', 'Nova Sorocaba', '18070760', NULL, NULL, '15', '32236386', '-23.468460', '-47.482625', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 160, 1, 4),
(171, 'EM \"Prof. Walter Carretero\"', 'Avenida Itavuvu', '7000', 'Conj. Hab. Ana Pala Eleutério', NULL, NULL, NULL, '15', '33111366', '-23.423233', '-47.482555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 161, 1, 4),
(172, 'EM \"Profa. Zilah Dias de Mello Schrepel\"', 'Rua Durvalino Manfio', '264', 'Jardim Santo André', '18077345', NULL, NULL, '15', '33111400', '-23.547770', '-47.466697', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 162, 1, 4),
(173, 'SABE TUDO - EM \"Prof. Amin Cassar\"', 'Rua Prof. Alberto Rossi', 's/n', 'Jardim São Camilo', '18079342', NULL, NULL, NULL, NULL, '-23.444945', '-47.485183', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(174, 'SABE TUDO - EM \"Prof. Ary de Oliveira Seabra\"', 'Rua João Granado', '45', 'Jardim Eliana', NULL, NULL, NULL, '15', '32254879', '-23.393243', '-47.379386', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(175, 'SABE TUDO - EM \"Avelino Leite de Camargo\"', 'Avenida Monsenhor Mario Calazanz', '133', 'Jardim Nova Esperança', '18061411', NULL, NULL, '15', '32175877', '-23.489208', '-47.489782', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(176, 'SABE TUDO - EM \"Prof. Basílio da Costa Daemon\"', 'Rua Ataliba Pontes', 's/n', 'Paineiras', '18078613', NULL, NULL, '15', '32393424', '-23.443424', '-47.474596', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(177, 'SABE TUDO - EM \"Prof. Benedicto José Nunes\"', 'Rua Padre Domenico Trivi', '129', 'Parque Esmeralda', '18055745', NULL, NULL, '15', '34171133', '-23.491958', '-47.495138', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(178, 'SABE TUDO - EM \"Duljara Fernandes de Oliveira\"', 'Rua Móbil Lopes de Oliveira', '51', 'Jardim Santo Amaro', '18074681', NULL, NULL, '15', '32394837', '-23.452251', '-47.492530', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(179, 'SABE TUDO - EM \"Prof. Flávio de Souza Nogueira\"', 'Rua Benedito Galdino de Barros', '47', 'Jardim Ferreira', '18080660', NULL, NULL, '15', '32110485', '-23.481460', '-47.460925', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(180, 'SABE TUDO - EM \"Profa. Genny Kalil Milego\"', 'Rua Vanderlei Felício', '215', 'Herbert de Souza', '18079025', NULL, NULL, '15', '32393631', '-23.434532', '-47.475819', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(181, 'SABE TUDO - EM \"Profa. Inês Rodrigues Cesarotti\"', 'Rua Maria Moreno Trugillano', 's/n', 'Jardim Bonsucesso', '18078430', NULL, NULL, '15', '32393988', '-23.428785', '-47.461862', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(182, 'SABE TUDO - EM \"Prof. Irineu Leister\"', 'Rua Odete Nanci Giraldi', '67', 'Jardim Ipiranga', '18055004', NULL, NULL, '15', '32171484', '-23.499121', '-47.508706', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(183, 'SABE TUDO - EM \"Prof. José Carlos Florenzano\"', 'Rua Vitor Cioffi de Luca', '800', 'Jardim Santa Esmeralda', NULL, NULL, NULL, NULL, NULL, '-23.433809', '-47.485891', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(184, 'SABE TUDO - EM \"José Mendes\"', 'Rua Armando Rizzo', '558', 'Jardim Hungarês', '18075501', NULL, NULL, '15', '32394774', '-23.470828', '-47.468707', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(185, 'SABE TUDO - EM \"Profa. Léa Edy Alonso Siliba\"', 'Rua Miguel Stefani', 's/n', 'Jardim Marcelo Augusto', '18071297', NULL, NULL, '15', '32131476', '-23.474012', '-47.499205', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(186, 'SABE TUDO - EM \"Prof. Luiz Almeida Marins\"', 'Avenida Américo Figueiredo', '3500', 'Júlio de Mesquita Filho', '18053000', NULL, NULL, '15', '32179038', '-23.503962', '-47.514767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(187, 'SABE TUDO - EM \"Profa. Maria de Lourdes Ayres de Moraes\"', 'Rua Vicente Miranda', '300', 'Jardim Santa Marina', '18035240', NULL, NULL, '15', '32395461', '-23.445622', '-47.469311', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(188, 'SABE TUDO - EM \"Profa. Maria de Lourdes Martins Martinez\"', 'Rua Daria Galvão da Silva', '292', 'Jardim Santa Bárbara', '18053368', NULL, NULL, '15', '32021152', '-23.499233', '-47.522985', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(189, 'SABE TUDO - EM \"Profa. Maria Domingas Tótora de Goes\"', 'Rua Laurindo de Brito', '180', 'Vila Carol', '18070295', NULL, NULL, '15', '32332320', '-23.476053', '-47.475754', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(190, 'SABE TUDO - EM \"Prof. Oswaldo de Oliveira\"', 'Rua Flor do Carvalho', '929', 'Jardim Jatobá', '18103125', NULL, NULL, '15', '32026132', '-23.410457', '-47.414304', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(191, 'SABE TUDO - EM \"Dr. Oswaldo Duarte\"', 'Rua Luiz Gabriotti', '213', 'Wanel Ville II', '18055089', NULL, NULL, '15', '33252215', '-23.488365', '-47.505804', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(192, 'SABE TUDO - EM \"Quinzinho de Barros\"', 'Rua Joaquim Rodrigues de Barros', '477', 'Vila Hortência', '18020282', NULL, NULL, '15', '32274050', '-23.509744', '-47.432734', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(193, 'SABE TUDO - EM \"Tereza Ciambelli Gianini\"', 'Avenida Rio Claro', '350', 'Nova Sorocaba', '18070710', NULL, NULL, '15', '32131452', '-23.468990', '-47.482927', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(194, 'SABE TUDO - EM \"Prof. Walter Carretero\"', 'Avenida Itavuvu', '7000', 'Conj. Hab. Ana Paula Eleutério', '18078005', NULL, NULL, '15', '32394843', '-23.423406', '-47.482412', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(195, 'SABE TUDO - EM \"Profa. Zilah Dias de Mello Schrepel\"', 'Rua Duvalino Manfio', '264', 'Jardim Santo André', '18077345', NULL, NULL, '15', '32393408', '-23.457813', '-47.466715', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(196, 'SABE TUDO - EE \"Altamir Gonçalves\"', 'Rua Assis Chateaubriand', '70', 'Jardim Belmejo', '18044440', NULL, NULL, '15', '32178504', '-23.510892', '-47.480100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(197, 'SABE TUDO - EE \"Antonio Cordeiro\"', 'Rua Bernardino Albiero', 's/n', 'Parque das Laranjeiras', '18077400', NULL, NULL, '15', '33025052', '-23.454320', '-47.475732', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(198, 'SABE TUDO - EE \"Antonio Miguel Pereira Júnior\"', 'Rua Érico Veríssimo', '1050', 'Central Parque', '18051100', NULL, NULL, '15', '32178825', '-23.517166', '-47.506230', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(199, 'SABE TUDO - EE \"Brigadeiro Tobias\"', 'Rua Azevedo Figueiredo', '21', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32366313', '-23.510039', '-47.361132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(200, 'SABE TUDO - EE \"Dulce Esmeralda Basile Ferreira\"', 'Avenida Vinicius de Moraes', 's/n', 'Parque São Bento', '18072060', NULL, NULL, '15', '32132099', '-23.440517', '-47.505950', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(201, 'SABE TUDO - EE \"Enéas Proença de Arruda\"', 'Rua Dr. Fernando Santos', '345', 'Jardim Maria do Carmo', '18081200', NULL, NULL, '15', '32339101', '-23.475637', '-47.444779', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(202, 'SABE TUDO - EE \"Humberto de Campos\"', 'Rua Humberto de Campos', '729', 'Jardim Zulmira', '18061000', NULL, NULL, '15', '32179633', '-23.499093', '-47.483560', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(203, 'SABE TUDO - EE \"Ida Yolanda Lanzoni de Barros\"', 'Rua Sidnéia Antonio Urban', '123', 'Vila Zacarias', '18022320', NULL, NULL, '15', '32339085', '-23.524520', '-47.435535', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(204, 'SABE TUDO - EE \"Marco Antonio Mencacci\"', 'Estrada da Rancharia', '330', 'Jardim Josane', NULL, NULL, NULL, '15', '32254490', '-23.451447', '-47.372787', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(205, 'Oficina do Saber - EM \"Prof. Edemir Antonio Digiampietri\"', 'Avenida Governador Mário Covas', '690', 'Vila Barão', NULL, NULL, NULL, '15', '32025489', '-23.491598', '-47.485473', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(206, 'Oficina do Saber - EM \"Bairro do Éden\"', 'Rua Salvador Leite Marques', '1030', 'Éden', '18103050', NULL, NULL, '15', '32135877', '-23.415225', '-47.412439', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(207, 'Oficina do Saber - EM \"Profa. Genny Kalil Milego\"', 'Rua Maria Dolores Moron Vieira', '399', 'Herbert de Souza', '18079024', NULL, NULL, '15', '32398967', '-23.434286', '-47.477583', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(208, 'Oficina do Saber - EM \"Dr. Hélio Rosa Baldy\"', 'Rua Darcy Landulfo', '698', 'Jardim São Guilerme II', '18074642', NULL, NULL, '15', '32398967', '-23.447643', '-47.488768', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(209, 'Oficina do Saber - EM \"Profa. Léa Edy Alonso Siliba\"', 'Rua Miguel Stefan', '63', 'Jardim Marcelo Augusto', '18071195', NULL, NULL, '15', '32135998', '-23.475395', '-47.498368', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(210, 'Oficina do Saber - EM \"Profa. Léa Edy Alonso Siliba\"', 'Rua Alpheu Castro Santos', 's/n', 'Jardim Rodrigo', NULL, NULL, NULL, NULL, NULL, '-23.471433', '-47.506619', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(211, 'Oficina do Saber - EM \"Leda Therezinha Borghesi Rodrigues\"', 'Avenida Ipanema', '5515', 'Jardim Ipanema Ville', '18071801', NULL, NULL, '15', '32275910', '-23.453515', '-47.508360', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(212, 'Oficina do Saber - EM \"Prof. Luiz de Almeida Marins\"', 'Rua Elisete Cardoso', '97', 'Julio de Mesquita', '18053091', NULL, NULL, '15', '32025006', '-23.504748', '-47.514866', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(213, 'Oficina do Saber - EM \"Profa. Maria de Lourdes Ayres de Moraes\"', 'Rua Deodário Alves da Silva', '539', 'Parque das Paineiras', '18078610', NULL, NULL, '15', '32263829', '-23.443729', '-47.467820', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(214, 'Oficina do Saber - EM \"Profa. Maria de Lourdes Martins Martinez\"', 'Rua José Leite do Canto Junior', '605', 'Jardim Santa Bárbara', '18053374', NULL, NULL, '15', '32026400', '-23.494613', '-47.523091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(215, 'Oficina do Saber - EM \"Profa. Maria Ignez Figueiredo Deluno\"', 'Rua Antero José da Rosa', 's/n', 'Mineirão', '18076481', NULL, NULL, '15', '32332597', '-23.466380', '-47.464727', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(216, 'Oficina do Saber - EM \"Profa. Norma Justa Dall\'ara\"', 'Rua José Batista de Camargo', '10', 'Jardim Bonsucesso', NULL, NULL, NULL, '15', '33024172', '-23.426795', '-47.460596', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(217, 'Oficina do Saber - EM \"Prof. Paulo Fernando Nóbrega Tortello\"', 'Rua Vidal de Oliveira', '55', 'Laranjeiras', '18077387', NULL, NULL, '15', '32391110', '-23.452691', '-47.471104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(218, 'Oficina do Saber - EM \"Tereza Ciambelli Gianini\"', 'Rua Pedro Moreira César', 's/n', 'Jardim Los Angeles', '18074070', NULL, NULL, '15', '32392819', '-23.467502', '-47.481313', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(219, 'Oficina do Saber - EM \"Prof. Walter Carretero\"', 'Rua Eugênio Carlos Mendes', 's/n', 'Conj. Hab. Ana Paula Eleutério', NULL, NULL, NULL, '15', '32395077', '-23.423729', '-47.482807', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(220, 'Espaço Escola Saudável', 'Rua Campinas', '140', 'Jardim Iguatemi', '18085400', NULL, NULL, '15', '32282165', '-23.473593', '-47.439627', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(221, 'Espaço Frei Galvão', 'Rua Frei Galvão', '229', 'Vila Santana', '18055270', NULL, NULL, '15', '32344161', '-23.486401', '-47.454028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(222, 'Oficina Pedagógica', 'Rua Constantino Spanghero', 's/n', 'Vila Rica', '18013280', NULL, NULL, '15', '32283578', '-23.485596', '-47.430962', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(223, 'Centro de Referência Educacional', 'Rua Arthur Caldini', '211', 'Jardim Saira', '18085050', NULL, NULL, '15', '32342004', '-23.478277', '-47.437388', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(224, 'Espaço Chile', 'Rua Chile', 's/n', 'Barcelona', NULL, NULL, NULL, NULL, NULL, '-23.516165', '-47.438272', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4),
(225, 'Centro Esportivo \"Francisco Lisboa\" – Jd. Maria Eugênia', 'Rua Mario Romano', 's/n', 'Jardim Maria Eugenia', '18074621', NULL, NULL, '15', '32263108', '-23.456372', '-47.492293', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(226, 'Centro Esportivo “Comendador Genésio Rodrigues” – Central Parque', 'Rua Mário Soave', 's/n', 'Central Parque', NULL, NULL, NULL, '15', '32210722', '-23.511882', '-47.503071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(227, 'Centro Esportivo “Armando Bacelli” – Vila Gabriel', 'Rua Joaquim Ferreira Barbosa', 's/n', 'Vila Gabriel', '18081080', NULL, NULL, '15', '32244860', '-23.478532', '-47.452658', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(228, 'Almoxarifado da SEMES', 'Avenida Carlos Reinaldo Mendes', '3041', 'Alto da Boa Vista', '18013280', NULL, NULL, '15', '32382316', '-23.478870', '-47.424099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(229, 'Centro Inclusivo “Aluisio de Almeida” - CIEL', 'Rua Deodoro Reis', '210', 'Vila Santana', '18080704', NULL, NULL, '15', '32343406', '-23.487734', '-47.459448', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6);
INSERT INTO `proprios` (`id`, `nome`, `endereco`, `numero`, `bairro`, `cep`, `horario_abert`, `horario_fech`, `ddd`, `telefone`, `latitude`, `longitude`, `pavimentos`, `data_construcao`, `area_const`, `validade_avcb`, `publico_alvo`, `benif_manha`, `benif_tarde`, `benif_noite`, `usuario_id`, `cidade_id`, `secretaria_id`) VALUES
(230, 'Centro Esportivo “André Matielo” - Pinheiros', 'Rua Padre Lara de Moraes', 's/n', 'Pinheiros', '18020220', '08:00', '17:00', '15', '32343580', '-23.508578', '-47.450635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(231, 'Centro Esportivo “Artidoro Mascarenhas” – Dr. Pitico', 'Rua Anselmo Rolim', 's/n', 'Vila Angélica', '18065450', NULL, NULL, '15', '32318612', '-23.479263', '-47.475962', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(232, 'Centro Esportivo “Joaquim Martins” – Brigadeiro Tobias', 'Avenida Bandeirantes', '3963', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32366277', '-23.507173', '-47.366889', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(233, 'Estádio Municipal “Walter Ribeiro”- CIC', 'Rua Pereira da Silva', 's/n', 'Santa Rosália', '18090000', NULL, NULL, '15', '32313069', '-23.490560', '-47.448633', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(234, 'Centro Esportivo ”Padre Pieroni” – CE Jd. Simus', 'Avenida Américo Figueiredo', '1200', 'Jardim Simus', '18055131', '06:00', '20:00', '15', '32227573', '-23.503651', '-47.495322', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(235, 'Ginásio Poliesportivo do Éden', 'Rua Salvador Leite Marques', '1030', 'Éden', '18103050', NULL, NULL, NULL, NULL, '-23.415517', '-47.412972', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(236, 'Ginásio Municipal de Esporte', 'Rua José Martins', 's/n', 'Além Ponte', NULL, NULL, NULL, '15', '32311345', '-23.504110', '-47.449601', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(237, 'Ginásio Nilton Torres \"Prof. Edson Antão de Souza\"', 'Rua Maria Raphaela Pagliucca', '155', 'Jardim Nilton Torres', NULL, NULL, NULL, '15', '32332021', '-23.404210', '-47.374266', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(238, 'Arena Sorocaba Eurydes Bertoni Júnior', 'Rodovia Raposo Tavares', 'Km 106', 'Ipanema das Pedras', NULL, NULL, NULL, '15', '57047018', '-23.525928', '-47.515793', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(239, 'Palacete Scarpa Gabinete da SEMES', 'Rua Souza Pereira', '448', 'Centro', '18010320', NULL, NULL, '15', '32127282', '-23.497311', '-47.454748', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6),
(240, 'Almoxerifado', 'Rua Paes de Linhares', '1763', 'Vila Fiori', '18075630', NULL, NULL, NULL, NULL, '-23.473146', '-47.456287', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(241, 'Barracão Cultural', 'Avenida Afonso Vergueiro', '310', 'Centro', '18035370', NULL, NULL, '15', '32110143', '-23.496078', '-47.455161', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(242, 'Biblioteca Infantil', 'Rua da Penha', '673', 'Centro', '18010000', '08:00', '17:00', '15', '32315723', '-23.500413', '-47.460495', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(243, 'Biblioteca Municipal de Sorocaba - Jorge Guilherme Senger', 'Rua Ministro Coqueijo Costa', '180', 'Alto da Boa Vista', '18013550', '08:00', '16:50', '15', '32281955', '-23.480470', '-47.422758', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(244, 'Casa Aluísio de Almeida', 'Rua Dr. Ruy Barbosa', '84', 'Vila Hortência', '18020040', '08:00', '17:00', '15', '32311669', '-23.501845', '-47.448394', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(245, 'Casa do Turista', 'Avenida Afonso Vergueiro', '310', 'Centro', '18035370', '08:00', '17:00', '15', '32332043', '-23.496224', '-47.454614', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(246, 'Casarão Brigadeiro Tobias', 'Rua Antônio Fratti', 's/n', 'Brigadeiro Tobias', NULL, '10:00', '16:00', '15', '32366002', '-23.506403', '-47.363424', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(247, 'Chalé Francês', 'Praça Maylask', 's/n', 'Centro', '18035370', NULL, NULL, '15', '32127280', '-23.496757', '-47.454929', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(248, 'Casa 52', 'Praça Maylask', 's/n', 'Centro', '18035370', NULL, NULL, NULL, NULL, '-23.496826', '-47.454391', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(249, 'Céu das Artes', 'Rua Whashington Pensa', 's/n', 'Parque das Laranjeiras', '18077580', NULL, NULL, NULL, NULL, '-23.450412', '-47.471367', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(250, 'Museu Estrada de Ferro', 'Rua Dr. Álvaro Soares', '553', 'Centro', NULL, '09:00', '16:30', '15', '32311026', '-23.496976', '-47.454131', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(251, 'Museu Histórico de Sorocaba', 'Rua Theodoro Kalsel', '883', 'Vila Hortência', '18020268', '09:00', '16:30', NULL, NULL, '-23.506849', '-47.437639', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(252, 'Parque dos Espanhóis', 'Rua Campos Sales', 's/n', 'Vila Assis', NULL, NULL, NULL, '15', '32339809', '-23.514841', '-47.444640', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(253, 'Teatro Municipal Teotonio Vilela', 'Avenida Eng. Carlos Reinaldo Mendes', 's/n', 'Alto da Boa Vista', '18013280', NULL, NULL, '15', '32382222', '-23.477161', '-47.423120', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7),
(254, 'Base da GCM', 'Avenida Vinícius de Moraes', '827', 'Parque São Bento', '18072060', NULL, NULL, NULL, NULL, '-23.434974', '-47.505760', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(255, 'Base da GCM', 'Rua Joaquim Ferreira Barbosa', '1431', 'Jardim Maria do Carmo', '18081085', NULL, NULL, NULL, NULL, '-23.471917', '-47.446690', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(256, 'Base da GCM', 'Rua Ramon Haro Martini', '1396', 'Vila Haro', NULL, NULL, NULL, NULL, NULL, '-23.504334', '-47.423655', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(257, 'Escola de Formação GCM', 'Rua Paes de Linhares', '1763', 'Vila Fiori', '18078630', NULL, NULL, NULL, NULL, '-23.472495', '-47.455876', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(258, 'Centro de Operações e Inteligência', 'Rua Castanho Taques', '100', 'Jardim Ana Maria', '18013280', NULL, NULL, '15', '32129400', '-23.485771', '-47.474955', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(259, 'Área de Fiscalização', 'Rua Pedro José Senger', '1396', 'Vila Haro', '18015000', NULL, NULL, NULL, NULL, '-23.503735', '-47.431940', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 8),
(260, 'Parque da Biquinha', 'Venida Comendador Pereira Inácio', '1112', 'Jardim Vergueiro', '18030005', '08:00', '17:00', '15', '32241997', '-23.515644', '-47.454779', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(261, 'Parque da Água Vermelha', 'Rua Romania', '150', 'Jardim Europa', '18045040', '08:00', '17:00', '15', '32216643', '-23.521266', '-47.488199', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(262, 'Parque Ouro Fino', 'Rua Alexandre Caldini', '265', 'Jardim Ouro Fino', '18055710', '08:00', '18:00', '15', '32228504', '-23.496618', '-47.504494', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(263, 'Parque Natural Chico Mendes', 'Avenida Três de Março', '1025', 'Alto da Boa Vista', '18087180', '08:00', '17:00', '15', '32281256', '-23.476660', '-47.412767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(264, 'Parque da Biodiversidade', 'Avenida Itavuvu', '9264', 'Jardim Santa Cecilia', NULL, '08:00', '17:00', '15', '32931103', '-23.390824', '-47.474681', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(265, 'Parque Porto das Águas', 'Avenida Quinze de Agosto', 's/n', 'Jardim Abaete', NULL, NULL, NULL, NULL, NULL, '-23.467595', '-47.445773', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(266, 'Centro de Educação Ambiental do Rio Sorocaba', 'Avenida Dom Aguirre', 's/n', 'Vila Matielo', NULL, NULL, NULL, NULL, NULL, '-23.506628', '-47.451529', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(267, 'Unidade de Controle Animal', 'Rua Rosa Maria de Oliveira', '345', 'Jardim Zulmira', '18061030', '09:00', '16:00', '15', '32222484', '-23.501408', '-47.483691', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(268, 'Jardim Botânico de Sorocaba Irmãos Villas-Bôas', 'Rua Miguel Montoro Lozano', '340', 'Jardim Iguatemi', '18085767', '09:00', '17:00', '15', '32351130', '-23.464969', '-47.438339', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(269, 'Parque Zoológico Municipal Quinzinho de Barros', 'Rua Teodoro Kaisel', '883', 'Vila Hortência', '18020268', '09:00', '16:00', '15', '32275454', '-23.506849', '-47.438075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(270, 'Escola de Gestão Pública', 'Avenida Rudolf Dafferner', '3043', 'Alto da Boa Vista', '18013280', NULL, NULL, '15', '32382426', '-23.474836', '-47.422904', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 11),
(271, 'Secretaria de Igualdade e Assistência Social', 'Rua Santa Cruz', '116', 'Centro', '18035630', '08:00', '17:00', '15', '32191920', '-23.504174', '-47.455950', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(272, 'CRAS Zona Oeste II – Nova Esperança', 'Rua Monsenhor Benedito Mário Calazans', '15', 'Jardim Nova Esperança', '18061441', '07:00', '17:00', '15', '32216108', '-23.491429', '-47.491397', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(273, 'CRAS Zona Oeste I – Ipiranga', 'Rua Santo Micheletti', '30', 'Jardim Ipiranga', '18055007', '08:00', '17:00', '15', '32028138', '-23.499585', '-47.508737', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(274, 'CRAS Zona Norte II – Vila Helena', 'Rua Riusaku Kanizawa', '376', 'Vila Helena', '18071284', '08:00', '17:00', '15', '32136204', '-23.467065', '-47.499195', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(275, 'CRAS Parque São Bento', 'Rua Otacilio Vieira', '107', 'Parque São Bento', '18072330', '08:00', '17:00', '15', '32135216', '-23.431536', '-47.508631', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(276, 'CREAS Oeste', NULL, NULL, NULL, NULL, NULL, NULL, '15', '32115070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(277, 'CRAS Ana Paula Eleutério', 'Rua Prof. Horacio Blaseck', 's/n', 'Conj. Hab. Ana Paula Eleutério', NULL, NULL, NULL, '15', '32264687', '-23.422555', '-47.482694', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(278, 'CRAS Zona Norte I – Laranjeiras', 'Rua Washington Pensa', 's/n', 'Parque das Laranjeiras', '18077580', NULL, NULL, '15', '32393867', '-23.450051', '-47.475796', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(279, 'CREAS Norte', 'Rua Dr. Fernando dos Santos', '17', 'Jardim Maria do Carmo', '18081170', '08:00', '17:00', '15', '32235319', '-23.478080', '-47.446398', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(280, 'Centro de Referência do Idoso - Chácara do Idoso', 'Rua Manoel Afonso', '64', 'Vila Progresso', '18090550', NULL, NULL, '15', '33023737', '-23.481090', '-47.449464', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(281, 'CRAS Brigadeiro Tobias', 'Avenida Bandeirantes', '3835', 'Brigadeiro Tobias', NULL, '08:00', '17:00', '15', '32367040', '-23.507345', '-47.367694', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(282, 'CRAS Aparecidinha', 'Rua do Terço', '100', 'Aparecidinha', NULL, NULL, NULL, '15', '33253956', '-23.443379', '-47.373311', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(283, 'CRAS Cajuru', 'Rua Jorge Elias', '42', 'Cajuru', NULL, NULL, NULL, '15', '32332928', '-23.398400', '-47.376947', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(284, 'CRAS João Romão', 'Rua Adelino Scarpa', '60', 'Jardim João Romão', NULL, NULL, NULL, '15', '32111986', '-23.521397', '-47.434409', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(285, 'CREAS Sul/Leste', 'Rua Santa Cruz', '116', 'Centro', '18035630', '08:00', '17:00', '15', '32191920', '-23.504206', '-47.455818', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(286, 'CEREM - Centro de Referência da Mulher Selma Said', 'Avenida Pres. Juscelino Kubitschek de Oliveira', '440', 'Centro', '18035060', NULL, NULL, '15', '32356770', '-23.505867', '-47.456154', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(287, 'Centro POP', 'Avenida Comendador Pereira Inácio', '763', 'Jardim Sandra', '18030005', '08:00', '17:00', '15', '32296690', '-23.512638', '-47.455530', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(288, 'Conselho Tutelar', 'Rua Libero Badaró', '141', 'Jardim Vergueiro', '18030060', '08:00', '17:00', '15', '32351212', '-23.508518', '-47.457461', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(289, 'Clube do Idoso', 'Rua Padre Lara de Moraes', 's/n', 'Vila Pinheiros', '18020220', NULL, NULL, '15', '32339592', '-23.509226', '-47.451522', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(290, 'CIAPCD', 'Rua João Gabriel Mendes', '351', 'Vila Gabriel', '18081020', NULL, NULL, '15', '32244636', '-23.480334', '-47.451890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(291, 'Território Jovem Ana Paula Eleutério', 'Avenida Itavuvu', '7435', 'Altos do Itavuvu', NULL, NULL, NULL, NULL, NULL, '-23.421242', '-47.481797', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(292, 'Território Jovem Cajuru', 'Rua Pedro Natividade da Silva', '209', 'Vila dos Dálmatas', NULL, NULL, NULL, NULL, NULL, '-23.396048', '-47.374816', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(293, 'Território Jovem Ipiranga', 'Rua Elias Maluf', '1080', 'Vila Wanel Ville', '18055215', NULL, NULL, NULL, NULL, '-23.498853', '-47.507485', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(294, 'Território Jovem João Romão', 'Rua Adelino Scarpa', '60', 'Vila João Romão', NULL, NULL, NULL, NULL, NULL, '-23.521375', '-47.434332', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(295, 'Território Jovem Maria Eugênia', 'Rua Sotero José do Bonfim', '171', 'Jardim Maria Eugênia', '18074450', NULL, NULL, '15', '32231450', '-23.456692', '-47.489406', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(298, 'Teste', 'teste', 'teste', 'teste', NULL, 'teste', 'teste', 'teste', 'teste', '12.000000', '333.000000', 2, NULL, 2, '0000-00-00', '1', 1, 1, 1, 19, 1, 13),
(299, 'aa', 'a', 'a', 'a', NULL, '12:30', '12:31', '12', '11111', '11.000000', '11.000000', 111, NULL, 12, '0000-00-00', '1', 1, 1, 1, 30, 1, 19);

-- --------------------------------------------------------

--
-- Estrutura para tabela `secretarias`
--

CREATE TABLE `secretarias` (
  `id` int(6) NOT NULL,
  `nome` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `abreviatura` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `secretarias`
--

INSERT INTO `secretarias` (`id`, `nome`, `abreviatura`, `usuario_id`) VALUES
(2, 'Secretaria de Conservação, Serviços Públicos e Obras', 'SERPO', NULL),
(3, 'Secretaria de Cidadania e Participação Popular', 'SECID', NULL),
(4, 'Secretaria da Educação', 'SEDU', NULL),
(6, 'Secretaria de Esportes e Lazer', 'SEMES', NULL),
(7, 'Secretaria de Cultura e Turismo', 'SECULTUR', NULL),
(8, 'Secretaria de Segurança e Defesa Civil', 'SESDEC', NULL),
(9, 'Secretaria da Saúde', 'SES', NULL),
(10, 'Secretaria de Meio Ambiente, Parques e Jardins', 'SEMA', NULL),
(11, 'Secretaria de Recursos Humanos', 'SERH', NULL),
(12, 'Secretaria de Igualdade e Assistência Social', 'SIAS', NULL),
(13, 'Secretaria de Habitação e Regularização Fundiária', 'SEHAB', NULL),
(14, 'Secretaria de Abastecimento, Agricultura e Nutrição', 'SEABAN', NULL),
(15, 'Secretaria de Assuntos Jurídicos e Patrimoniais', 'SAJ', NULL),
(16, 'Secretaria de Comunicação e Eventos', 'SECOM', NULL),
(17, 'Secretaria de Desenvolvimento Econômico, Trabalho e Renda', 'SEDETER', NULL),
(18, 'Secretaria da Fazenda', 'SEFAZ', NULL),
(19, 'Secretaria de Gabinete Central', 'SGC', NULL),
(20, 'Secretaria de Licitações e Contratos', 'SELC', NULL),
(21, 'Secretaria da Mobilidade e Acessibilidade', 'SEMOA', NULL),
(22, 'Secretaria de Planejamento e Projetos', 'SEPLAN', NULL),
(23, 'Secretaria de Saneamento', 'SESAN', NULL),
(24, 'Secretaria de Relações Institucionais e Metropolitanas', 'SERIM', NULL),
(25, 'Fundo Social de Solidariedade', 'FSS', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `terceirizadas`
--

CREATE TABLE `terceirizadas` (
  `id` int(6) NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `terceirizadas`
--

INSERT INTO `terceirizadas` (`id`, `nome`, `cnpj`, `ddd`, `telefone`, `email`, `usuario_id`) VALUES
(6, 'Teste LTDA', '3456649777665', '15', '234561234', NULL, 171);

-- --------------------------------------------------------

--
-- Estrutura para tabela `trabalhadores`
--

CREATE TABLE `trabalhadores` (
  `id` int(6) NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipe_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `trabalhadores`
--

INSERT INTO `trabalhadores` (`id`, `cpf`, `nome`, `ddd`, `telefone`, `equipe_id`) VALUES
(1, '35284888081', 'Lucas da Silva e Silva', '15', '996542123', 5),
(2, '49983428008', 'Samuel Torres', '15', '998652342', 5),
(3, '41836514093', 'Juvenal Eloy', '15', '981457862', NULL),
(4, '91166988007', 'Bob Grey', '11', '981256348', NULL),
(5, '94623719030', 'João Cavalcante', '15', '998301147', NULL),
(6, '42628152002', 'José Cravo', '15', '986547215', NULL),
(7, '01177728010', 'Matheus Leite', '15', '996257841', 5),
(8, '93903062057', 'Gabriel da Silva', NULL, '994253210', NULL),
(9, '30975138090', 'Heitor Gomes', '15', '30315420', NULL),
(10, '28173029075', 'Aiton da Silva', '11', '981546774', NULL),
(11, '73613928060', 'Roberto Luz', '15', '30345012', NULL),
(12, 'CPF Criptografado', 'Josevaldo Andrade de Camargo', '15', '981547854', NULL),
(13, '123', 'teste', '', '', 5),
(14, '123', 'Teste', '13', '11111111', 5),
(15, '123', 'Teste', '13', '321', NULL),
(16, '2472947198247987', 'JOAO', '15', '1532364197', 23);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(6) NOT NULL,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` int(4) NOT NULL,
  `senha` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `email`, `ddd`, `telefone`, `tipo`, `senha`) VALUES
(19, 'Bárbara Braz', 'CPF', 'bb@email.com', '15', '998290695', 1, ''),
(20, 'Rafael Ricardo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(21, 'Rafaela Talita Leonardi', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(22, 'Amauri de Carvalho', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(23, 'Elaine Aparecida Ferreira Marfil Sposito', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(24, 'Ana Maria de Almeira Alves', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(25, 'Luciana Maria Bálsamo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(26, 'Patricia Cistina Camargo Fernandez', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(27, 'Sônia Maria Carvalho de Sanctis Garcia', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(28, 'Eliana Cristina Guilherme Rubinatto', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(29, 'Thais Bragatto Arten', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(30, 'Sandra Cristina Honofre Belloto', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(31, 'Angélica Rodrigues Correa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(32, 'Maria Jose Quito Oliveira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(33, 'Elaine Cristina de Matos Fernandez', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(34, 'Edileine Regina Rodrigues Dias', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(35, 'Kátia Regina Pereira Puglia', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(36, 'Cristiane Aparecida Evangelista Gusmão', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(37, 'Márcia Regina Andrade', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(38, 'Cristina Fernandes Ribeiro', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(39, 'Eliana de Mello Marques Braile', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(40, 'Silvana Aparecida de Moraes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(41, 'Ione Aparecida Moreira Xavier de Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(42, 'Luciane Manes de Miranda', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(43, 'Andrea Silva da Costa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(44, 'Jacqueline Herrera Estebam Lobo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(45, 'Daniela Gaete Sewaybricker Bravo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(46, 'Ursula Jacinto Medeiros', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(47, 'Joyce de Oliveira Campos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(48, 'Luzia Cristina Simplício Moura', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(49, 'Renata Pereira Proença Rossi', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(50, 'Gisele Karin de Moraes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(51, 'Marcelo Carvalho Vieira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(52, 'Antonio de Deus', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(53, 'Pedro Gomes Lima', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(54, 'Roseli Gonçalves Ribeiro Martins Garcia', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(55, 'Amanda Regina Martins Dias', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(56, 'Karen da Silva Pretél Fernandes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(57, 'Daniela Harder Oliveira Tersi', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(58, 'Dorcas Theodoro dos Santos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(59, 'Ednilson Celestino Ferreira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(60, 'Ademar Benedito Ribeiro da Mota', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(61, 'Kária Regina Pereira Inácio Gomes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(62, 'Ana Lúcia Castelo Altino', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(63, 'Ariadne de Barros Oliveira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(64, 'Elisabeth de Souza Martinelli da Silva Filha', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(65, 'Graziele Cristina Poveda Narvarro', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(66, 'Agda Cristina Fogaça Zuliani', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(67, 'Mara Irani Souza Branco', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(68, 'Sandra Cardoso', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(69, 'Vanessa Nascimento da Silva', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(70, 'Ida Magali Blaz Martinez', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(72, 'Adriana Ricardo da Mota Almeida', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(73, 'Márcia Maria Zoranz Armelin Meira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(74, 'Denise Cavalcante Silva Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(75, 'Sandra Maria Rodrigues Teixeira Callado', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(76, 'Sidneia Aparecida Castilho', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(77, 'Raquel Barros Mariano', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(78, 'Gisele Cristina de Almeida Santos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(79, 'Selma Lezier Rodrigues', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(80, 'Sandra Martelli de Albuquerque do Carmo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(81, 'Erika Beatriz Ramos Ferreira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(82, 'Petula Ramanauskas Santorum e Silva', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(83, 'Carla Duarte Sa Lemos Anacleto', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(84, 'Danieli Casare da Silva Moreira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(85, 'Isabel Cristina de Campos Borges', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(86, 'Fabiana Aparecida Pereira Jochi', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(87, 'Cristiane Aoad Machado de Oliveira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(88, 'Ana Lucia de Jesus Cerqueira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(89, 'Elaine Cristina Marcelino dos Santos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(90, 'Leila Regina Cintra Aoki', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(91, 'Raquel Aparecida de Almeira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(92, 'Edneia Maria Facci dos Santos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(93, 'Gláucia Renata Albarossi Scarpelli', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(94, 'Daniela Santos Augusto Stievano', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(95, 'Rosemary Serrão de Oliveira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(96, 'Silvana da Silveira Monteiro Yonashiro', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(97, 'Maria Aparecida de Moraes Cajueiro de Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(98, 'Luis Calos da Silva', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(99, 'Aretha Fabiana do Amaral Feliciano', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(100, 'Denise Aparecida Nunes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(101, 'Cleusa Ferreira de Souza Fernandes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(102, 'Viviane Anesia Bueno', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(103, 'Claudia Maria de Oliveira Neves', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(104, 'Patricia Lara Camargo', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(105, 'Lucia Florindo da Silva Mariano', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(106, 'Selma Almeida Costa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(107, 'Ana Carolina Araújo Sanches do Nascimento', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(108, 'Marcia Pereira Gomes de Mello', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(109, 'Tereza de Jesus Aparecida R. R. Silva', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(110, 'Alessandra Cirilo Nunes', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(111, 'Ana Lúcia da Silva Pereira Acosta', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(112, 'Vania Francine Martins dos Santos Costa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(113, 'Elaine Martiniuk', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(114, 'Marcelino de Almeida', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(115, 'Paulo Rogério Balbino Venancio', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(116, 'Andreia Alves da Silva Pereira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(117, 'Elaine Ortiz Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(118, 'Silvana Gabriel Correa de Miranda', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(119, 'Daniela Marques de Barros Renna', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(120, 'Wagner Luiz Paes Coelho', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(121, 'Sandro Ivo de Meira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(122, 'Iara Paques Guedes Prohaska', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(123, 'Lilian Aparecida dos Santos de Pádua Carneiro', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(124, 'Maria Selma de Campos Mariz', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(125, 'Alaine Cristiane de Salles', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(126, 'Antônio Avelino Minhano Alves', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(127, 'Maria Aparecida de O. Duarte', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(128, 'Janaína Roberta Petronilha dos Santos Faria', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(129, 'Andréa Picanço Souza Tichy', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(130, 'Edlaine Gonçalves Fernades Garcia', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(131, 'Roberto Martinez', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(132, 'José Adão Neres de Jesus', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(133, 'Aparecida de Fátima Pereira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(134, 'Elaine Cristina Faustinoni da Silva', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(135, 'Andrea Edwiges Gomes Proença', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(136, 'Odirlei Paulino dos Santos', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(137, 'Lilian Cristina de Freitas Cavalcante', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(138, 'Roseli Marli Laprano Zuliani', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(139, 'Ana Cláudia Joaquim', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(140, 'Lucibel Newman Correa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(141, 'Bianca da Silva Leite Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(142, 'Daniel Tadeu Moreira S. Merlin', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(143, 'Sandra de Fátima Assis', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(144, 'Vanessa Baccelli Michelacci de Almeida', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(145, 'Patrícia Banietti Rosa', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(146, 'Ana Paula de Oliveira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(147, 'Eder Rodrigues Proença', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(148, 'Francine Alessandra Gracia Menna', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(149, 'Ana Paula Rodrigues Sanches', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(150, 'Mario Aparecido de Lima', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(151, 'Ana Maria Souza Claro', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(152, 'Cristiane Andrade Vieira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(153, 'Janaína Santos de Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(154, 'Maria Aparecida de Lima Madureira', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(155, 'Patricia Aparecida Moron Dipsie', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(156, 'Carla Aparecida Nani Vieira Flório', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(157, 'Francesli Esposito Vieira Alves', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(158, 'Valderez Luci Moreira Vieira Soares', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(159, 'Alessandra Rosa Vieira de Souza', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(160, 'Karine Cristiane C. B. Del Cistia', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(161, 'Gláucia Amendola F. Davanso', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(162, 'Renata Cristina Silvano', 'CPF', NULL, NULL, NULL, 3, 'Senha'),
(163, 'Tester', '123', 'teste@teste.com', '15', '11111111', 1, '88ddad224bf256869af5e4bfdee65192'),
(165, 'Responsável Geral', '46141760031', 'responsavel.geral@email.com', '15', '998101010', 1, '88ddad224bf256869af5e4bfdee65192'),
(166, 'Responsável da Secretaria', '45273862043', 'responsavel.secretaria@email.com', '15', '998202020', 2, 'eeba4c30a59511ec8b3eb02833bbd585'),
(167, 'Responsável Local', '18901228076', 'responsavel.local@email.com', '15', '998303030', 3, 'e023f220dcde869e03490ad43ff23461'),
(168, 'Responsável da Equipe Própria', '71316807029', 'responsavel.equipe@email.com', '15', '998404040', 4, '8fc8825c7da98a625b6a6075e0fe7ba0'),
(169, 'Responsável de Terceirizada', '94455191089', 'responsavel.terceirizada@email.com', '15', '998505050', 5, 'b3210a726affa9aa178759423736ced4'),
(171, 'Jose Lucas Oliveira', '430.374.008-03', 'jlucaslima1996@gmail.com', '15 99', '15998037196', 1, 'cd4bcb67fef253058d693207b5fb630f');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_chamados_proprios_idx` (`proprio_id`),
  ADD KEY `fk_chamados_palavras_chaves_idx` (`palavra_chave_id`);

--
-- Índices de tabela `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cidades_estados_idx` (`estado_id`);

--
-- Índices de tabela `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contratos_terceirizadas_idx` (`terceirizada_id`),
  ADD KEY `fk_contratos_chamados_idx` (`chamado_id`);

--
-- Índices de tabela `equipes`
--
ALTER TABLE `equipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipes_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `equipes_propria`
--
ALTER TABLE `equipes_propria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipes_propria_chamados_idx` (`chamado_id`),
  ADD KEY `fk_equipes_propria_equipes_idx` (`equipe_id`);

--
-- Índices de tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `itens_chamado`
--
ALTER TABLE `itens_chamado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_itens_realizados_chamados_idx` (`chamado_id`);

--
-- Índices de tabela `ocorrencias`
--
ALTER TABLE `ocorrencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ocorrencias_chamados_idx` (`chamado_id`),
  ADD KEY `fk_ocorrencias_palavras_chaves_idx` (`palavra_chave_id`),
  ADD KEY `fk_ocorrencias_proprios_idx` (`proprio_id`);

--
-- Índices de tabela `palavras_chave`
--
ALTER TABLE `palavras_chave`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `proprios`
--
ALTER TABLE `proprios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proprios_cidades_idx` (`cidade_id`),
  ADD KEY `fk_proprios_usuarios_idx` (`usuario_id`),
  ADD KEY `fk_proprios_secretarias_idx` (`secretaria_id`);

--
-- Índices de tabela `secretarias`
--
ALTER TABLE `secretarias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_secretarias_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `terceirizadas`
--
ALTER TABLE `terceirizadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_terceirizadas_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `trabalhadores`
--
ALTER TABLE `trabalhadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_trabalhadores_equipes_idx` (`equipe_id`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `contratos`
--
ALTER TABLE `contratos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `equipes`
--
ALTER TABLE `equipes`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `equipes_propria`
--
ALTER TABLE `equipes_propria`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `itens_chamado`
--
ALTER TABLE `itens_chamado`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `ocorrencias`
--
ALTER TABLE `ocorrencias`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de tabela `palavras_chave`
--
ALTER TABLE `palavras_chave`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de tabela `proprios`
--
ALTER TABLE `proprios`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;

--
-- AUTO_INCREMENT de tabela `secretarias`
--
ALTER TABLE `secretarias`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de tabela `terceirizadas`
--
ALTER TABLE `terceirizadas`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `trabalhadores`
--
ALTER TABLE `trabalhadores`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `chamados`
--
ALTER TABLE `chamados`
  ADD CONSTRAINT `fk_chamados_palavras_chaves` FOREIGN KEY (`palavra_chave_id`) REFERENCES `palavras_chave` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_chamados_proprios` FOREIGN KEY (`proprio_id`) REFERENCES `proprios` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Restrições para tabelas `cidades`
--
ALTER TABLE `cidades`
  ADD CONSTRAINT `fk_cidades_estados` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `fk_contratos_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos_resp_terc` FOREIGN KEY (`terceirizada_id`) REFERENCES `terceirizadas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `equipes`
--
ALTER TABLE `equipes`
  ADD CONSTRAINT `fk_equipes_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `equipes_propria`
--
ALTER TABLE `equipes_propria`
  ADD CONSTRAINT `fk_equipes_propria_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipes_propria_equipes` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `itens_chamado`
--
ALTER TABLE `itens_chamado`
  ADD CONSTRAINT `fk_itens_chamado_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `ocorrencias`
--
ALTER TABLE `ocorrencias`
  ADD CONSTRAINT `fk_ocorrencias_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ocorrencias_palavras_chaves` FOREIGN KEY (`palavra_chave_id`) REFERENCES `palavras_chave` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ocorrencias_proprios` FOREIGN KEY (`proprio_id`) REFERENCES `proprios` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Restrições para tabelas `proprios`
--
ALTER TABLE `proprios`
  ADD CONSTRAINT `fk_proprios_cidades` FOREIGN KEY (`cidade_id`) REFERENCES `cidades` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proprios_secretarias` FOREIGN KEY (`secretaria_id`) REFERENCES `secretarias` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proprios_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `secretarias`
--
ALTER TABLE `secretarias`
  ADD CONSTRAINT `fk_secretarias_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `terceirizadas`
--
ALTER TABLE `terceirizadas`
  ADD CONSTRAINT `fk_terceirizadas_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `trabalhadores`
--
ALTER TABLE `trabalhadores`
  ADD CONSTRAINT `fk_trabalhadores_equipes` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
