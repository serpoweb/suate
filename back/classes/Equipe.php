<?php

class Equipe { 
	private $id;
	private $nome;
	private $usuario;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarId($equipe_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($equipe_id))
				$equipe_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM equipes WHERE id = :id;");
			$sql->bindValue(":id",$equipe_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return null;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `equipes`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `equipes` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `equipes` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function add(){
		try{
			$sql = $this->con->prepare("INSERT INTO equipes (nome, usuario_id) VALUES (:nome, :u_id);");
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->bindValue(":nome",$this->nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function atualizar(){
		try{
			$sql = $this->con->prepare("UPDATE equipes SET nome = :nome, usuario_id = :u_id WHERE id = :e_id;");
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->bindValue(":e_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":nome",$this->nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function deletar($equipe_id = null){
		try{
			if(is_null($equipe_id))
				$equipe_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM equipes WHERE id = :id;");
			$sql->bindValue(":id",$equipe_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	
	 
	 
	public function listarPorSecretaria($secretaria_id = null) {
		try{
			if(is_null($secretaria_id))
				return false;
			$sql = $this->con->prepare("SELECT eq.id, eq.nome, eq.usuario_id FROM equipes as eq, equipes_propria as eqp, chamados as c, proprios as p, secretarias as sec WHERE eqp.equipe_id = eq.id AND eqp.chamado_id = c.id AND c.proprio_id = p.id AND p.secretaria_id = sec.id AND sec.id = :id AND c.data_finalizacao IS NULL;");
			$sql->bindValue(':id',$secretaria_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			return false;
		}
	}
	 
	public function listar($equipe_id = null) {
		try{
			if(is_null($equipe_id))
				$equipe_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM equipes WHERE id = :id;");
			$sql->bindValue(':id',$equipe_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->fetch(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			return false;
		}
	}

	public function listarTrabalhandoNoMomento() {
		try{
			$sql = $this->con->prepare("SELECT eq.id, eq.nome, eq.usuario_id FROM equipes as eq, equipes_propria as eqp, chamados as c WHERE eqp.equipe_id = eq.id AND eqp.chamado_id = c.id AND c.data_finalizacao IS NULL;");
			$sql->execute();
			return $sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			return false;
		}
	}

	/**
	 * Get the value of usuario
	 */ 
	public function getUsuario()
	{
		return $this->usuario;
	}

	/**
	 * Set the value of usuario
	 *
	 * @return  self
	 */ 
	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;

		return $this;
	}

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
