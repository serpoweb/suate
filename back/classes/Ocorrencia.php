<?php

class Ocorrencia {
	private $id;
	private $situacao;
	private $cpf;
	private $descricao;
	private $foto;
	private $palavra_chave;
	private $chamado_id;
	private $proprio_id;
	private $con;

	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function adicionar() {
		try{
			$sql = $this->con->prepare("INSERT INTO ocorrencias (situacao, cpf, descricao, foto, palavra_chave_id, chamado_id, proprio_id) VALUES (:situacao, :cpf, :descricao, :foto, :pc_id, :c_id, :p_id);");
			$sql->bindValue(':p_id',$this->proprio_id,PDO::PARAM_INT);	
			$sql->bindValue(':c_id',$this->chamado_id);	
			$sql->bindValue(':pc_id',$this->palavra_chave,PDO::PARAM_INT);	
			$sql->bindValue(':foto',$this->foto);	
			$sql->bindValue(':descricao',$this->descricao);	
			$sql->bindValue(':cpf',$this->cpf);	
			$sql->bindValue(':situacao',$this->situacao);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function atualizar() {
		try{
			$sql = $this->con->prepare("UPDATE ocorrencias SET situacao = :situacao, cpf = :cpf, descricao = :descricao, palavra_chave_id = :pc_id, chamado_id = :c_id, proprio_id = :p_id WHERE id = :id;");
			$sql->bindValue(':id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':p_id',$this->proprio_id,PDO::PARAM_INT);	
			$sql->bindValue(':c_id',$this->chamado_id);	
			$sql->bindValue(':pc_id',$this->palavra_chave,PDO::PARAM_INT);				
			$sql->bindValue(':descricao',$this->descricao);	
			$sql->bindValue(':cpf',$this->cpf);	
			$sql->bindValue(':situacao',$this->situacao);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($ocorrencia_id = null) {
		try{
			if(is_null($ocorrencia_id))
				$ocorrencia_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM ocorrencias WHERE id = :id;");
			$sql->bindValue(':id',$ocorrencia_id,PDO::PARAM_INT);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function adicionarFoto() {
		try{
			$sql = $this->con->prepare('UPDATE ocorrencias SET foto = :foto WHERE id = :id;');	
			$sql->bindValue(':id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':foto',$this->foto);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
  public function adicionarChamado($chamado_id = null) {
		try{
      if(is_null($chamado_id))
          $chamado_id = $this->chamado_id;
			$sql = $this->con->prepare('UPDATE ocorrencias SET chamado_id = :c_id WHERE id = :id;');	
			$sql->bindValue(':id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':c_id',$chamado_id);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function adicionarPalavraChave($palavra_chave_id = null) {
		try{
			if(is_null($palavra_chave_id))
				$palavra_chave_id = $this->palavra_chave;
			$sql = $this->con->prepare("UPDATE ocorrencias SET palavra_chave_id = :pc_id WHERE id = :id;");	
			$sql->bindValue(':id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':pc_id',$palavra_chave_id);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return null;
		}
	}

	public function adicionarProprio($proprio_id = null) {
		try{
			if(is_null($proprio_id))
				$proprio_id = $this->proprio_id;
			$sql = $this->con->prepare("UPDATE ocorrencias SET proprio_id = :p_id WHERE id = :id;");	
			$sql->bindValue(':id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':p_id',$proprio_id,PDO::PARAM_INT);	
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `ocorrencias`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `ocorrencias` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `ocorrencias` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}

	public function listarPorSecretaria($secretaria_id = null) {
		try{
			$sql = $this->con->prepare("SELECT o.id, o.situacao, o.cpf, o.descricao, o.foto, o.palavra_chave_id, o.chamado_id, o.proprio_id FROM ocorrencias as o, proprios as p, secretarias as s WHERE o.proprio_id = p.id AND p.secretaria_id = s.id AND s.id = :id;");	
			$sql->bindValue(':id',$secretaria_id,PDO::PARAM_INT);	
			$sql->execute();
			$res = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function listar($ocorrencia_id = null) {
		try{
			if(is_null($ocorrencia_id))
				$ocorrencia_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM ocorrencias WHERE id = :id;");	
			$sql->bindValue(':id',$ocorrencia_id,PDO::PARAM_INT);	
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}
  public function listarPorCpf($cpf = null) {
		try{
			if(is_null($cpf))
				$cpf = $this->cpf;
			$sql = $this->con->prepare("SELECT * FROM ocorrencias WHERE cpf = :cpf;");	
			$sql->bindValue(':cpf',$cpf);	
			$sql->execute();
			$res = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function gerarChamado() {
		try{
			$sql = $this->con->prepare("UPDATE ocorrencias SET chamado_id = :c_id WHERE id = :o_id;");	
			$sql->bindValue(':o_id',$this->id,PDO::PARAM_INT);	
			$sql->bindValue(':c_id',$this->chamado_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function getProprio_id()
	{
		return $this->proprio_id;
	}

	public function setProprio_id($proprio_id)
	{
		$this->proprio_id = $proprio_id;

		return $this;
	}

	public function getChamado_id()
	{
		return $this->chamado_id;
	}

	public function setChamado_id($chamado_id)
	{
		$this->chamado_id = $chamado_id;

		return $this;
	}

	public function getPalavra_chave()
	{
		return $this->palavra_chave;
	}

	public function setPalavra_chave($palavra_chave)
	{
		$this->palavra_chave = $palavra_chave;

		return $this;
	}

	public function getFoto()
	{
		return $this->foto;
	}

	public function setFoto($foto)
	{
		$this->foto = $foto;

		return $this;
	}

	public function getDescricao()
	{
		return $this->descricao;
	}

	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;

		return $this;
	}

	public function getCpf()
	{
		return $this->cpf;
	}

	public function setCpf($cpf)
	{
		$this->cpf = $cpf;

		return $this;
	}

	public function getSituacao()
	{
		return $this->situacao;
	}

	public function setSituacao($situacao)
	{
		$this->situacao = $situacao;

		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
