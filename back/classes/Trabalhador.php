<?php
class Trabalhador {
	private $id;
	private $cpf; 
	private $nome;
	private $ddd;
	private $telefone;
	private $equipe;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function alocarParaEquipe() {
		try{
			$sql = $this->con->prepare("UPDATE trabalhadores SET equipe_id = :e_id WHERE id = :t_id;");	
			$sql->bindValue(":e_id",$this->equipe,PDO::PARAM_INT);
			$sql->bindValue(":t_id",$this->id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listar($trabalhador_id = null) {
		try{
			if($trabalhador_id == null)
				$trabalhador_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM trabalhadores WHERE id = :id;");	
			$sql->bindValue(":id",$trabalhador_id,PDO::PARAM_INT);
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionar() {
		try{
			$sql = $this->con->prepare("INSERT INTO trabalhadores (nome, cpf, ddd, telefone, equipe_id) VALUES (:nome, :cpf, :ddd, :telefone, :e_id);");	
			$sql->bindValue(":e_id",$this->equipe,PDO::PARAM_INT);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":cpf",$this->cpf);
			$sql->bindValue(":nome",$this->nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($trabalhador_id = null) {
		try{
			if(is_null($trabalhador_id))
				$trabalhador_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM trabalhadores WHERE id = :id;");	
			$sql->bindValue(":id",$trabalhador_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function atualizar() {
		try{
			$sql = $this->con->prepare("UPDATE trabalhadores SET nome = :nome, cpf = :cpf, ddd = :ddd, telefone = :telefone, equipe_id = :e_id WHERE id = :t_id;");	
			$sql->bindValue(":e_id",$this->equipe,PDO::PARAM_INT);
			$sql->bindValue(":t_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":cpf",$this->cpf);
			$sql->bindValue(":nome",$this->nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `trabalhadores`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `trabalhadores` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `trabalhadores` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}

	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of cpf
	 */ 
	public function getCpf()
	{
		return $this->cpf;
	}

	/**
	 * Set the value of cpf
	 *
	 * @return  self
	 */ 
	public function setCpf($cpf)
	{
		$this->cpf = $cpf;

		return $this;
	}

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of ddd
	 */ 
	public function getDdd()
	{
		return $this->ddd;
	}

	/**
	 * Set the value of ddd
	 *
	 * @return  self
	 */ 
	public function setDdd($ddd)
	{
		$this->ddd = $ddd;

		return $this;
	}

	/**
	 * Get the value of telefone
	 */ 
	public function getTelefone()
	{
		return $this->telefone;
	}

	/**
	 * Set the value of telefone
	 *
	 * @return  self
	 */ 
	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;

		return $this;
	}

	/**
	 * Get the value of equipe
	 */ 
	public function getEquipe()
	{
		return $this->equipe;
	}

	/**
	 * Set the value of equipe
	 *
	 * @return  self
	 */ 
	public function setEquipe($equipe)
	{
		$this->equipe = $equipe;

		return $this;
	}
}
