<?php
class item_chamado {
	private $id;
	private $descricao; 
	private $unidade; 
	private $quantidade; 	 
	private $observacao;
	private $chamado;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `itens_chamado`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `itens_chamado` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `itens_chamado` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function adicionar(){
		if(!$this->con) return false;
		try{
			$sql = $this->con->prepare("INSERT INTO itens_chamado (descricao, unidade, quantidade, observacao, chamado_id) VALUES (:descricao, :unidade, :qtd, :obs, :c_id);");
			$sql->bindValue(":descricao",$this->descricao);
			$sql->bindValue(":unidade",$this->unidade);
			$sql->bindValue(":qtd",$this->quantidade);
			$sql->bindValue(":obs",$this->observacao);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function atualizar(){
		if(!$this->con) return false;
		try{
			$sql = $this->con->prepare("UPDATE itens_chamado SET descricao = :descricao, unidade = :unidade, quantidade = :qtd, observacao = :obs, chamado_id = :c_id WHERE id = :i_id;");
			$sql->bindValue(":descricao",$this->descricao);
			$sql->bindValue(":unidade",$this->unidade);
			$sql->bindValue(":qtd",$this->quantidade);
			$sql->bindValue(":obs",$this->observacao);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->bindValue(":i_id",$this->id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function procurarDescricao($descricao = null){
		if(!$this->con) return false;
		try{
			if(is_null($descricao))
				$descricao = $this->descricao;
			$sql = $this->con->prepare("SELECT descricao FROM itens_chamado WHERE descricao = :descricao;");
			$sql->bindValue(":descricao",$descricao);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function procurarId($item_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($item_id))
				$item_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM itens_chamado WHERE id = :id;");
			$sql->bindValue(":id",$item_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function listar($item_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($item_id))
				$item_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM itens_chamado WHERE id = :id;");
			$sql->bindValue(":id",$item_id,PDO::PARAM_INT);
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function deletar($item_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($item_id))
				$item_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM itens_chamado WHERE id = :id;");
			$sql->bindValue(":id",$item_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function getChamado()
	{
		return $this->chamado;
	}

	/**
	 * Set the value of chamado
	 *
	 * @return  self
	 */ 
	public function setChamado($chamado)
	{
		$this->chamado = $chamado;

		return $this;
	}

	public function getObservacao()
	{
		return $this->observacao;
	}

	/**
	 * Set the value of observacao
	 *
	 * @return  self
	 */ 
	public function setObservacao($observacao)
	{
		$this->observacao = $observacao;

		return $this;
	}

	public function getQuantidade()
	{
		return $this->quantidade;
	}

	/**
	 * Set the value of quantidade
	 *
	 * @return  self
	 */ 
	public function setQuantidade($quantidade)
	{
		$this->quantidade = $quantidade;

		return $this;
	}

	public function getUnidade()
	{
		return $this->unidade;
	}

	/**
	 * Set the value of unidade
	 *
	 * @return  self
	 */ 
	public function setUnidade($unidade)
	{
		$this->unidade = $unidade;

		return $this;
	}

	public function getDescricao()
	{
		return $this->descricao;
	}

	/**
	 * Set the value of descricao
	 *
	 * @return  self
	 */ 
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;

		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
