<?php
class Proprio { 
	private $id;
	private $nome;
	private $endereco;
	private $numero;
	private $bairro;
	private $cep;
	private $horario_abert;
	private $horario_fech;
	private $ddd;
	private $telefone;
	private $latitude;
	private $longitude;
	private $pavimentos;
	private $data_construcao;
	private $area_construida;
	private $validade_avcb;
	private $publico_alvo;
	private $benef_manha;
	private $benef_tarde;
	private $benef_noite;
	private $usuarios_id;
	private $secretaria_id;
	private $cidade_id;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarPalavraChaveId($palavra_chave_id = null){
		try{
			if(is_null($palavra_chave_id))
				return false;
			$sql = $this->con->prepare("SELECT * FROM `palavras_chave` WHERE id = :id;");	
			$sql->bindValue(":id",$palavra_chave_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarPalavraChave($palavra_chave_id = null){
		try{
			if(is_null($palavra_chave_id))
				return false;
			$sql = $this->con->prepare("SELECT * FROM `palavras_chave` WHERE id = :id;");	
			$sql->bindValue(":id",$palavra_chave_id,PDO::PARAM_INT);
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTodasPalavrasChave(){
		try{
			$sql = $this->con->prepare("SELECT * FROM `palavras_chave`;");	
			$sql->execute();
			$res = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $res != null ? $res : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function procurarNome($proprio_nome = null){
		try{
			if(is_null($proprio_nome))
				$proprio_nome = $this->nome;
			$sql = $this->con->prepare("SELECT nome FROM proprios WHERE nome = :nome;");	
			$sql->bindValue(":nome",$proprio_nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarId($proprio_id = null){
		try{
			if(is_null($proprio_id))
				$proprio_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM proprios WHERE id = :p_id");	
			$sql->bindValue(":p_id",$proprio_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `proprios`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `proprios` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `proprios` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function listar($proprio_id) {
		if(!$this->con) return false;
		try{
			$q = "SELECT * FROM proprios WHERE id = :id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":id",$proprio_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function listarPorSecretaria($id_secretaria) {
		if(!$this->con) return false;
		try{
			$q = "SELECT * FROM proprios WHERE secretaria_id = :s_id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":s_id",$id_secretaria,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function pegarSecretaria($proprio_id) {
		if(!$this->con) return false;
		try{
			$q = "SELECT secretarias.* FROM proprios,secretarias WHERE proprios.id = :id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":id",$proprio_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Execption $e){
			return false;
		}
	}

	public function listarPorCategoriaDeSecretaria($categoria) {
		return "";
	}
	public function adicionar() {
		if(!$this->con) return false;
		try{
			$q = "INSERT INTO proprios (nome, endereco, numero, bairro, cep, horario_abert, horario_fech, ddd, telefone, latitude, longitude, pavimentos, data_construcao, area_const, validade_avcb, publico_alvo, benif_manha, benif_tarde, benif_noite, usuario_id, cidade_id, secretaria_id) VALUES (:nome, :endereco, :numero, :bairro, :cep, :horario_abert, :horario_fech, :ddd, :telefone, :latitude, :longitude, :pavimentos, :data_construcao, :area_const, :validade_avcb, :publico_alvo, :benif_manha, :benif_tarde, :benif_noite, :usuario_id, :cidade_id, :secretaria_id);";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":endereco",$this->endereco);
			$sql->bindValue(":numero",$this->numero);
			$sql->bindValue(":bairro",$this->bairro);
			$sql->bindValue(":cep",$this->cep);
			$sql->bindValue(":horario_abert",$this->horario_abert);
			$sql->bindValue(":horario_fech",$this->horario_fech);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":latitude",$this->latitude);
			$sql->bindValue(":longitude",$this->longitude);
			$sql->bindValue(":pavimentos",$this->pavimentos);
			$sql->bindValue(":data_construcao",$this->data_construcao);
			$sql->bindValue(":area_const",$this->area_construida);
			$sql->bindValue(":validade_avcb",$this->validade_avcb);
			$sql->bindValue(":publico_alvo",$this->publico_alvo);
			$sql->bindValue(":benif_manha",$this->benef_manha);
			$sql->bindValue(":benif_tarde",$this->benef_tarde);
			$sql->bindValue(":benif_noite",$this->benef_noite);
			$sql->bindValue(":usuario_id",$this->usuarios_id);
			$sql->bindValue(":cidade_id",$this->cidade_id);
			$sql->bindValue(":secretaria_id",$this->secretaria_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function atualizar() {
		if(!$this->con) return false;
		try{
			$q = "UPDATE proprios SET nome = :nome, endereco = :endereco, numero = :numero, bairro = :bairro, horario_abert = :horario_abert, horario_fech = :horario_fech, ddd = :ddd, telefone = :telefone, latitude = :latitude, longitude = :longitude, pavimentos = :pavimentos, data_construcao = :data_construcao, area_const = :area_const, validade_avcb = :validade_avcb, publico_alvo = :publico_alvo, benif_manha = :benif_manha, benif_tarde = :benif_tarde, benif_noite = :benif_noite, usuario_id = :usuario_id, cidade_id = :cidade_id, secretaria_id = :secretaria_id WHERE id = :p_id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":endereco",$this->endereco);
			$sql->bindValue(":numero",$this->numero);
			$sql->bindValue(":bairro",$this->bairro);
			$sql->bindValue(":horario_abert",$this->horario_abert);
			$sql->bindValue(":horario_fech",$this->horario_fech);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":latitude",$this->latitude);
			$sql->bindValue(":longitude",$this->longitude);
			$sql->bindValue(":pavimentos",$this->pavimentos);
			$sql->bindValue(":data_construcao",$this->data_construcao);
			$sql->bindValue(":area_const",$this->area_construida);
			$sql->bindValue(":validade_avcb",$this->validade_avcb);
			$sql->bindValue(":publico_alvo",$this->publico_alvo);
			$sql->bindValue(":benif_manha",$this->benef_manha);
			$sql->bindValue(":benif_tarde",$this->benef_tarde);
			$sql->bindValue(":benif_noite",$this->benef_noite);
			$sql->bindValue(":usuario_id",$this->usuarios_id);
			$sql->bindValue(":cidade_id",$this->cidade_id);
			$sql->bindValue(":secretaria_id",$this->secretaria_id);
			$sql->bindValue(":p_id",$this->id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function deletar($proprio_id = null){
		try{
			if(is_null($proprio_id))
				$proprio_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM proprios WHERE id = :p_id;");	
			$sql->bindValue(":p_id",$proprio_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionarUsuario($usuario_id) {
		try{
			if(is_null($usuario_id))
				$usuario_id = $this->usuarios_id;
			$sql = $this->con->prepare("UPDATE proprios SET usuario_id = :u_id WHERE proprios.id = :p_id;");	
			$sql->bindValue(":p_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":u_id",$usuario_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionarSecretaria($secretaria_id = null) {
		try{
			if(is_null($secretaria_id))
				$secretaria_id = $this->secretaria_id;
			$sql = $this->con->prepare("UPDATE proprios SET secretaria_id = :s_id WHERE proprios.id = :p_id;");	
			$sql->bindValue(":p_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":s_id",$secretaria_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionarCidade($cidade_id = null) {
		try{
			if(is_null($cidade_id))
				$cidade_id = $this->cidade_id;
			$sql = $this->con->prepare("UPDATE proprios SET cidade_id = :c_id WHERE proprios.id = :p_id;");	
			$sql->bindValue(":p_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":c_id",$cidade_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function ocultar($proprio_id) {
		return false;
	}
	/**
	 * Get the value of cidade_id
	 */ 
	public function getCidade_id()
	{
		return $this->cidade_id;
	}

	/**
	 * Set the value of cidade_id
	 *
	 * @return  self
	 */ 
	public function setCidade_id($cidade_id)
	{
		$this->cidade_id = $cidade_id;

		return $this;
	}

	/**
	 * Get the value of secretaria_id
	 */ 
	public function getSecretaria_id()
	{
		return $this->secretaria_id;
	}

	/**
	 * Set the value of secretaria_id
	 *
	 * @return  self
	 */ 
	public function setSecretaria_id($secretaria_id)
	{
		$this->secretaria_id = $secretaria_id;

		return $this;
	}

	/**
	 * Get the value of usuarios_id
	 */ 
	public function getUsuarios_id()
	{
		return $this->usuarios_id;
	}

	/**
	 * Set the value of usuarios_id
	 *
	 * @return  self
	 */ 
	public function setUsuarios_id($usuarios_id)
	{
		$this->usuarios_id = $usuarios_id;

		return $this;
	}

	/**
	 * Get the value of benef_noite
	 */ 
	public function getBenef_noite()
	{
		return $this->benef_noite;
	}

	/**
	 * Set the value of benef_noite
	 *
	 * @return  self
	 */ 
	public function setBenef_noite($benef_noite)
	{
		$this->benef_noite = $benef_noite;

		return $this;
	}

	/**
	 * Get the value of benef_tarde
	 */ 
	public function getBenef_tarde()
	{
		return $this->benef_tarde;
	}

	/**
	 * Set the value of benef_tarde
	 *
	 * @return  self
	 */ 
	public function setBenef_tarde($benef_tarde)
	{
		$this->benef_tarde = $benef_tarde;

		return $this;
	}

	/**
	 * Get the value of benef_manha
	 */ 
	public function getBenef_manha()
	{
		return $this->benef_manha;
	}

	/**
	 * Set the value of benef_manha
	 *
	 * @return  self
	 */ 
	public function setBenef_manha($benef_manha)
	{
		$this->benef_manha = $benef_manha;

		return $this;
	}

	/**
	 * Get the value of publico_alvo
	 */ 
	public function getPublico_alvo()
	{
		return $this->publico_alvo;
	}

	/**
	 * Set the value of publico_alvo
	 *
	 * @return  self
	 */ 
	public function setPublico_alvo($publico_alvo)
	{
		$this->publico_alvo = $publico_alvo;

		return $this;
	}

	/**
	 * Get the value of validade_avcb
	 */ 
	public function getValidade_avcb()
	{
		return $this->validade_avcb;
	}

	/**
	 * Set the value of validade_avcb
	 *
	 * @return  self
	 */ 
	public function setValidade_avcb($validade_avcb)
	{
		$this->validade_avcb = $validade_avcb;

		return $this;
	}

	/**
	 * Get the value of area_construida
	 */ 
	public function getArea_construida()
	{
		return $this->area_construida;
	}

	/**
	 * Set the value of area_construida
	 *
	 * @return  self
	 */ 
	public function setArea_construida($area_construida)
	{
		$this->area_construida = $area_construida;

		return $this;
	}

	/**
	 * Get the value of data_construcao
	 */ 
	public function getData_construcao()
	{
		return $this->data_construcao;
	}

	/**
	 * Set the value of data_construcao
	 *
	 * @return  self
	 */ 
	public function setData_construcao($data_construcao)
	{
		$this->data_construcao = $data_construcao;

		return $this;
	}

	/**
	 * Get the value of pavimentos
	 */ 
	public function getPavimentos()
	{
		return $this->pavimentos;
	}

	/**
	 * Set the value of pavimentos
	 *
	 * @return  self
	 */ 
	public function setPavimentos($pavimentos)
	{
		$this->pavimentos = $pavimentos;

		return $this;
	}

	/**
	 * Get the value of longitude
	 */ 
	public function getLongitude()
	{
		return $this->longitude;
	}

	/**
	 * Set the value of longitude
	 *
	 * @return  self
	 */ 
	public function setLongitude($longitude)
	{
		$this->longitude = $longitude;

		return $this;
	}

	/**
	 * Get the value of latitude
	 */ 
	public function getLatitude()
	{
		return $this->latitude;
	}

	/**
	 * Set the value of latitude
	 *
	 * @return  self
	 */ 
	public function setLatitude($latitude)
	{
		$this->latitude = $latitude;

		return $this;
	}

	/**
	 * Get the value of telefone
	 */ 
	public function getTelefone()
	{
		return $this->telefone;
	}

	/**
	 * Set the value of telefone
	 *
	 * @return  self
	 */ 
	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;

		return $this;
	}

	/**
	 * Get the value of ddd
	 */ 
	public function getDdd()
	{
		return $this->ddd;
	}

	/**
	 * Set the value of ddd
	 *
	 * @return  self
	 */ 
	public function setDdd($ddd)
	{
		$this->ddd = $ddd;

		return $this;
	}

	/**
	 * Get the value of horario_abert
	 */ 
	public function getHorario_abert()
	{
		return $this->horario_abert;
	}

	/**
	 * Set the value of horario_abert
	 *
	 * @return  self
	 */ 
	public function setHorario_abert($horario_abert)
	{
		$this->horario_abert = $horario_abert;

		return $this;
	}

	/**
	 * Get the value of horario_fech
	 */ 
	public function getHorario_fech()
	{
		return $this->horario_fech;
	}

	/**
	 * Set the value of horario_fech
	 *
	 * @return  self
	 */ 
	public function setHorario_fech($horario_fech)
	{
		$this->horario_fech = $horario_fech;

		return $this;
	}

	/**
	 * Get the value of cep
	 */ 
	public function getCep()
	{
		return $this->cep;
	}

	/**
	 * Set the value of cep
	 *
	 * @return  self
	 */ 
	public function setCep($cep)
	{
		$this->cep = $cep;

		return $this;
	}

	/**
	 * Get the value of bairro
	 */ 
	public function getBairro()
	{
		return $this->bairro;
	}

	/**
	 * Set the value of bairro
	 *
	 * @return  self
	 */ 
	public function setBairro($bairro)
	{
		$this->bairro = $bairro;

		return $this;
	}

	/**
	 * Get the value of numero
	 */ 
	public function getNumero()
	{
		return $this->numero;
	}

	/**
	 * Set the value of numero
	 *
	 * @return  self
	 */ 
	public function setNumero($numero)
	{
		$this->numero = $numero;

		return $this;
	}

	/**
	 * Get the value of endereco
	 */ 
	public function getEndereco()
	{
		return $this->endereco;
	}

	/**
	 * Set the value of endereco
	 *
	 * @return  self
	 */ 
	public function setEndereco($endereco)
	{
		$this->endereco = $endereco;

		return $this;
	}

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
