<?php
class Chamado {
	private $id;
	private $situacao;
	private $data_abertura;
	private $data_finalizacao;
	private $class_manut;
	private $periodicidade;
	private $atividade;
	private $observacoes;
	private $proprio_id;
	private $palavra_chave_id;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function adicionarProprio(){
		try{
			$sql = $this->con->prepare("UPDATE chamados SET proprio_id = :p_id WHERE id = :c_id;");
			$sql->bindValue(":c_id",$this->id);
			$sql->bindValue(":p_id",$this->proprio_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	
	public function procurarId($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM chamados WHERE id = :id;");
			$sql->bindValue(":id",$chamado_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function alertas(){
		try{
			
			$sql = $this->con->prepare("SELECT * FROM `chamados` WHERE class_manut = 3 ORDER BY `chamados`.`data_abertura` DESC  LIMIT 3;");
			$sql->execute();
			return $sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $ex){
			return null;
		}
	}
	public function manutencoesProximas(){
		try{
			
			$sql = $this->con->prepare("SELECT * FROM `chamados` WHERE class_manut = 1 ORDER BY `chamados`.`data_abertura` DESC  LIMIT 3;");
			$sql->execute();
			return $sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $ex){
			return null;
		}
	}
	public function manutencoesProximasCorretiva(){
		try{
			
			$sql = $this->con->prepare("SELECT * FROM `chamados` WHERE class_manut = 2 ORDER BY `chamados`.`data_abertura` DESC  LIMIT 3;");
			$sql->execute();
			return $sql->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarAtividade($atividade = null){
		try{
			if(is_null($atividade))
				$atividade = $this->atividade;
			$sql = $this->con->prepare("SELECT atividade FROM chamados WHERE atividade = :atividade;");
			$sql->bindValue(":atividade",$atividade);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function gerarId($secretaria_sigla){
		try{
			$id = "SM";
			$id .= $secretaria_sigla;
			$sql = $this->con->prepare("SELECT COUNT(id) AS 'num' FROM chamados WHERE id LIKE :sigla;");
			$sql->bindValue(":sigla","%".$secretaria_sigla."%");
			$sql->execute();
			$num = $sql->fetch(PDO::FETCH_ASSOC);
			$num = (int) $num["num"] + 1;
			$id .= $num;
			return $id;
		}catch(Exception $e){
			return false;
		}
	}
	public function abrir($sigla) {
		try{
			$chamado_id = $this->gerarId($sigla);
			$this->id = $chamado_id;
			$sql = $this->con->prepare("INSERT INTO chamados (id, situacao, data_abertura, data_finalizacao, class_manut, periodicidade, atividade, observacoes, proprio_id, palavra_chave_id) VALUES (:c_id, :situacao, CURRENT_TIMESTAMP, null, :class, :periodicidade, :atividade, :obs, :p_id, :pc_id);");
			$sql->bindValue(":c_id",$chamado_id);
			$sql->bindValue(":situacao",$this->situacao);
			$sql->bindValue(":class",$this->class_manut);
			$sql->bindValue(":periodicidade",$this->periodicidade);
			$sql->bindValue(":atividade",$this->atividade);
			$sql->bindValue(":obs",$this->observacoes);
			$sql->bindValue(":p_id",$this->proprio_id,PDO::PARAM_INT);
			$sql->bindValue(":pc_id",$this->palavra_chave_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function atualizar(){
		try{
			$sql = $this->con->prepare("UPDATE chamados SET situacao = :sit, data_abertura = :data_a, data_finalizacao = null, class_manut = :class, periodicidade = :peri, atividade = :atv , observacoes = :obs, proprio_id = :p_id, palavra_chave_id = :pc_id WHERE id = :id;");
			$sql->bindValue(":id",$this->id);
			$sql->bindValue(":sit",$this->situacao);
			$sql->bindValue(":data_a",$this->data_abertura);
			$sql->bindValue(":class",$this->class_manut);
			$sql->bindValue(":peri",$this->periodicidade);
			$sql->bindValue(":atv",$this->atividade);
			$sql->bindValue(":obs",$this->observacoes);
			$sql->bindValue(":p_id",$this->proprio_id,PDO::PARAM_INT);
			$sql->bindValue(":pc_id",$this->palavra_chave_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM chamados WHERE id = :id;");
			$sql->bindValue(":id",$chamado_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionarPalavraChave($palavra_chave_id = null) {
		try{
			if(is_null($palavra_chave_id))
				$palavra_chave_id = $this->palavra_chave_id;
			$sql = $this->con->prepare("UPDATE chamados SET palavra_chave_id = :pc_id WHERE id = :c_id;");
			$sql->bindValue(":c_id",$this->id);
			$sql->bindValue(":pc_id",$palavra_chave_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function listar($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM chamados WHERE id = :id;");
			$sql->bindValue(":id",$chamado_id);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `chamados`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `chamados` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `chamados` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function adicionarEquipePropria($equipe_id = null)  {
		try{
			if(is_null($equipe_id) || is_null($this->id))
				return false;
			$equipe_propria = new Equipe_propria();
			$equipe_propria->setId($equipe_id);
			return $equipe_propria->adicionarChamado($this->id) ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
 
	public function getPalavra_chave_id()
	{
		return $this->palavra_chave_id;
	}
	/**
	 * Set the value of palavra_chave_id
	 *
	 * @return  self
	 */ 
	public function setPalavra_chave_id($palavra_chave_id)
	{
		$this->palavra_chave_id = $palavra_chave_id;

		return $this;
	}
	/**
	 * Get the value of proprio_id
	 */ 
	public function getProprio_id()
	{
		return $this->proprio_id;
	}
	/**
	 * Set the value of proprio_id
	 *
	 * @return  self
	 */ 
	public function setProprio_id($proprio_id)
	{
		$this->proprio_id = $proprio_id;

		return $this;
	}
	/**
	 * Get the value of observacoes
	 */ 
	public function getObservacoes()
	{
		return $this->observacoes;
	}
	/**
	 * Set the value of observacoes
	 *
	 * @return  self
	 */ 
	public function setObservacoes($observacoes)
	{
		$this->observacoes = $observacoes;

		return $this;
	}
	/**
	 * Get the value of atividade
	 */ 
	public function getAtividade()
	{
		return $this->atividade;
	}
	/**
	 * Set the value of atividade
	 *
	 * @return  self
	 */ 
	public function setAtividade($atividade)
	{
		$this->atividade = $atividade;

		return $this;
	}
	/**
	 * Get the value of periodicidade
	 */ 
	public function getPeriodicidade()
	{
		return $this->periodicidade;
	}
	/**
	 * Set the value of periodicidade
	 *
	 * @return  self
	 */ 
	public function setPeriodicidade($periodicidade)
	{
		$this->periodicidade = $periodicidade;

		return $this;
	}
	/**
	 * Get the value of class_manut
	 */ 
	public function getClass_manut()
	{
		return $this->class_manut;
	}
	/**
	 * Set the value of class_manut
	 *
	 * @return  self
	 */ 
	public function setClass_manut($class_manut)
	{
		$this->class_manut = $class_manut;

		return $this;
	}
	/**
	 * Get the value of data_finalizacao
	 */ 
	public function getData_finalizacao()
	{
		return $this->data_finalizacao;
	}
	/**
	 * Set the value of data_finalizacao
	 *
	 * @return  self
	 */ 
	public function setData_finalizacao($data_finalizacao)
	{
		$this->data_finalizacao = $data_finalizacao;

		return $this;
	}
	/**
	 * Get the value of data_abertura
	 */ 
	public function getData_abertura()
	{
		return $this->data_abertura;
	}
	/**
	 * Set the value of data_abertura
	 *
	 * @return  self
	 */ 
	public function setData_abertura($data_abertura)
	{
		$this->data_abertura = $data_abertura;

		return $this;
	}
	/**
	 * Get the value of situacao
	 */ 
	public function getSituacao()
	{
		return $this->situacao;
	}
	/**
	 * Set the value of situacao
	 *
	 * @return  self
	 */ 
	public function setSituacao($situacao)
	{
		$this->situacao = $situacao;

		return $this;
	}
	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}
	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
