<?php
class Cidade { 
	private $id;
	private $nome;
	private $estado_id;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarId($cidade_id){
		try{
			$sql = $this->con->prepare("SELECT id FROM cidades WHERE id = :id;");	
			$sql->bindValue(":id",$cidade_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return true;
		}
	}
	public function procurarNome($cidade_nome = null){
		try{
			if(is_null($cidade_nome))
				$cidade_nome = $this->nome;
			$sql = $this->con->prepare("SELECT nome FROM cidades WHERE nome = :nome;");	
			$sql->bindValue(":nome",$cidade_nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return true;
		}
	}
	public function listar($cidade_id = null){
		try{
			if(is_null($cidade_id))
				$cidade_nome = $this->id;
			$sql = $this->con->prepare("SELECT * FROM cidades WHERE id = :id;");	
			$sql->bindValue(":id",$cidade_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->fetch(PDO::FETCH_ASSOC);
		}catch(Exception $ex){
			return null;
		}
	}
	public function adicionar($estado_id) {
		try{
			$sql = $this->con->prepare("INSERT INTO cidades (nome, estado_id) VALUES (:nome, :estado_id);");	
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":estado_id",$estado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($cidade_id = null) {
		try{
			if(is_null($cidade_id))
				$cidade_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM cidades WHERE id = :c_id;");	
			$sql->bindValue(":c_id",$cidade_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `cidades`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `cidades` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `cidades` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}

	public function getEstado_id()
	{
		return $this->estado_id;
	}

	/**
	 * Set the value of estado_id
	 *
	 * @return  self
	 */ 
	public function setEstado_id($estado_id)
	{
		$this->estado_id = $estado_id;

		return $this;
	}

	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
