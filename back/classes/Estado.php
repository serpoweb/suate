<?php
class Estado {
	private $id;
	private $nome;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	} 
	public function procurarId($estado_id = null){
		try{
			if(is_null($estado_id))
				$estado_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM estados WHERE id = :id");
			$sql->bindValue(":id",$estado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function procurarNome($estado_nome = null){
		try{
			if(is_null($estado_nome))
				$estado_nome = $this->nome;
			$sql = $this->con->prepare("SELECT nome FROM estados WHERE nome = :nome;");
			$sql->bindValue(":nome",$estado_nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function adicionar($estado_nome = null) {
		try{
			if(is_null($estado_nome))
				$estado_nome = $this->nome;
			$sql = $this->con->prepare("INSERT INTO estados (nome) VALUES (:nome);");
			$sql->bindValue(":nome",$estado_nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($estado_id = null) {
		try{
			if(is_null($estado_id))
				$estado_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM estados WHERE id = :id;");
			$sql->bindValue(":id",$estado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `estados`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `estados` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `estados` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function listar($estado_id = null){
		try{
			if(is_null($estado_id))
				$estado_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM estados WHERE id = :id;");
			$sql->bindValue(":id",$estado_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
