<?php
class Blacklist {
	private $id; 
	private $cpf;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function adicionar($cpf) {
		try{
			$sq = $this->con->prepare("SELECT * FROM blacklist WHERE cpf = :cpff;");
			$sq->bindValue(":cpff",$cpf);
			$sq->execute();
			if($sq->rowCount() == 0){
				$sql = $this->con->prepare("INSERT INTO blacklist (cpf) VALUES (:cpff);");
				$sql->bindValue(":cpff",$cpf);
				$sql->execute();
				return $sql->rowCount() != 0 ? true : false;
			}else{
				return true;
			}
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `blacklist`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `blacklist` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `blacklist` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function remover($cpf) {
		try{
			$sql = $this->con->prepare("DELETE FROM blacklist WHERE cpf = :cpff;");
			$sql->bindValue(":cpff",$cpf);
			return $sql->execute() ? true : false;
		}catch(Exception $ex){
			return false;
		}
	} 
	public function procurar($cpf) {
		try{
			$sql = $this->con->prepare("DELETE FROM blacklist WHERE cpf = :cpff;");
			$sql->bindValue(":cpff",$cpf);
			return $sql->execute() ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of cpf
	 */ 
	public function getCpf()
	{
		return $this->cpf;
	}

	/**
	 * Set the value of cpf
	 *
	 * @return  self
	 */ 
	public function setCpf($cpf)
	{
		$this->cpf = $cpf;

		return $this;
	}
}
