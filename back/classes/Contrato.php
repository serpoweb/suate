<?php
class Contrato { 
	private $id;
	private $data_de_contratacao;
	private $valor_principal;
	private $valor1;
	private $valor2;
	private $valor3;
	private $valor4;
	private $valor5;
	private $observacoes;
	private $chamado;
	private $tercerizada;
	private $con;

	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarChamado($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->chamado;
			$sql = $this->con->prepare("SELECT chamado_id FROM contratos WHERE chamado_id = :id;");
			$sql->bindValue(":id",$chamado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function procurarId($contrato_id = null){
		try{
			if(is_null($contrato_id))
				$contrato_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM contratos WHERE id = :id;");
			$sql->bindValue(":id",$contrato_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `contratos`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `contratos` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `contratos` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function adicionar() {
		if(!$this->con) return false;
		try{
			$q = "INSERT INTO contratos (data_contratacao, valor_principal, valor1, valor2, valor3, valor4, valor5, observacoes, chamado_id, terceirizada_id) VALUES (:data_c, :v, :v1, :v2, :v3, :v4, :v5, :obs, :c_id, :t_id);";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":data_c",$this->data_de_contratacao);
			$sql->bindValue(":v",$this->valor_principal);
			$sql->bindValue(":v1",$this->valor1);
			$sql->bindValue(":v2",$this->valor2);
			$sql->bindValue(":v3",$this->valor3);
			$sql->bindValue(":v4",$this->valor4);
			$sql->bindValue(":v5",$this->valor5);
			$sql->bindValue(":obs",$this->observacoes);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->bindValue(":t_id",$this->tercerizada,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function atualizar() {
		if(!$this->con) return false;
		try{
			$q = "UPDATE contratos SET valor_principal = :v, valor1 = :v1, valor2 = :v2, valor3 = :v3, valor4 = :v4, valor5 = :v5, observacoes = :obs, chamado_id = :c_id, terceirizada_id = :t_id WHERE id = :id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":v",$this->valor_principal);
			$sql->bindValue(":v1",$this->valor1);
			$sql->bindValue(":v2",$this->valor2);
			$sql->bindValue(":v3",$this->valor3);
			$sql->bindValue(":v4",$this->valor4);
			$sql->bindValue(":v5",$this->valor5);
			$sql->bindValue(":obs",$this->observacoes);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->bindValue(":t_id",$this->tercerizada,PDO::PARAM_INT);
			$sql->bindValue(":id",$this->id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function listar($contrato_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($contrato_id))
				$contrato_id = $this->id;
				$sql = $this->con->prepare("SELECT * FROM contratos WHERE id = :id;");
				$sql->bindValue(":id",$contrato_id,PDO::PARAM_INT);
				$sql->execute();
				$response = $sql->fetch(PDO::FETCH_ASSOC);
				return $response != null ? $response : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function deletar($contrato_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($contrato_id))
				$contrato_id = $this->id;
				$sql = $this->con->prepare("DELETE FROM contratos WHERE id = :id;");
				$sql->bindValue(":id",$contrato_id,PDO::PARAM_INT);
				$sql->execute();
				return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of data_de_cotratacao
	 */ 
	public function getData_de_contratacao()
	{
		return $this->data_de_contratacao;
	}

	/**
	 * Set the value of data_de_cotratacao
	 *
	 * @return  self
	 */ 
	public function setData_de_contratacao($data_de_contratacao)
	{
		$this->data_de_contratacao = $data_de_contratacao;

		return $this;
	}

	/**
	 * Get the value of valor_principal
	 */ 
	public function getValor_principal()
	{
		return $this->valor_principal;
	}

	/**
	 * Set the value of valor_principal
	 *
	 * @return  self
	 */ 
	public function setValor_principal($valor_principal)
	{
		$this->valor_principal = $valor_principal;

		return $this;
	}

	/**
	 * Get the value of valor1
	 */ 
	public function getValor1()
	{
		return $this->valor1;
	}

	/**
	 * Set the value of valor1
	 *
	 * @return  self
	 */ 
	public function setValor1($valor1)
	{
		$this->valor1 = $valor1;

		return $this;
	}

	/**
	 * Get the value of valor2
	 */ 
	public function getValor2()
	{
		return $this->valor2;
	}

	/**
	 * Set the value of valor2
	 *
	 * @return  self
	 */ 
	public function setValor2($valor2)
	{
		$this->valor2 = $valor2;

		return $this;
	}

	/**
	 * Get the value of valor3
	 */ 
	public function getValor3()
	{
		return $this->valor3;
	}

	/**
	 * Set the value of valor3
	 *
	 * @return  self
	 */ 
	public function setValor3($valor3)
	{
		$this->valor3 = $valor3;

		return $this;
	}

	/**
	 * Get the value of valor4
	 */ 
	public function getValor4()
	{
		return $this->valor4;
	}

	/**
	 * Set the value of valor4
	 *
	 * @return  self
	 */ 
	public function setValor4($valor4)
	{
		$this->valor4 = $valor4;

		return $this;
	}

	/**
	 * Get the value of valor5
	 */ 
	public function getValor5()
	{
		return $this->valor5;
	}

	/**
	 * Set the value of valor5
	 *
	 * @return  self
	 */ 
	public function setValor5($valor5)
	{
		$this->valor5 = $valor5;

		return $this;
	}

	/**
	 * Get the value of observacoes
	 */ 
	public function getObservacoes()
	{
		return $this->observacoes;
	}

	/**
	 * Set the value of observacoes
	 *
	 * @return  self
	 */ 
	public function setObservacoes($observacoes)
	{
		$this->observacoes = $observacoes;

		return $this;
	}

	/**
	 * Get the value of chamado
	 */ 
	public function getChamado()
	{
		return $this->chamado;
	}

	/**
	 * Set the value of chamado
	 *
	 * @return  self
	 */ 
	public function setChamado($chamado)
	{
		$this->chamado = $chamado;

		return $this;
	}

	/**
	 * Get the value of tercerizada
	 */ 
	public function getTercerizada()
	{
		return $this->tercerizada;
	}

	/**
	 * Set the value of tercerizada
	 *
	 * @return  self
	 */ 
	public function setTercerizada($tercerizada)
	{
		$this->tercerizada = $tercerizada;

		return $this;
	}
}
