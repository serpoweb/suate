<?php
class Tercerizado {
	private $id;
	private $nome;
	private $cnpj;
	private $ddd;
	private $telefone;
	private $email;
	private $usuario;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `terceirizadas`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `terceirizadas` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `terceirizadas` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function procurarNome($nome = null){
		try{
			if(is_null($nome))
				$nome = $this->nome;
			$q = "SELECT * FROM terceirizadas WHERE nome = :nome;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":nome",$nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return true;
		}
	}
	public function procurarId($tercerizado_id = null){
		try{
			if(is_null($tercerizado_id))
				$tercerizado_id = $this->id;
			$q = "SELECT * FROM terceirizadas WHERE id = :id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":id",$tercerizado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return true;
		}
	}
	public function listar($tercerizado_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($tercerizado_id))
				$tercerizado_id = $this->id;
			
			$q = "SELECT * FROM terceirizadas WHERE id = :id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":id",$tercerizado_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function adicionar(){
		if(!$this->con) return false;
		try{
			$q = "INSERT INTO terceirizadas (nome, cnpj, ddd, telefone, email, usuario_id) VALUES (:nome, :cnpj, :ddd, :telefone, :email, :u_id);";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":cnpj",$this->cnpj);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":email",$this->email);
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function atualizar(){
		if(!$this->con) return false;
		try{
			$q = "UPDATE terceirizadas SET nome = :nome, cnpj = :cnpj, ddd = :ddd, telefone = :telefone, email = :email, usuario_id = :u_id WHERE id = :t_id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":cnpj",$this->cnpj);
			$sql->bindValue(":ddd",$this->ddd);
			$sql->bindValue(":telefone",$this->telefone);
			$sql->bindValue(":email",$this->email);
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->bindValue(":t_id",$this->id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	public function deletar($tercerizado_id = null){
		if(!$this->con) return false;
		try{
			if(is_null($tercerizado_id))
				$tercerizado_id = $this->id;
			$q = "DELETE FROM terceirizadas WHERE id = :t_id;";
			$sql = $this->con->prepare($q);
			$sql->bindValue(":t_id",$tercerizado_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Execption $e){
			return false;
		}
	}
	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of cnpj
	 */ 
	public function getCnpj()
	{
		return $this->cnpj;
	}

	/**
	 * Set the value of cnpj
	 *
	 * @return  self
	 */ 
	public function setCnpj($cnpj)
	{
		$this->cnpj = $cnpj;

		return $this;
	}

	/**
	 * Get the value of ddd
	 */ 
	public function getDdd()
	{
		return $this->ddd;
	}

	/**
	 * Set the value of ddd
	 *
	 * @return  self
	 */ 
	public function setDdd($ddd)
	{
		$this->ddd = $ddd;

		return $this;
	}

	/**
	 * Get the value of telefone
	 */ 
	public function getTelefone()
	{
		return $this->telefone;
	}

	/**
	 * Set the value of telefone
	 *
	 * @return  self
	 */ 
	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;

		return $this;
	}

	/**
	 * Get the value of email
	 */ 
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @return  self
	 */ 
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get the value of usuario
	 */ 
	public function getUsuario()
	{
		return $this->usuario;
	}

	/**
	 * Set the value of usuario
	 *
	 * @return  self
	 */ 
	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;

		return $this;
	}
}
