<?php
class Usuarios {
	private $id;
	private $nome;
	private $cpf;
	private $email;
	private $ddd;
	private $telefone;
	private $tipo;
	private $senha;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
			$this->id = (isset($_SESSION["usuario_id"]) && $_SESSION["usuario_id"] != "" ? $_SESSION["usuario_id"] : null);
		}catch(Exception $ex){
			return null;
		}
	}
	public function validarCampos(){
		return ($this->nome != null && $this->cpf != null && $this->email != null && $this->ddd != null && $this->telefone != null && $this->tipo != null && $this->senha != null ? true : false);
	}
	public function validarCamposSemSenha(){
		return ($this->nome != null && $this->cpf != null && $this->email != null && $this->ddd != null && $this->telefone != null && $this->tipo != null ? true : false);
	}
	public function procurarEmail(){
		try{
			$sql = $this->con->prepare("SELECT * FROM usuarios WHERE email = :email");
			$sql->bindValue(":email",$this->email);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarId($id){
		try{
			$sql = $this->con->prepare("SELECT * FROM usuarios WHERE id = :id");
			$sql->bindValue(":id",$id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function atualizarAdministrador(){
		try{
			if($this->validarCamposSemSenha()){
				$sql = $this->con->prepare("UPDATE usuarios SET nome = :nome, cpf = :cpf, email = :email, ddd = :ddd, telefone = :telefone, tipo = :tipo WHERE id = :id;");
				$sql->bindValue(":nome",$this->nome);
				$sql->bindValue(":email",$this->email);
				$sql->bindValue(":cpf",$this->cpf);
				$sql->bindValue(":ddd",$this->ddd);
				$sql->bindValue(":telefone",$this->telefone);
				$sql->bindValue(":tipo",$this->tipo);
				$sql->bindValue(":id",$this->id,PDO::PARAM_INT);
				$sql->execute();
				return $sql->rowCount() != 0 ? true : false;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	public function adicionarAdministrador() {
		try{
			if($this->validarCampos()){
				$sql = $this->con->prepare("INSERT INTO usuarios (nome, cpf, email, ddd, telefone, tipo, senha) VALUES (:nome, :cpf, :email, :ddd, :telefone, :tipo, :senha);");
				$sql->bindValue(":nome",$this->nome);
				$sql->bindValue(":email",$this->email);
				$sql->bindValue(":cpf",$this->cpf);
				$sql->bindValue(":ddd",$this->ddd);
				$sql->bindValue(":telefone",$this->telefone);
				$sql->bindValue(":tipo",$this->tipo);
				$sql->bindValue(":senha",$this->senha);
				$sql->execute();
				return $sql->rowCount() != 0 ? true : false;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	public function bloquearUsuario($usuario_id) {
		try{
			$sql = $this->con->prepare("SELECT cpf FROM `usuarios` WHERE `id` = :id;");
			$sql->bindValue(":id",$usuario_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			if($response["cpf"] != NULL || $response["cpf"] != "NULL"){
				$cpf = $response["cpf"];
				$blacklist = new Blacklist();
				return $blacklist->adicionar($cpf) ? true : false;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	public function sair(){
		session_destroy();
		return true;
	}
	public function entrar($email, $senha) {
		try{
			$sql = $this->con->prepare("SELECT * FROM usuarios WHERE email = :email AND senha = :senha;");
			$sql->bindValue(":email",$email);
			$sql->bindValue(":senha",$senha);
			$sql->execute();
			if($sql->rowCount() != 0){
				$response = $sql->fetch(PDO::FETCH_ASSOC);
				$_SESSION["usuario_id"] = $response["id"];
				$_SESSION["usuario_nivel_de_acesso"] = $response["tipo"];
				$_SESSION["logado"] = true;
				$_SESSION["usuario_email"] = $response["email"];
				$_SESSION["usuario_nome"] = $response["nome"];
				$_SESSION["usuario_senha"] = $response["senha"];
				return true;
			}else{
				return false;
			}
		}catch(Exception $ex){
			return false;
		}
	}
	public function verificarSenha(){
		$usuario_id = $this->id;
		$antigaSenha = $this->senha;
		$sq = $this->con->prepare("SELECT senha FROM usuarios WHERE senha = :senha AND id = :id;");
		$sq->bindValue(":senha",$antigaSenha);
		$sq->bindValue(":id",$usuario_id);
		$sq->execute();
		return $sq->rowCount() != 0 ? true : false;
	}
	public function solicitarTrocaDeSenha($novaSenha) {
		try{
			if($this->verificarSenha()){
				$sql = $this->con->prepare("UPDATE usuarios SET senha = :novasenha WHERE senha = :antigasenha AND id = :id;");
				$sql->bindValue(":novasenha",$novaSenha);
				$sql->bindValue(":antigasenha",$this->senha);
				$sql->bindValue(":id",$this->id,PDO::PARAM_INT);
				return $sql->execute() ? true : false;
			}else{
				return false;
			}
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($user_id) {
		try{
			$sql = $this->con->prepare("DELETE FROM usuarios WHERE id = :id;");
			$sql->bindValue(":id",$user_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `usuarios`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `usuarios` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `usuarios` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function listar($usuario_id) {
		try{
			$sql = $this->con->prepare("SELECT * FROM usuarios WHERE id = :id;");
			$sql->bindValue(":id",$usuario_id,PDO::PARAM_INT);
			$sql->execute();
			if($sql->rowCount() == 1)
				$response = $sql->fetch(PDO::FETCH_ASSOC);
			else 
				$response = false;
			return $response;
		}catch(Exception $ex){
			return false;
		}
	}
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}
	public function getCpf()
	{
		return $this->cpf;
	}

	/**
	 * Set the value of cpf
	 *
	 * @return  self
	 */ 
	public function setCpf($cpf)
	{
		$this->cpf = $cpf;

		return $this;
	}
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @return  self
	 */ 
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}
	public function getDdd()
	{
		return $this->ddd;
	}

	/**
	 * Set the value of ddd
	 *
	 * @return  self
	 */ 
	public function setDdd($ddd)
	{
		$this->ddd = $ddd;

		return $this;
	}
	public function getTelefone()
	{
		return $this->telefone;
	}

	/**
	 * Set the value of telefone
	 *
	 * @return  self
	 */ 
	public function setTelefone($telefone)
	{
		$this->telefone = $telefone;

		return $this;
	}
	public function getTipo()
	{
		return $this->tipo;
	}

	/**
	 * Set the value of tipo
	 *
	 * @return  self
	 */ 
	public function setTipo($tipo)
	{
		$this->tipo = $tipo;

		return $this;
	}
	public function getSenha()
	{
		return $this->senha;
	}

	/**
	 * Set the value of senha
	 *
	 * @return  self
	 */ 
	public function setSenha($senha)
	{
		$this->senha = $senha;

		return $this;
	}
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
