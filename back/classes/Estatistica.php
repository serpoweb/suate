<?php

class Estatistica {
	public $manutencoes_feitas;
	public $manutencoes_nao_feitas;
	public $manutencoesTotais;
	public $ocorrencias_totais;
	public $ocorrencias_falsas;
	public $ocorrencias_atendidas;
	public $ocorrencias_nao_atendidas;
	public $demanda_atendida;
	public $manutencoes_proximas;
	public $acompanhamento;
  public $m_c;
  public $m_i;
  public $m_p;
  public $usuarios;
  public $proprios;
  public $secretarias;
  public $contratos;
	private $com;
	public function __CONSTRUCT(){
		try{
			$this->com = DataBase::getConnection();	
			$sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE data_finalizacao IS NOT NULL;");
			$sql->execute();
			$this->manutencoes_feitas = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE data_finalizacao IS NULL;");
			$sql->execute();
			$this->manutencoes_nao_feitas = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM chamados;");
			$sql->execute();
			$this->manutencoesTotais = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM ocorrencias;");
			$sql->execute();
			$this->ocorrencias_totais = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(ocorrencias.id) FROM ocorrencias, chamados WHERE ocorrencias.chamado_id = chamados.id AND chamados.situacao = 4;");
			$sql->execute();
			$this->ocorrencias_falsas = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM ocorrencias as o, chamados as c WHERE o.chamado_id = c.id AND c.data_finalizacao IS NOT NULL;");
			$sql->execute();
			$this->ocorrencias_atendidas = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM ocorrencias as o, chamados as c WHERE o.chamado_id = c.id AND data_finalizacao IS NULL;");
			$sql->execute();
			$this->ocorrencias_nao_atendidas = $sql->fetch(PDO::FETCH_BOTH)[0];
			$sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE data_finalizacao IS NULL");
			$sql->execute();
			$this->manutencoes_proximas = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE class_manut = 1;");
			$sql->execute();
			$this->m_p = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE class_manut = 2;");
			$sql->execute();
			$this->m_c = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM chamados WHERE class_manut = 3;");
			$sql->execute();
			$this->m_i = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM usuarios;");
			$sql->execute();
			$this->usuarios = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM proprios;");
			$sql->execute();
			$this->proprios = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM secretarias;");
			$sql->execute();
			$this->secretarias = $sql->fetch(PDO::FETCH_BOTH)[0];
      $sql = $this->com->prepare("SELECT COUNT(*) FROM contratos;");
			$sql->execute();
			$this->contratos = $sql->fetch(PDO::FETCH_BOTH)[0];
		}catch(Exception $ex){
			return null;
		}
	}

	public function listarSi(){
		return $this;
	}

	/**
	 * Get the value of ocorrencias_nao_atendidas
	 */ 
	public function getOcorrencias_nao_atendidas()
	{
		return $this->ocorrencias_nao_atendidas;
	}

	/**
	 * Set the value of ocorrencias_nao_atendidas
	 *
	 * @return  self
	 */ 
	public function setOcorrencias_nao_atendidas($ocorrencias_nao_atendidas)
	{
		$this->ocorrencias_nao_atendidas = $ocorrencias_nao_atendidas;

		return $this;
	}

	/**
	 * Get the value of ocorrencias_atendidas
	 */ 
	public function getOcorrencias_atendidas()
	{
		return $this->ocorrencias_atendidas;
	}

	/**
	 * Set the value of ocorrencias_atendidas
	 *
	 * @return  self
	 */ 
	public function setOcorrencias_atendidas($ocorrencias_atendidas)
	{
		$this->ocorrencias_atendidas = $ocorrencias_atendidas;

		return $this;
	}

	/**
	 * Get the value of ocorrencias_falsas
	 */ 
	public function getOcorrencias_falsas()
	{
		return $this->ocorrencias_falsas;
	}

	/**
	 * Set the value of ocorrencias_falsas
	 *
	 * @return  self
	 */ 
	public function setOcorrencias_falsas($ocorrencias_falsas)
	{
		$this->ocorrencias_falsas = $ocorrencias_falsas;

		return $this;
	}

	/**
	 * Get the value of ocorrencias_totais
	 */ 
	public function getOcorrencias_totais()
	{
		return $this->ocorrencias_totais;
	}

	/**
	 * Set the value of ocorrencias_totais
	 *
	 * @return  self
	 */ 
	public function setOcorrencias_totais($ocorrencias_totais)
	{
		$this->ocorrencias_totais = $ocorrencias_totais;

		return $this;
	}

	/**
	 * Get the value of manutencoesTotais
	 */ 
	public function getManutencoesTotais()
	{
		return $this->manutencoesTotais;
	}

	/**
	 * Set the value of manutencoesTotais
	 *
	 * @return  self
	 */ 
	public function setManutencoesTotais($manutencoesTotais)
	{
		$this->manutencoesTotais = $manutencoesTotais;

		return $this;
	}

	/**
	 * Get the value of manutencoes_nao_feitas
	 */ 
	public function getManutencoes_nao_feitas()
	{
		return $this->manutencoes_nao_feitas;
	}

	/**
	 * Set the value of manutencoes_nao_feitas
	 *
	 * @return  self
	 */ 
	public function setManutencoes_nao_feitas($manutencoes_nao_feitas)
	{
		$this->manutencoes_nao_feitas = $manutencoes_nao_feitas;

		return $this;
	}

	/**
	 * Get the value of manutencoes_feitas
	 */ 
	public function getManutencoes_feitas()
	{
		return $this->manutencoes_feitas;
	}

	/**
	 * Set the value of manutencoes_feitas
	 *
	 * @return  self
	 */ 
	public function setManutencoes_feitas($manutencoes_feitas)
	{
		$this->manutencoes_feitas = $manutencoes_feitas;

		return $this;
	}

	/**
	 * Get the value of demanda_atendida
	 */ 
	public function getDemanda_atendida()
	{
		return $this->demanda_atendida;
	}

	/**
	 * Set the value of demanda_atendida
	 *
	 * @return  self
	 */ 
	public function setDemanda_atendida($demanda_atendida)
	{
		$this->demanda_atendida = $demanda_atendida;

		return $this;
	}

	/**
	 * Get the value of acompanhamento
	 */ 
	public function getAcompanhamento()
	{
		return $this->acompanhamento;
	}

	/**
	 * Set the value of acompanhamento
	 *
	 * @return  self
	 */ 
	public function setAcompanhamento($acompanhamento)
	{
		$this->acompanhamento = $acompanhamento;

		return $this;
	}
}
