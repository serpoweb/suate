<?php

 
 
class Secretaria {
	private $id;
	private $nome;
	private $abreviatura;
	private $usuario; 
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function listarTudo($limite = null, $inicio = null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT * FROM `secretarias`;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `secretarias` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `secretarias` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function validaCampos(){
		return ($this->nome != null && $this->abreviatura != null  && $this->usuario != null ? true : false);
	}
	public function procuraNome(){
		try{
			$sql = $this->con->prepare("SELECT * FROM secretarias WHERE nome = :nome;");
			$sql->bindValue(":nome",$this->nome);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return null;
		}
	}
	public function procurarId($secretaria_id){
		try{
			$sql = $this->con->prepare("SELECT * FROM secretarias WHERE id = :id;");
			$sql->bindValue(":id",$secretaria_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function add(){
		try{
			if(!$this->validaCampos()){
				return false;
			}
			$sql = $this->con->prepare("INSERT INTO secretarias (nome, abreviatura, usuario_id) VALUES (:nome, :abr, :u_id);");
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":abr",$this->abreviatura);
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function listar($secretaria_id){
		try{
			$sql = $this->con->prepare("SELECT * FROM secretarias WHERE id = :id;");
			$sql->bindValue(":id",$secretaria_id,PDO::PARAM_INT);
			$sql->execute();
			$response = $sql->fetch(PDO::FETCH_ASSOC);
			return $response != null ? $response : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function atualizar(){
		try{
			$sql = $this->con->prepare("UPDATE secretarias SET nome = :nome, abreviatura = :abr, usuario_id = :u_id WHERE id = :id;");
			$sql->bindValue(":id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":nome",$this->nome);
			$sql->bindValue(":abr",$this->abreviatura);
			$sql->bindValue(":u_id",$this->usuario,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	public function deletar($secretaria_id){
		try{
			$sql = $this->con->prepare("DELETE FROM secretarias WHERE id = :id;");
			$sql->bindValue(":id",$secretaria_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $ex){
			return false;
		}
	}
	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of nome
	 */ 
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Set the value of nome
	 *
	 * @return  self
	 */ 
	public function setNome($nome)
	{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * Get the value of abreviatura
	 */ 
	public function getAbreviatura()
	{
		return $this->abreviatura;
	}

	/**
	 * Set the value of abreviatura
	 *
	 * @return  self
	 */ 
	public function setAbreviatura($abreviatura)
	{
		$this->abreviatura = $abreviatura;

		return $this;
	}

	/**
	 * Get the value of usuario
	 */ 
	public function getUsuario()
	{
		return $this->usuario;
	}

	/**
	 * Set the value of usuario
	 *
	 * @return  self
	 */ 
	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;

		return $this;
	}
}
