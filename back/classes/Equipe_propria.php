<?php

class Equipe_propria {
	private $id; 
	private $data;
	private $observacoes;
	private $chamado;
	private $equipe;
	private $con;
	public function __CONSTRUCT(){
		try{
			$this->con = DataBase::getConnection();	
		}catch(Exception $ex){
			return null;
		}
	}
	public function listarTudo($limite = null, $inicio = null, $fim= null) {
		if(!$this->con) return false;
		try{
			if($limite == null && $inicio == null ){
				$q = "SELECT equipes_propria.* FROM equipes_propria,chamados WHERE equipes_propria.chamado_id = chamados.id AND chamados.data_finalizacao IS NULL;";
				$sql = $this->con->prepare($q);
			}else if($limite != null && $inicio == null){
				$q = "SELECT * FROM `equipes_propria` LIMIT :limite;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
			}else if($limite != null && $inicio != null){
				$q = "SELECT * FROM `equipes_propria` LIMIT :limite, :inicio;";
				$sql = $this->con->prepare($q);
				$sql->bindValue(":limite",$limite, PDO::PARAM_INT);
				$sql->bindValue(":inicio",$inicio, PDO::PARAM_INT);
			}
			$sql->execute();
			$response = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $response;
		}catch(Execption $e){
			return null;
		}
	}
	public function adicionarChamado($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->chamado;
			$sql = $this->con->prepare("INSERT INTO equipes_propria (data, observacoes, chamado_id, equipe_id) VALUES (CURRENT_DATE, null, :c_id, :equipe_id);");
			$sql->bindValue(":c_id",$chamado_id);
			$sql->bindValue(":equipe_id",$this->id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function adicionar(){
		try{
			$flag = false;
			$q = "INSERT INTO equipes_propria (`data`, observacoes, chamado_id, equipe_id) VALUES (:dat, :obs, :c_id, :e_id);";
			if(is_null($this->data)){
				$q = str_replace(":dat","CURRENT_DATE",$q);
			}else 
				$flag = true;
			$sql = $this->con->prepare($q);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->bindValue(":e_id",$this->equipe,PDO::PARAM_INT);
			$sql->bindValue(":obs",$this->observacoes);
			if($flag) $sql->bindValue(":dat",$this->data);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function atualizar(){
		try{
			$sql = $this->con->prepare("UPDATE equipes_propria SET data = :dat, observacoes = :obs, chamado_id = :c_id, equipe_id = :e_id WHERE id = :ep_id;");
			$sql->bindValue(":dat",$this->data);
			$sql->bindValue(":c_id",$this->chamado);
			$sql->bindValue(":e_id",$this->equipe,PDO::PARAM_INT);
			$sql->bindValue(":ep_id",$this->id,PDO::PARAM_INT);
			$sql->bindValue(":obs",$this->observacoes);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function procurarChamado($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->chamado;
			$sql = $this->con->prepare("SELECT chamado_id FROM equipes_propria WHERE chamado_id = :c_id;");
			$sql->bindValue(":c_id",$chamado_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function procurarId($equipe_propria_id = null){
		try{
			if(is_null($equipe_propria_id))
				$equipe_propria_id = $this->id;
			$sql = $this->con->prepare("SELECT id FROM equipes_propria WHERE id = :id;");
			$sql->bindValue(":id",$equipe_propria_id);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	public function listar($equipe_propria_id = null){
		try{
			if(is_null($equipe_propria_id))
				$equipe_propria_id = $this->id;
			$sql = $this->con->prepare("SELECT * FROM equipes_propria WHERE id = :id;");
			$sql->bindValue(":id",$equipe_propria_id);
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			if($res != null){
				return $res;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	public function listarPorChamado($chamado_id = null){
		try{
			if(is_null($chamado_id))
				$chamado_id = $this->chamado;
			$sql = $this->con->prepare("SELECT * FROM equipes_propria WHERE chamado_id = :id;");
			$sql->bindValue(":id",$chamado_id);
			$sql->execute();
			$res = $sql->fetch(PDO::FETCH_ASSOC);
			if($res != null){
				return $res;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	public function deletar($equipe_propria_id = null){
		try{
			if(is_null($equipe_propria_id))
				$equipe_propria_id = $this->id;
			$sql = $this->con->prepare("DELETE FROM equipes_propria WHERE id = :id;");
			$sql->bindValue(":id",$equipe_propria_id,PDO::PARAM_INT);
			$sql->execute();
			return $sql->rowCount() != 0 ? true : false;
		}catch(Exception $e){
			return false;
		}
	}
	/**
	 * Get the value of equipe
	 */ 
	public function getEquipe()
	{
		return $this->equipe;
	}

	/**
	 * Set the value of equipe
	 *
	 * @return  self
	 */ 
	public function setEquipe($equipe)
	{
		$this->equipe = $equipe;

		return $this;
	}

	/**
	 * Get the value of chamado
	 */ 
	public function getChamado()
	{
		return $this->chamado;
	}

	/**
	 * Set the value of chamado
	 *
	 * @return  self
	 */ 
	public function setChamado($chamado)
	{
		$this->chamado = $chamado;

		return $this;
	}

	/**
	 * Get the value of observacoes
	 */ 
	public function getObservacoes()
	{
		return $this->observacoes;
	}

	/**
	 * Set the value of observacoes
	 *
	 * @return  self
	 */ 
	public function setObservacoes($observacoes)
	{
		$this->observacoes = $observacoes;

		return $this;
	}

	/**
	 * Get the value of data
	 */ 
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Set the value of data
	 *
	 * @return  self
	 */ 
	public function setData($data)
	{
		$this->data = $data;

		return $this;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}
}
