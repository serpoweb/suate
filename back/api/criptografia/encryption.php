<?php 
	//Chave Tem que ter 9 letras
	$key = "coworking";
	//Valor qualquer
	//$value = "";
	//Comparação 
	//$comparation = "";
	//Criptografar o valor
	//echo "Valor: ",$value,"<br>","Criptografado: ",encrypt($key,$value),"<br>";
	//var_dump(decrypt($key,$value,$comparation));
	//Criptografia recebe chave e valor a ser criptografado
	function encrypt($value){
		//Deixa minusculo a chave
		$key = "coworking";
		//Le o valor 
		$value = (string) $value;
		//Verifica a chave
		if(strlen($key) != 9 || is_numeric(filter_var($key, FILTER_SANITIZE_NUMBER_INT))){ return false;}
		$count = 0;
		//Soma os equivalencias da chave em numero
		for ($i=0; $i < strlen($key); $i++) { 
			$count += criptoCesarNumber($key{$i});
		}
		//Retorna o valor com três níveis de criptogracia
		//Codifica a cifra de cesar com chave baseada na soma e aplica um md5 em cima de um sha1
		$v = md5(sha1(criptoCesar($value,+$count)));
		return $v;
	}
	function criptoCesar($str, $shift){
		//Cria um array com o alfabeto
	    $char = range('a', 'z');
	    $flip = array_flip($char);
	    //Percorre o valor trocando as letrar conforme o chave ($shift)
	    for ($i = 0; $i < strlen($str); $i++) {
	        if (in_array(strtolower($str{$i}), $char)) {
	            $ord = $flip[strtolower($str{$i})];
	            $ord = ($ord + $shift) % 26;
	            if ($ord < 0) $ord += 26;
	            $str{$i} = ($str{$i} == strtolower($str{$i})) ? $char[$ord] : strtoupper($char[$ord]);
	        }
	    }
	    //Retorna o valor trocado
	    return $str;
	}
	function criptoCesarNumber($str){
	    $char = range('a', 'z');
	    $flip = array_flip($char);
	    for ($i = 0; $i < strlen($str); $i++) {
	        if (in_array(strtolower($str{$i}), $char)) {
	            $ord = $flip[strtolower($str{$i})];
	        }
	    }
	    //Retorna o numero da letra no alfabeto
	    return $ord;
	}
	function decrypt($value,$comparation){
		//Deixa minusculo a chave
		$key = "coworking";
		//Le o valor 
		$value = (string) $value;
		//Verifica a chave
		if(strlen($key) != 9 || is_numeric(filter_var($key, FILTER_SANITIZE_NUMBER_INT))){ return "invalid key";}
		$count = 0;
		//Soma os equivalencias da chave em numero
		for ($i=0; $i < strlen($key); $i++) { 
			$count += criptoCesarNumber($key{$i});
		}
		//Retorna o valor com três níveis de criptogracia
		//Codifica a cifra de cesar com chave baseada na soma e aplica um md5 em cima de um sha1
		$v = md5(sha1(criptoCesar($value,+$count)));
		//Retorna se os dois valores são iguais
		if ($v === $comparation) {
			return true;
		}else{
			return false;
		}
	}
?>