<?php
    try{
        $cidade = new Cidade();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        $cidade_id = (int) $_POST["cidade_id"] ?? null;

        if(!$cidade->procurarId($cidade_id)){

            $response = array("error"=>true,"msg"=>"Essa cidade não existe");
            echo json_encode($response); exit;

        }

       
        if($cidade->deletar($cidade_id)){

            $response = array("error"=>false,"msg"=>"Cidade deletada com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Cidade não deletada com sucesso.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>