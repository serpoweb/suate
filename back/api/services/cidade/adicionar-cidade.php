<?php
    try{
        $cidade = new Cidade();
        $estado = new Estado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        $estado_id = (int) $_POST["estado_id"] ?? null;

        $nome = $_POST["nome"] ?? null;

        if(!$estado->procurarId($estado_id)){

            $response = array("error"=>true,"msg"=>"Este estado não existe");
            echo json_encode($response); exit;

        }

        if($cidade->procurarNome($nome)){

            $response = array("error"=>true,"msg"=>"Já existem uma cidade com esse nome");
            echo json_encode($response); exit;

        }

        $cidade->setNome($nome);
      
        if($cidade->adicionar($estado_id)){

            $response = array("error"=>false,"msg"=>"Cidade adicionada com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Cidade não adicionada com sucesso.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>