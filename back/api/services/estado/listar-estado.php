<?php
    try{
        $estado = new Estado();
        $estado_id = (int) $_POST["estado_id"];
        $res = $estado->listar($estado_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Estado especifico","estado"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Esse estado não existe");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>