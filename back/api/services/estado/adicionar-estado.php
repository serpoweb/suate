<?php
    try{
        $estado = new Estado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
       
        $nome = $_POST["nome"] ?? null;

        if($estado->procurarNome($nome)){

            $response = array("error"=>true,"msg"=>"Este estado já existe");
            echo json_encode($response); exit;

        }

        $estado->setNome($nome);
      
        if($estado->adicionar()){

            $response = array("error"=>false,"msg"=>"Estado adicionado com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Estado não adicionado com sucesso.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>