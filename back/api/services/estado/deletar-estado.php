<?php
    try{
        $estado = new Estado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
       
        $estado_id = $_POST["estado_id"] ?? null;

        if(!$estado->procurarId($estado_id)){

            $response = array("error"=>true,"msg"=>"Este estado não existe");
            echo json_encode($response); exit;

        }

        $estado->setId($estado_id);
    
        if($estado->deletar(null)){

            $response = array("error"=>false,"msg"=>"Estado deletado com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Estado não deletado.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>