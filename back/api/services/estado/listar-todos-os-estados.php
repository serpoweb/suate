<?php
    try{
        $estado = new Estado();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $estados = $estado->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $estados = $estado->listarTudo();
        else if(isset($limite,$inicio))
            $estados = $estado->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de estados","estados"=>($estados));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>