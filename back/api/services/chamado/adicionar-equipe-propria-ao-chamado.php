<?php
    try{
        $chamado = new Chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        
       
        $equipe_id = $_POST["equipe_id"] ?? null;
        $chamado_id = $_POST["chamado_id"] ?? null;

        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
        $equipe = new Equipe();

        if(!$equipe->procurarId($equipe_id)){

            $response = array("error"=>true,"msg"=>"Essa equipe não existe");
            echo json_encode($response); exit;

        }
        $equipe_propria = new Equipe_propria();
        $equipe_propria->setId($equipe_id);
        $chamado->setId($chamado_id);

        if($chamado->adicionarEquipePropria($equipe_id)){
            $response = array("error"=>false,"msg"=>"Equipe vinculada ao chamado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe não vinculada :(");
        }   

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>