<?php
    try{
        $chamado = new Chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        
       
        $proprio_id = $_POST["proprio_id"] ?? null;
        $chamado_id = $_POST["chamado_id"] ?? null;

       

        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
        $proprio = new Proprio();

        if(!$proprio->procurarId($proprio_id)){

            $response = array("error"=>true,"msg"=>"Esse proprio não existe");
            echo json_encode($response); exit;

        }
        $chamado->setId($chamado_id);
        $chamado->setProprio_id($proprio_id);

        if($chamado->adicionarProprio()){
            $response = array("error"=>false,"msg"=>"Próprio vinculado ao chamado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Proprio não vinculado :(");
        }   

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>