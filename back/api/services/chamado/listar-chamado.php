<?php
    try{
        $chamado = new Chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $chamado_id = $_POST["chamado_id"] ?? null;
        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
        $res = $chamado->listar($chamado_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Chamado especifico","chamado"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Chamado não encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>