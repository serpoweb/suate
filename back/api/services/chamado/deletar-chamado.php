<?php
    try{
        $chamado = new Chamado();
       
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }

        $chamado_id = $_POST["chamado_id"] ?? null;

        if(!$chamado->procurarId($chamado_id)){
            $response = array('error'=>true,'msg'=>'Esse chamado não existe');
            echo json_encode($response); exit;
        }
      
        if($chamado->deletar($chamado_id))
            $response = array('error'=>false,'msg'=>'Chamado deletado :)');
        else 
            $response = array('error'=>true,'msg'=>'Chamado não foi deletado :(');
        
        echo json_encode($response); exit;
    }catch(Exception $e){
        $response = array('error'=>true,'msg'=>'Erro desconhecido');
        echo json_encode($response); exit;
    }
?>  