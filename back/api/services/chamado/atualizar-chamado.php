<?php
    try{
        $chamado = new Chamado();
        $proprio = new Proprio();
        
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }

        $situacao = $_POST["situacao"] ?? null;
        $data_abertura = $_POST["data_abertura"] ?? null;
        $classe_manutencao = $_POST["classe_manutencao"] ?? null;
        $periodicidade = $_POST["periodicidade"] ?? null;
        $atividade = $_POST["atividade"] ?? null;
        $observacoes = $_POST["observacoes"] ?? null;
        $proprio_id = $_POST["proprio_id"] ?? null;
        $palavra_chave_id = $_POST["palavra_chave_id"] ?? null;
        $chamado_id = $_POST["chamado_id"] ?? null;

        if(!$chamado->procurarId($chamado_id)){
            $response = array('error'=>true,'msg'=>'Esse chamado não existe');
            echo json_encode($response); exit;
        }
        if(!$proprio->procurarPalavraChaveId($palavra_chave_id)){
            $response = array('error'=>true,'msg'=>'Essa palavra chave não existe');
            echo json_encode($response); exit;
        }
        if(!$proprio->procurarId($proprio_id)){
            $response = array('error'=>true,'msg'=>'Esse próprio não existe');
            echo json_encode($response); exit;
        }

        $chamado->setId($chamado_id);
        $chamado->setSituacao($situacao);
        $chamado->setData_abertura($data_abertura);
        $chamado->setClass_manut($classe_manutencao);
        $chamado->setPeriodicidade($periodicidade);
        $chamado->setAtividade($atividade);
        $chamado->setObservacoes($observacoes);
        $chamado->setProprio_id($proprio_id);
        $chamado->setPalavra_chave_id($palavra_chave_id);

        if($chamado->atualizar())
            $response = array('error'=>false,'msg'=>'Chamado atualizado com sucesso :)');
        else 
            $response = array('error'=>true,'msg'=>'Chamado não foi atualizado :(');
        
        echo json_encode($response); exit;
    }catch(Exception $e){
        $response = array('error'=>true,'msg'=>'Erro desconhecido');
        echo json_encode($response); exit;
    }
?>  