<?php
    try{
        $chamado = new Chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        
       
        $palavra_chave_id = $_POST["palavra_chave_id"] ?? null;
        $chamado_id = $_POST["chamado_id"] ?? null;

       

        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
        $proprio = new Proprio();

        if(!$proprio->procurarPalavraChaveId($palavra_chave_id)){

            $response = array("error"=>true,"msg"=>"Essa palavra chave não existe");
            echo json_encode($response); exit;

        }
        $chamado->setId($chamado_id);
        $chamado->setPalavra_chave_id($palavra_chave_id);

        if($chamado->adicionarPalavraChave()){
            $response = array("error"=>false,"msg"=>"Palavra Chave vinculada ao chamado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Palavra Chave não vinculado :(");
        }   

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>