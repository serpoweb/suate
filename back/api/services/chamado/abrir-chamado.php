<?php
    try{
        $chamado = new Chamado();
        $secretaria = new Secretaria();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        
        $sigla = $_POST["sigla"] ?? null;
        $classe = $_POST["classe"] ?? null;
        $periodicidade = $_POST["periodicidade"] ?? null;
        $atividade = $_POST["atividade"] ?? null;
        $observacoes = $_POST["observacoes"] ?? null;
        $proprio_id = $_POST["proprio_id"] ?? null;
        $palavra_chave_id = $_POST["palavra_chave_id"] ?? null;
        $situacao = $_POST["situacao"] ?? null;

        $proprio = new Proprio();

        if(!$proprio->procurarId($proprio_id)){

            $response = array("error"=>true,"msg"=>"Esse próprio não existe");
            echo json_encode($response); exit;

        }

        if(!$proprio->procurarPalavraChaveId($palavra_chave_id)){

            $response = array("error"=>true,"msg"=>"Essa palavra chave não existe");
            echo json_encode($response); exit;

        }
        $chamado->setSituacao($situacao);
        $chamado->setClass_manut($classe);
        $chamado->setPeriodicidade($periodicidade);
        $chamado->setAtividade($atividade);
        $chamado->setObservacoes($observacoes);
        $chamado->setProprio_id($proprio_id);
        $chamado->setPalavra_chave_id($palavra_chave_id);
        if($chamado->procurarAtividade($atividade)){

            $response = array("error"=>true,"msg"=>"Já existe um chamado com essa atividade.");
            echo json_encode($response); exit;

        }
        if(is_null($sigla))
            $sigla = $secretaria->listar($proprio->listar($proprio_id)['secretaria_id'])['abreviatura'];
        if($chamado->abrir($sigla)){
            $response = array("error"=>false,"msg"=>"Chamado aberto com sucesso :)","novo_chamado_id"=>$chamado->getId());
        }else{
            $response = array("error"=>true,"msg"=>"Chamado não aberto :(");
        }   

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>