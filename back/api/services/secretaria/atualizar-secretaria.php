<?php 
    try{
        $secretaria_id = $_POST["secretaria_id"];
        $nome = $_POST["nome"];
        $abreviatura = $_POST["abreviatura"];
        $usuario_id = $_POST["usuario_id"];
        $secretaria = new Secretaria();
        if(!$secretaria->procurarId($secretaria_id)){
            $response = array("error"=>true,"msg"=>"Essa secretaria não existe");
            echo json_encode($response); exit;
        }
        $secretaria->setId($secretaria_id);
        $secretaria->setNome($nome);
        $secretaria->setAbreviatura($abreviatura);
        $usuario = new Usuarios();
        if(!$usuario->procurarId($usuario_id)){
            $response = array("error"=>true,"msg"=>"Este usuário não existe");
            echo json_encode($response); exit;
        }
        $secretaria->setUsuario($usuario_id);
        $response = array("error"=>null,"msg"=>null);
        if($secretaria->atualizar()){
            $response["error"] = false;
            $response["msg"] = "Secretaria atualizada com sucesso";
        }else{
            $response["error"] = true;
            $response["msg"] = "Secretaria não atualizada";
        }
        echo json_encode($response); exit;
       
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>