<?php
    try{
        $secretaria = new Secretaria();
        $secretaria_id = (int) $_POST["secretaria_id"];
        if($secretaria->deletar($secretaria_id)){
            $response = array("error"=>false,"msg"=>"Secretaria deletada com sucesso");    
        }else{
            $response = array("error"=>true,"msg"=>"Secretaria não deletada");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>