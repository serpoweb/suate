<?php
    try{
        $secretaria = new Secretaria();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $secretarias = $secretaria->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $secretarias = $secretaria->listarTudo();
        else if(isset($limite,$inicio))
            $secretarias = $secretaria->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de secretarias","secretarias"=>($secretarias));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>