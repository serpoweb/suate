<?php 
    try{
        $nome = $_POST["nome"];
        $abreviatura = $_POST["abreviatura"];
        $usuario_id = $_POST["usuario_id"];
        $secretaria = new Secretaria();
        $secretaria->setNome($nome);
        $secretaria->setAbreviatura($abreviatura);
        $usuario = new Usuarios();
        if(!$usuario->procurarId($usuario_id)){
            $response = array("error"=>true,"msg"=>"Este usuário não existe");
            echo json_encode($response); exit;
        }
        $secretaria->setUsuario($usuario_id);
        $response = array("error"=>null,"msg"=>null);
        if(!$secretaria->procuraNome()){
            if($secretaria->add()){
                $response["error"] = false;
                $response["msg"] = "Secretaria adicionada com sucesso";
            }else{
                $response["error"] = true;
                $response["msg"] = "Secretaria não adicionada";
            }
            echo json_encode($response); exit;
        }else{
            $response["error"] = true;
            $response["msg"] = "Já existe uma secretaria com esse nome";
            echo json_encode($response); exit;
        }
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>