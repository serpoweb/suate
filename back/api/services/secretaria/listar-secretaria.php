<?php
    try{
        $secretaria = new Secretaria();
        $secretaria_id = (int) $_POST["secretaria_id"];
        $res = $secretaria->listar($secretaria_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Secretaria especifica","secretaria"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Secretaria não existe");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>