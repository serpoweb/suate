<?php
    try{
        $contrato = new Contrato();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        $contrato_id = (int) $_POST["contrato_id"] ?? null;

        if(!$contrato->procurarId($contrato_id)){

            $response = array("error"=>true,"msg"=>"Esse contrato não existe");
            echo json_encode($response); exit;

        }

       
        if($contrato->deletar($contrato_id)){

            $response = array("error"=>false,"msg"=>"Contrato deletado com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Contrato não deletado.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>