<?php
    try{
        $contrato = new Contrato();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        if(isset($_POST["tercerizada_id"]) && $_POST["tercerizada_id"] != "")
            $tercerizada_id = $_POST["tercerizada_id"];
        else 
            $tercerizada_id = null;

        $chamado_id = $_POST["chamado_id"] ?? null;
        $chamado = new Chamado();

        if(!$chamado->procurarId($chamado_id)){
            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;
        }
       
        $data_de_contratacao = $_POST["data_de_contratacao"] ?? null;
        $valor_principal = $_POST["valor_principal"] ?? null;
        $valor1 = $_POST["valor1"] ?? null;
        $valor2 = $_POST["valor2"] ?? null;
        $valor3 = $_POST["valor3"] ?? null;
        $valor4 = $_POST["valor4"] ?? null;
        $valor5 = $_POST["valor5"] ?? null;
        $observacoes = $_POST["observacoes"] ?? null;
        $contrato->setData_de_contratacao($data_de_contratacao);
        $contrato->setValor_principal($valor_principal);
        $contrato->setValor1($valor1);
        $contrato->setValor2($valor2);
        $contrato->setValor3($valor3);
        $contrato->setValor4($valor4);
        $contrato->setValor5($valor5);
        $contrato->setObservacoes($observacoes);
        $contrato->setChamado($chamado_id);
        $contrato->setTercerizada($tercerizada_id);
        if($contrato->procurarChamado($chamado_id)){
            $response = array("error"=>true,"msg"=>"Já existe um contrato para esse chamado");
            echo json_encode($response); exit;
        }
        if($contrato->adicionar()){
            $response = array("error"=>false,"msg"=>"Contrato adicionado com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Contrato não adicionado.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>