<?php
    try{
        $contrato = new Contrato();
        $contrato_id = (int) $_POST["contrato_id"];
        
        if(!$contrato->procurarId($contrato_id)){
            $response = array("error"=>true,"msg"=>"Esse contrato não existe");
            echo json_encode($response); exit;    
        }
        $valor1 = $_POST["valor1"] ?? null;
        $valor2 = $_POST["valor2"] ?? null;
        $valor3 = $_POST["valor3"] ?? null;
        $valor4 = $_POST["valor4"] ?? null;
        $valor5 = $_POST["valor5"] ?? null;
        $chamado_id = $_POST["chamado_id"] ?? null;
        $tercerizada_id = $_POST["tercerizada_id"] ?? null;
        $observacoes = $_POST["observacoes"] ?? null;
        $valorPrincipal = $_POST["valor_principal"] ?? null;
       $contrato->setValor_principal($valorPrincipal);
       $contrato->setValor1($valor1);
        $contrato->setValor2($valor2);
        $contrato->setValor3($valor3);
        $contrato->setValor4($valor4);
        $contrato->setValor5($valor5);
        $contrato->setObservacoes($observacoes);
        $contrato->setChamado($chamado_id);
        $contrato->setTercerizada($tercerizada_id);
        $contrato->setId($contrato_id);
        if($contrato->atualizar()){
            $response = array("error"=>false,"msg"=>"Contrato atualizado com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Contrato não atualizado.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>