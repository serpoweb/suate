<?php
    try{
        $contrato = new Contrato();
        $contrato_id = (int) $_POST["contrato_id"];
        $res = $contrato->listar($contrato_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"contrato especifico","contrato"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Esse contrato não existe");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>