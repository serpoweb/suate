<?php
    try{
        $estatistica = new Estatistica();
       
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
     
        $response = array("error"=>false,"msg"=>"Estatisticas",'estatisticas'=>$estatistica->listarSi());


        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>