<?php
    try{
        $ocorrencia = new Ocorrencia();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $proprio_id = $_POST["proprio_id"] ?? null;
        $ocorrencia_id = $_POST["ocorrencia_id"] ?? null;
        $ocorrencia->setProprio_id($proprio_id);
        $ocorrencia->setId($ocorrencia_id);

        if($ocorrencia->adicionarProprio()){
            $response = array("error"=>false,"msg"=>"Proprio adicionado");
        }else{
            $response = array("error"=>true,"msg"=>"Proprio não adicionado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>