<?php
    try{
        $ocorrencia = new Ocorrencia();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $secretaria_id = $_POST["secretaria_id"] ?? null;
        $res = $ocorrencia->listarPorSecretaria($secretaria_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Lista de ocorrencias por secretaria","ocorrencias"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado",'ocorrencias'=>[]);
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>