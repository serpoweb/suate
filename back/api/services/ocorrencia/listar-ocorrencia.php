<?php
    try{
        $ocorrencia = new Ocorrencia();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $ocorrencia_id = $_POST["ocorrencia_id"] ?? null;
        $res = $ocorrencia->listarPorSecretaria($ocorrencia_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Ocorrencia específica","ocorrencia"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado",'ocorrencia'=>[]);
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>