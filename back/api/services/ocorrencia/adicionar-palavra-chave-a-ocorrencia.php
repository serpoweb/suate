<?php
    try{
        $ocorrencia = new Ocorrencia();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $palavra_chave = $_POST["palavra_chave"] ?? null;
        $ocorrencia_id = $_POST["ocorrencia_id"] ?? null;
        $ocorrencia->setPalavra_chave($palavra_chave);
        $ocorrencia->setId($ocorrencia_id);

        if($ocorrencia->adicionarPalavraChave()){
            $response = array("error"=>false,"msg"=>"Palavra chave adicionada");
        }else{
            $response = array("error"=>true,"msg"=>"Palavra chave não adicionada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>