<?php
    try{
        $ocorrencia = new Ocorrencia();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $foto = $_POST["foto"] ?? null;
        $ocorrencia_id = $_POST["ocorrencia_id"] ?? null;
        $ocorrencia->setFoto($foto);
        $ocorrencia->setId($ocorrencia_id);

        if($ocorrencia->adicionarFoto()){
            $response = array("error"=>false,"msg"=>"Foto adicionada com sucesso");
        }else{
            $response = array("error"=>true,"msg"=>"Foto não adicionada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>