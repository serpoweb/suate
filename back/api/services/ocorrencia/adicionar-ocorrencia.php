<?php
    try{
        $ocorrencia = new Ocorrencia();
       
        // if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

        //     $response = array("error"=>true,"msg"=>"Você não está autenticado");
        //     echo json_encode($response); exit;

        // }
        $situacao = $_POST['situacao'] ?? null;
        $cpf = $_POST['cpf'] ?? null;
        $descricao = $_POST['descricao'] ?? null;
        if(isset($_FILES['foto'])){
            $file_name = $_FILES['foto']['name'];
            $file_tmp =$_FILES['foto']['tmp_name'];
            $file_type=$_FILES['foto']['type'];
            @$file_ext = strtolower(end(explode('.',$file_name)));
            
            $expensions= array("jpeg","jpg","png");
            
            if(in_array($file_ext,$expensions)=== false){
                $response = array("error"=>true,"msg"=>"Foto não aceita.");
                echo json_encode($response); exit;
            }
            $file_new_name = md5($file_name).uniqid().time().'.'.$file_ext;
            $foto = $file_new_name;
            @move_uploaded_file($file_tmp,ROOT."/uploads/".$file_new_name);
        }else{
            $foto = null;
        }
        $palavra_chave_id = $_POST['palavra_chave_id'] ?? null;
        $chamado_id = $_POST['chamado_id'] ?? null;
        $proprio_id = $_POST['proprio_id'] ?? null;

        $ocorrencia->setSituacao($situacao);
        $ocorrencia->setCpf($cpf);
        $ocorrencia->setDescricao($descricao);
        $ocorrencia->setFoto($foto);
        $ocorrencia->setDescricao($descricao);
        $ocorrencia->setCpf($cpf);
        $ocorrencia->setSituacao($situacao);
        $ocorrencia->setProprio_id($proprio_id);
        $ocorrencia->setPalavra_chave($palavra_chave_id);
        $ocorrencia->setChamado_id($chamado_id);
            
        if($ocorrencia->adicionar()){

            $response = array("error"=>false,"msg"=>"Ocorrencia adicionada com sucesso");

        }
        else{
            
            $response = array("error"=>true,"msg"=>"Ocorrencia não adicionada.");

        }
        
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>