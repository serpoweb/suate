<?php
    try{
        $ocorrencia = new Ocorrencia();
       
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        $situacao = $_POST['situacao'] ?? null;
        $cpf = $_POST['cpf'] ?? null;
        $descricao = $_POST['descricao'] ?? null;
        $foto = $_POST['foto'] ?? null;
        $palavra_chave_id = $_POST['palavra_chave_id'] ?? null;
        $chamado_id = $_POST['chamado_id'] ?? null;
        $proprio_id = $_POST['proprio_id'] ?? null;
        $ocorrencia_id = $_POST['ocorrencia_id'] ?? null;

        $ocorrencia->setId($ocorrencia_id);
        $ocorrencia->setSituacao($situacao);
        $ocorrencia->setCpf($cpf);
        $ocorrencia->setDescricao($descricao);
        $ocorrencia->setFoto($foto);
        $ocorrencia->setDescricao($descricao);
        $ocorrencia->setCpf($cpf);
        $ocorrencia->setSituacao($situacao);
        $ocorrencia->setProprio_id($proprio_id);
        $ocorrencia->setPalavra_chave($palavra_chave_id);
        $ocorrencia->setChamado_id($chamado_id);
            
        if($ocorrencia->atualizar()){

            $response = array("error"=>false,"msg"=>"Ocorrencia atualizada");

        }
        else{

            $response = array("error"=>true,"msg"=>"Ocorrencia não atualizada.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>