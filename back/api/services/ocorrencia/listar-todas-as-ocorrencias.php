<?php
    try{
        $ocorrencia = new Ocorrencia();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $ocorrencias = $ocorrencia->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $ocorrencias = $ocorrencia->listarTudo();
        else if(isset($limite,$inicio))
            $ocorrencias = $ocorrencia->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de ocorrencias","ocorrencias"=>($ocorrencias));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>