<?php 
    try{
        $nome = $_POST["nome"];
        $email = $_POST["email"];
        $cpf = $_POST["cpf"];
        $ddd = $_POST["ddd"];
        $tipo = $_POST["tipo"];
        $senha = $_POST["senha"];
        $telefone = $_POST["telefone"];
        $Usuario = new Usuarios();
        $Usuario->setNome($nome);
        $Usuario->setEmail($email);
        $Usuario->setCpf($cpf);
        $Usuario->setDdd($ddd);
        $Usuario->setTipo($tipo);
        $Usuario->setTelefone($telefone);
        $Usuario->setSenha(encrypt($senha));
        $response = array("error"=>null,"msg"=>null);
        if(!$Usuario->procurarEmail()){
            if($Usuario->adicionarAdministrador()){
                $response["error"] = false;
                $response["msg"] = "Adiministrador adicionado com sucesso";
            }else{
                $response["error"] = true;
                $response["msg"] = "Administrador não adicionado";
            }
            echo json_encode($response); exit;
        }else{
            $response["error"] = true;
            $response["msg"] = "Já existe um usuário com esse email";
            echo json_encode($response); exit;
        }
        
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>