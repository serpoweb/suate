<?php
    try{
        $usuario = new Usuarios();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $usuario_id_a_bloquear = (int)$_POST["bloqueado_id"];
        if(!$usuario->procurarId($usuario_id_a_bloquear)){
            $response = array("error"=>true,"msg"=>"Usuário não existe.");
            echo json_encode($response); exit;
        }
        if($usuario->bloquearUsuario($usuario_id_a_bloquear)){
            $response = array("error"=>false,"msg"=>"Usuário bloqueado com sucesso.");
        }
        else{
            $response = array("error"=>true,"msg"=>"Usuário não bloqueado.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>