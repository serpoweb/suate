<?php
    try{
        $usuario = new Usuarios();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $usuario_id = (int) $_SESSION["usuario_id"] ?? null;
        $nome = $_POST["nome"] ?? null;
        $cpf = $_POST["cpf"] ?? null;
        $email = $_POST["email"] ?? null;
        $ddd = $_POST["ddd"] ?? null;
        $telefone = $_POST["telefone"] ?? null;
        $tipo = $_POST["tipo"] ?? null;
        $usuario->setNome($nome);
        $usuario->setCpf($cpf);
        $usuario->setEmail($email);
        $usuario->setDdd($ddd);
        $usuario->setTelefone($telefone);
        $usuario->setTipo($tipo);
        if($usuario->atualizarAdministrador()){
            $response = array("error"=>false,"msg"=>"Dados atualizados com sucesso");
            $usuario->sair();
        }
        else{
            $response = array("error"=>true,"msg"=>"Dados não atualizados.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>