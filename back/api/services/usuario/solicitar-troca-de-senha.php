<?php
    try{
        $usuario = new Usuarios();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $usuario_id = (int) $_SESSION["usuario_id"];
        $senha = encrypt($_POST["senha"]);
        $novaSenha = encrypt($_POST["nova_senha"]);
        $usuario->setId($usuario_id);
        $usuario->setSenha($senha);
        if($usuario->solicitarTrocaDeSenha($novaSenha))
            $response = array("error"=>false,"msg"=>"Senha atualizada com sucesso.");
        else
            $response = array("error"=>true,"msg"=>"Senha não atualizada, verifique seus dados");
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>