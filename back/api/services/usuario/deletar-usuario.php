<?php
    try{
        $usuario = new Usuarios();
        $usuario_id = (int) $_POST["usuario_id"];
        if($usuario->deletar($usuario_id))
            $response = array("error"=>false,"msg"=>"Usuário deletado com sucesso.");
        else
            $response = array("error"=>true,"msg"=>"Usuário não deletado.");
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>