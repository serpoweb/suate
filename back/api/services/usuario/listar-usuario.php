<?php
    try{
        $usuario = new Usuarios();
        $usuario_id = (int) $_POST["usuario_id"];
        $res = $usuario->listar($usuario_id);
        if($res != false)
            $response = array("error"=>false,"msg"=>"Usuário especifico","usuario"=>($res));
        else
            $response = array("error"=>true,"msg"=>"Usuário não existe");
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>