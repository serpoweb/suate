<?php 
    try{
        $email = $_POST["email"];
        $senha = encrypt($_POST["senha"]);
        $Usuario = new Usuarios();
        $response = array("error"=>null,"msg"=>null);
        if($Usuario->entrar($email,$senha)){
            $response["error"] = false;
            $response["msg"] = "Autenticado";
        }else{
            $response["error"] = true;
            $response["msg"] = "Email ou senha não encontrados.";
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>