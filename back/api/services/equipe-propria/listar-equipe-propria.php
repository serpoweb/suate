<?php
    try{
        $equipe_propria = new Equipe_propria();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $equipe_propria_id = $_POST["equipe_propria_id"] ?? null;
        if(!$equipe_propria->procurarId($equipe_propria_id)){

             $response = array("error"=>true,"msg"=>"Essa equipe propria não existe");
            echo json_encode($response); exit;

        }
        $res = $equipe_propria->listar($equipe_propria_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Equipe própria específica","equipe_propria"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Equipe própria não encontrada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>