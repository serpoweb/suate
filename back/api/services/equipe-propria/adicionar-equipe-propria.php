<?php
    try{
        $equipe_propria = new Equipe_propria();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $chamado_id = $_POST["chamado_id"] ?? null;

        $chamado = new Chamado();
        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
       
        $equipe_id = $_POST["equipe_id"] ?? null;
        $equipe = new Equipe();

        if(!$equipe->procurarId($equipe_id)){

             $response = array("error"=>true,"msg"=>"Essa equipe não existe");
            echo json_encode($response); exit;

        }

        $observacoes = $_POST["observacoes"] ?? null;
        $data = $_POST["data"] ?? null;

        $equipe_propria->setData($data);
        $equipe_propria->setObservacoes($observacoes);
        $equipe_propria->setChamado($chamado_id);
        $equipe_propria->setEquipe($equipe_id);

        if($equipe_propria->procurarChamado($chamado_id)){
            $response = array("error"=>true,"msg"=>"Já existe uma equipe pra esse chamado");
            echo json_encode($response); exit;
        }
        
        if($equipe_propria->adicionar()){
            $response = array("error"=>false,"msg"=>"Equipe própria adicionada com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe própria não adicionada :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>