<?php
    try{
        $equipe_propria = new Equipe_propria();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $equipes_proprias = $equipe_propria->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $equipes_proprias = $equipe_propria->listarTudo();
        else if(isset($limite,$inicio))
            $equipes_proprias = $equipe_propria->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de equipes próprias","equipes"=>($equipes_proprias));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>