<?php
    try{
        $equipe_propria = new Equipe_propria();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $equipe_propria_id = $_POST["equipe_propria_id"] ?? null;
    
        if($equipe_propria->deletar($equipe_propria_id)){
            $response = array("error"=>false,"msg"=>"Equipe própria deletada com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe própria não deletada :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>