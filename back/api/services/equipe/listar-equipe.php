<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $equipe_id = $_POST["equipe_id"] ?? null;
       
        $res = $equipe->listar($equipe_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Listagem de equipe","equipe"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>