<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $nome = $_POST["equipe_nome"] ?? null;
        $user_id = $_POST["user_id"] ?? null;
        $equipe_id = $_POST["equipe_id"] ?? null;
        $equipe->setNome($nome);
        $equipe->setUsuario($user_id);
        $equipe->setId($equipe_id);
        if($equipe->atualizar()){
            $response = array("error"=>false,"msg"=>"Equipe atualizada com sucesso");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe não atualizada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>