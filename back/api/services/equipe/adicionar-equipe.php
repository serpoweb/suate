<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $nome = $_POST["equipe_nome"] ?? null;
        $user_id = $_POST["user_id"] ?? null;
        $equipe->setNome($nome);
        $equipe->setUsuario($user_id);
        if($equipe->add()){
            $response = array("error"=>false,"msg"=>"Equipe adicionada com sucesso");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe não adicionada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>