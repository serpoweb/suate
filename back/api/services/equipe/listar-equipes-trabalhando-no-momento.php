<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
       
        $res = $equipe->listarTrabalhandoNoMomento();
        if($res != false){
            $response = array("error"=>false,"msg"=>"Listagem de equipes trabalhando no momento","equipes"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>