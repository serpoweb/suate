<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $secretaria_id = $_POST["secretaria_id"] ?? null;
       
        $res = $equipe->listarPorSecretaria($secretaria_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Listagem por secretaria","equipe"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>