<?php
    try{
        $equipe = new Equipe();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
        $equipe_id = $_POST["equipe_id"] ?? null;
       
        $equipe->setId($equipe_id);
        if($equipe->deletar()){
            $response = array("error"=>false,"msg"=>"Equipe deletada com sucesso");
        }else{
            $response = array("error"=>true,"msg"=>"Equipe não deletada");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>