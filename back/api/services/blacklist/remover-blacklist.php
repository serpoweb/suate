<?php
    try{
        $blacklist = new Blacklist();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $cpf = $_POST["cpf"] ?? null;
        
        if($blacklist->remover($cpf)){
            $response = array("error"=>false,"msg"=>"Cpf removido da lista negra");
        }else{
            $response = array("error"=>true,"msg"=>"Cpf continua na lista negra");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>