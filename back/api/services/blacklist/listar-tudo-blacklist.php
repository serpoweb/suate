<?php
    try{
        $blacklist = new Blacklist();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $blacklists = $blacklist->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $blacklists = $blacklist->listarTudo();
        else if(isset($limite,$inicio))
            $blacklists = $blacklist->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Blacklists","blacklists"=>($blacklists));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>