<?php
    try{
        $tercerizado = new Tercerizado();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
       
        $nome = $_POST["nome"] ?? null;
        $cnpj = $_POST["cnpj"] ?? null;
        $ddd = $_POST["ddd"] ?? null;
        $telefone = $_POST["telefone"] ?? null;
        $email = $_POST["email"] ?? null;
        $usuario_id = (int)$_POST["usuario_id"] ?? null;
        $tercerizado_id = (int)$_POST["tercerizado_id"] ?? null;
        
        if(!$tercerizado->procurarId($tercerizado_id)){

            $response = array("error"=>true,"msg"=>"Esse tercerizado não existe");
            echo json_encode($response); exit;

        }
        $usuario = new Usuarios();
        if(!$usuario->procurarId($usuario_id)){

            $response = array("error"=>true,"msg"=>"Esse usuário não existe");
            echo json_encode($response); exit;

        }
        $tercerizado->setId($tercerizado_id);
        $tercerizado->setNome($nome);
        $tercerizado->setCnpj($cnpj);
        $tercerizado->setDdd($ddd);
        $tercerizado->setTelefone($telefone);
        $tercerizado->setEmail($email);
        $tercerizado->setUsuario($usuario_id);
      
        if($tercerizado->atualizar()){

            $response = array("error"=>false,"msg"=>"Tercerizado atualizado com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Tercerizado não atualizado.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>