<?php
    try{
        $tercerizado = new Tercerizado();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }
       
        $tercerizado_id = (int)$_POST["tercerizado_id"] ?? null;
        
        if(!$tercerizado->procurarId($tercerizado_id)){

            $response = array("error"=>true,"msg"=>"Esse tercerizado não existe");
            echo json_encode($response); exit;

        }
      
        if($tercerizado->deletar($tercerizado_id)){

            $response = array("error"=>false,"msg"=>"Tercerizado deletado com sucesso");

        }
        else{

            $response = array("error"=>true,"msg"=>"Tercerizado não deletado.");

        }

        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>