<?php
    try{
        $tercerizado = new Tercerizado();
        $tercerizado_id = (int) $_POST["tercerizado_id"];
        $res = $tercerizado->listar($tercerizado_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Tercerizado especifico","estado"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Esse tercerizado não existe");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>