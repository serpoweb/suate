<?php
    try{
        $proprio = new Proprio();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $usuario_id = (int) $_SESSION["usuario_id"];
        if(isset($_POST["usuario_responsavel"]) && $_POST["usuario_responsavel"] != "")
            $usuario_id = $_POST["usuario_responsavel"];

        $usuario = new Usuarios();
        if(!$usuario->procurarId($usuario_id)){
            $response = array("error"=>true,"msg"=>"Usuário responsavel não existe");
            echo json_encode($response); exit;
        }
        $cidade_id = $_POST["cidade_id"] ?? null;
        $cidade = new Cidade();
        if(!$cidade->procurarId($cidade_id)){
            $response = array("error"=>true,"msg"=>"Cidade não existe");
            echo json_encode($response); exit;
        }
        $secretaria_id = $_POST["secretaria_id"] ?? null;
        $secretaria = new Secretaria();
        if(!$secretaria->procurarId($secretaria_id)){
            $response = array("error"=>true,"msg"=>"Secretaria não existe");
            echo json_encode($response); exit;
        }
        $nome = $_POST["nome"] ?? null;
        $endereco = $_POST["endereco"] ?? null;
        $numero = $_POST["numero"] ?? null;
        $bairro = $_POST["bairro"] ?? null;
        $cep = $_POST["cep"] ?? null;
        $horario_abert = $_POST["horario_abert"] ?? null;
        $horario_fech = $_POST["horario_fech"] ?? null;
        $ddd = $_POST["ddd"] ?? null;
        $telefone = $_POST["telefone"] ?? null;
        $latitude = $_POST["latitude"] ?? null;
        $longitude = $_POST["longitude"] ?? null;
        $pavimentos = $_POST["pavimentos"] ?? null;
        $data_contrucao = $_POST["data_construcao"] ?? null;
        $area_const = $_POST["area_const"] ?? null;
        $validade_avcb = $_POST["validade_avcb"] ?? null;
        $publico_alvo = $_POST["publico_alvo"] ?? null;
        $benif_manha = $_POST["benif_manha"] ?? null;
        $benif_tarde = $_POST["benif_tarde"] ?? null;
        $benif_noite = $_POST["benif_noite"] ?? null;
        $proprio_id = $_POST["proprio_id"] ?? null;
        $proprio->setId($proprio_id);
        $proprio->setNome($nome);
        $proprio->setEndereco($endereco);
        $proprio->setNumero($numero);
        $proprio->setBairro($bairro);
        $proprio->setcep($cep);
        $proprio->setHorario_abert($horario_abert);
        $proprio->setHorario_fech($horario_fech);
        $proprio->setDdd($ddd);
        $proprio->setTelefone($telefone);
        $proprio->setLatitude($latitude);
        $proprio->setLongitude($longitude);
        $proprio->setPavimentos($pavimentos);
        $proprio->setData_construcao($data_contrucao);
        $proprio->setArea_construida($area_const);
        $proprio->setValidade_avcb($validade_avcb);
        $proprio->setPublico_alvo($publico_alvo);
        $proprio->setBenef_manha($benif_manha);
        $proprio->setBenef_tarde($benif_tarde);
        $proprio->setBenef_noite($benif_noite);
        $proprio->setUsuarios_id($usuario_id);
        $proprio->setCidade_id($cidade_id);
        $proprio->setSecretaria_id($secretaria_id);
       
        if($proprio->atualizar()){
            $response = array("error"=>false,"msg"=>"Próprio atualizado com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Próprio não atualizado.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>