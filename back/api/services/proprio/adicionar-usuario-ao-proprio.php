<?php
    try{
        $proprio = new Proprio();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $usuario_id = (int) $_POST["usuario_id"];
        $proprio_id = (int) $_POST["proprio_id"];

        if(!$proprio->procurarId($proprio_id)){
            $response = array("error"=>true,"msg"=>"Este próprio não existe");
            echo json_encode($response); exit;
        }

        $usuario = new Usuarios();
        if(!$usuario->procurarId($usuario_id)){
            $response = array("error"=>true,"msg"=>"Usuário responsavel não existe");
            echo json_encode($response); exit;
        }
        $proprio->setId($proprio_id);
      
        if($proprio->adicionarUsuario($usuario_id)){
            $response = array("error"=>false,"msg"=>"Usuário atrelado ao próprio com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Usuário não atrelado ao próprio.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>