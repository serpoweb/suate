<?php
    try{
        $proprio = new Proprio();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $proprio_id = (int) $_POST["proprio_id"] ?? null;
        if(!$proprio->procurarId($proprio_id)){
            $response = array("error"=>true,"msg"=>"Esse próprio não existe");
            echo json_encode($response); exit;
        }
        if($proprio->deletar($proprio_id)){
            $response = array("error"=>false,"msg"=>"Próprio deletado com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Próprio não deletado.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>