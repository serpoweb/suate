<?php
    try{
        $proprio = new Proprio();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $secretaria_id = (int) $_POST["secretaria_id"];
        $secretaria = new Secretaria();
        if(!$secretaria->procurarId($secretaria_id)){
            $response = array("error"=>true,"msg"=>"Secretaria não existe");
            echo json_encode($response); exit;
        }
        $proprio_id = (int) $_POST["proprio_id"];

        if(!$proprio->procurarId($proprio_id)){
            $response = array("error"=>true,"msg"=>"Este próprio não existe");
            echo json_encode($response); exit;
        }

        $proprio->setId($proprio_id);
      
        if($proprio->adicionarSecretaria($secretaria_id)){
            $response = array("error"=>false,"msg"=>"Secretaria atrelada ao próprio com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Secretaria não atrelada ao próprio.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>