<?php
    try{
        $proprio = new Proprio();
        $secretaria_id = (int) $_POST["secretaria_id"];
        $res = $proprio->listarPorSecretaria($secretaria_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Lista de proprios por secretaria","proprios"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Nenhum próprio encontrado","proprios"=>null);    
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>