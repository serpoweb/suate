<?php
    try{
        $proprio = new Proprio();
        $proprio_id = (int) $_POST["proprio_id"];
        $res = $proprio->listar($proprio_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Próprio especifico","proprio"=>($res));    
        }else{
            $response = array("error"=>true,"msg"=>"Esse proprio não existe");
        }
         echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>