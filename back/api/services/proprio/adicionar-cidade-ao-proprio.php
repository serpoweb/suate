<?php
    try{
        $proprio = new Proprio();
        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){
            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;
        }
        $cidade_id = (int) $_POST["cidade_id"];
        $cidade = new Cidade();
        if(!$cidade->procurarId($cidade_id)){
            $response = array("error"=>true,"msg"=>"Cidade não existe");
            echo json_encode($response); exit;
        }
        $proprio_id = (int) $_POST["proprio_id"];

        if(!$proprio->procurarId($proprio_id)){
            $response = array("error"=>true,"msg"=>"Este próprio não existe");
            echo json_encode($response); exit;
        }

        $proprio->setId($proprio_id);
        
        if($proprio->adicionarCidade($cidade_id)){
            $response = array("error"=>false,"msg"=>"Cidade atrelada ao próprio com sucesso");
        }
        else{
            $response = array("error"=>true,"msg"=>"Cidade não atrelada ao próprio.");
        }
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>