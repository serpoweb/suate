<?php
    try{
        $proprio = new Proprio();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $proprios = $proprio->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $proprios = $proprio->listarTudo();
        else if(isset($limite,$inicio))
            $proprios = $proprio->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de próprios","proprios"=>($proprios));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>