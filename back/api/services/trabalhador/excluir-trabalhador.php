<?php
    try{
        $trabalhador = new Trabalhador();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

      
        $trabalhador_id = $_POST["trabalhador_id"] ?? null;
        $trabalhador->setId($trabalhador_id);
      
        if($trabalhador->deletar()){
            $response = array("error"=>false,"msg"=>"Trabalhador deletado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Trabalhador não deletado :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>