<?php
    try{
        $trabalhador = new Trabalhador();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

      
        $trabalhador_id = $_POST["trabalhador_id"] ?? null;
        $equipe_id = $_POST["equipe_id"] ?? null;
        $trabalhador->setId($trabalhador_id);
        $trabalhador->setEquipe($equipe_id);
        if($trabalhador->alocarParaEquipe()){
            $response = array("error"=>false,"msg"=>"Trabalhador alocado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Trabalhador não alocado :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>