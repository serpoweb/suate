<?php
    try{
        $trabalhador = new Trabalhador();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $nome = $_POST["nome"] ?? null;
        $cpf = $_POST["cpf"] ?? null;
        $telefone = $_POST["telefone"] ?? null;
        $ddd = $_POST["ddd"] ?? null;
        $equipe_id = $_POST["equipe_id"] ?? null;
       
        $trabalhador->setEquipe($equipe_id);
        $trabalhador->setNome($nome);
        $trabalhador->setCpf($cpf);
        $trabalhador->setDdd($ddd);
        $trabalhador->setTelefone($telefone);
        if($trabalhador->adicionar()){
            $response = array("error"=>false,"msg"=>"Trabalhador adicionado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Trabalhador não adicionado :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>