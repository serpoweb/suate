<?php
    try{
        $trabalhador = new Trabalhador();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $trabalhador_id = $_POST["trabalhador_id"] ?? null;
        $res = $trabalhador->listar($trabalhador_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Trabalhador especifico","trabalhador"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Nada encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>