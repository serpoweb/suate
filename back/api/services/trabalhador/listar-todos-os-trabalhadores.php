<?php
    try{
        $trabalhador = new Trabalhador();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $trabalhadores = $trabalhador->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $trabalhadores = $trabalhador->listarTudo();
        else if(isset($limite,$inicio))
            $trabalhadores = $trabalhador->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de trabalhadores","trabalhadores"=>($trabalhadores));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>