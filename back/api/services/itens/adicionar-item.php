<?php
    try{
        $item = new item_chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $chamado_id = $_POST["chamado_id"] ?? null;

        $chamado = new Chamado();
        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
       
        $descricao = $_POST["descricao"] ?? null;
        if($item->procurarDescricao($descricao)){

             $response = array("error"=>true,"msg"=>"Já existe um item com essa descrição");
            echo json_encode($response); exit;

        }

        $unidade = $_POST["unidade"] ?? null;
        $quantidade = $_POST["quantidade"] ?? null;
        $observacao = $_POST["observacao"] ?? null;
        $item->setDescricao($descricao);
        $item->setUnidade($unidade);
        $item->setQuantidade($quantidade);
        $item->setObservacao($observacao);
        $item->setChamado($chamado_id);
        if($item->adicionar()){
            $response = array("error"=>false,"msg"=>"Item adicionado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Item não adicionado :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>