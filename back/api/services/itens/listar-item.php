<?php
    try{
        $item = new item_chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $item_id = $_POST["item_id"] ?? null;
        if(!$item->procurarId($item_id)){

             $response = array("error"=>true,"msg"=>"Esse item não existe");
            echo json_encode($response); exit;

        }
        $res = $item->listar($item_id);
        if($res != false){
            $response = array("error"=>false,"msg"=>"Item especifico","item"=>$res);
        }else{
            $response = array("error"=>true,"msg"=>"Item não encontrado");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>