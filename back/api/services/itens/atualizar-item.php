<?php
    try{
        $item = new item_chamado();

        if(!isset($_SESSION["usuario_id"]) || $_SESSION["usuario_id"] == ""){

            $response = array("error"=>true,"msg"=>"Você não está autenticado");
            echo json_encode($response); exit;

        }

        $chamado_id = $_POST["chamado_id"] ?? null;

        $chamado = new Chamado();
        if(!$chamado->procurarId($chamado_id)){

            $response = array("error"=>true,"msg"=>"Esse chamado não existe");
            echo json_encode($response); exit;

        }
        $item_id = $_POST["item_id"] ?? null;

        if(!$item->procurarId($item_id)){

            $response = array("error"=>true,"msg"=>"Esse item não existe");
            echo json_encode($response); exit;

        }
        $descricao = $_POST["descricao"] ?? null;
        $unidade = $_POST["unidade"] ?? null;
        $quantidade = $_POST["quantidade"] ?? null;
        $observacao = $_POST["observacao"] ?? null;
        $item->setDescricao($descricao);
        $item->setUnidade($unidade);
        $item->setQuantidade($quantidade);
        $item->setObservacao($observacao);
        $item->setChamado($chamado_id);
        $item->setId($item_id);
        if($item->atualizar()){
            $response = array("error"=>false,"msg"=>"Item atualizado com sucesso :)");
        }else{
            $response = array("error"=>true,"msg"=>"Item não atualizado :(");
        }
        echo json_encode($response); exit;

    }catch(Exception $ex){

        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;

    }
?>