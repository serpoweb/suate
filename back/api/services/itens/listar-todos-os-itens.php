<?php
    try{
        $item = new item_chamado();
        if(isset($_POST["limite"]) && $_POST["limite"] != ""){
            $limite = (int) $_POST["limite"];
        }
        if(isset($_POST["inicio"]) && $_POST["inicio"] != ""){
            $inicio = (int) $_POST["inicio"];
        }
        if(isset($limite) && !isset($inicio))
            $itens = $item->listarTudo($limite,null);
        else if(!isset($limite,$inicio))
            $itens = $item->listarTudo();
        else if(isset($limite,$inicio))
            $itens = $item->listarTudo($limite,$inicio);
        $response = array("error"=>false,"msg"=>"Lista de itens","itens"=>($itens));
        echo json_encode($response); exit;
    }catch(Exception $ex){
        $response = array("error"=>true,"msg"=>"Erro desconhecido");
        echo json_encode($response); exit;
    }
?>