<?php 
    session_start();

    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

    date_default_timezone_set('America/Sao_Paulo');

    define("ROOT",dirname(dirname(dirname(__FILE__))));
    function now(string $formato_final = 'Y-m-d H:i:s'){
        return (new DateTime('now'))->format($formato_final);
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    function verClasseManutencao($s){
        $s = (int) $s;
        switch($s){
            case 1: return 'Manutenção Preventiva'; break;
            case 2: return 'Manutenção Corretiva'; break;
            case 3: return 'Manutenção Imprevisível'; break;
        }
    }
    function verSituacao($s){
        $s = (int) $s;
        switch($s){
            case 1: return 'Não executado'; break;
            case 2: return 'Em execução'; break;
            case 3: return 'Executado'; break;
            case 4: return 'Cancelado'; break;
        }
    }
    function verSituacaoMunicipe($s){
        $s = (int) $s;
        switch($s){
            case 1: return 'Aguardando aprovação'; break;
            case 2: return 'Em andamento'; break;
            case 3: return 'Executado'; break;
            case 4: return 'Aguardando aprovação'; break;
        }
    }
    function verTipoDeUsuario($t){
        $t = (int) $t;
        switch($t){
            case 1: return 'Responsável Geral'; break;
            case 2: return 'Responsável da Secretaria'; break;
            case 3: return 'Responsável Local'; break;
            case 4: return 'Responsável de Equipe Própria'; break;
            case 5: return 'Responsável de Terceirizada'; break;
        }
    }
    function listarTodasAsSituacoes(){
        for($i = 0; $i < 4; $i++){
            echo '<option id="'.($i+1).'">'.verSituacao($i+1).'</option>';
        }
    }
    function listarTodasAsClassManut(){
        for($i = 0; $i < 3; $i++){
            echo '<option id="'.($i+1).'">'.verClasseManutencao($i+1).'</option>';
        }
    }
    function listarTodosOsTiposDeUsuarios(){
        for($i = 0; $i < 5; $i++){
            echo '<option id="'.($i+1).'">'.verTipoDeUsuario($i+1).'</option>';
        }
    }

    require_once(ROOT.'/back/api/criptografia/encryption.php');
    require_once(ROOT.'/back/classes/Database.php');
    require_once(ROOT.'/back/classes/Blacklist.php');
    require_once(ROOT.'/back/classes/Chamado.php');
    require_once(ROOT.'/back/classes/Cidade.php');
    require_once(ROOT.'/back/classes/Contrato.php');
    require_once(ROOT.'/back/classes/Equipe_propria.php');
    require_once(ROOT.'/back/classes/Equipe.php');
    require_once(ROOT.'/back/classes/Estado.php');
    require_once(ROOT.'/back/classes/item_chamado.php');
    require_once(ROOT.'/back/classes/Ocorrencia.php');
    require_once(ROOT.'/back/classes/Proprio.php');
    require_once(ROOT.'/back/classes/Secretaria.php');
    require_once(ROOT.'/back/classes/Tercerizado.php');
    require_once(ROOT.'/back/classes/Trabalhador.php');
    require_once(ROOT.'/back/classes/Usuarios.php');
    require_once(ROOT.'/back/classes/Estatistica.php');
    $NDA = $_SESSION["usuario_nivel_de_acesso"] ?? 0;
    function listarTodosOsPropriosPorCategoria($categoria){
        $p = new Proprio();
        // var_dump($categoria);
        $pr = $p->listarPorSecretaria($categoria);
        // var_dump($proprios);
        for($i = 0; $i < count($pr); $i++){
            echo '<option id="'.($pr[$i]['id']).'">'.$pr[$i]['nome'].'</option>';
        }
    }
    function listarTodosOsProprios(){
        $p = new Proprio();
        // var_dump($categoria);
        $proprios = $p->listarTudo();
        // var_dump($proprios);
        for($i = 0; $i < count($proprios); $i++){
            echo '<option id="'.($proprios[$i]['id']).'">'.$proprios[$i]['nome'].'</option>';
        }
    }
    function listarTodasPalavrasChave(){
        $p = new Proprio();
        // var_dump($categoria);
        $proprios = $p->listarTodasPalavrasChave();
        // var_dump($proprios);
        for($i = 0; $i < count($proprios); $i++){
            echo '<option id="'.($proprios[$i]['id']).'">'.$proprios[$i]['descricao'].'</option>';
        }
    }

    // require_once(ROOT.'/api/testes.php');
    if(isset($_POST["type"])){
        $type = $_POST["type"] ?? "UNDEFINED";
        $image_extensions = array('jpeg','jpg','png','gif','PNG','JPEG','JPG');
        switch($type){
            //Começo das funções do Usuario

            //Necessita de POST: email,senha
            case "admin-login":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;

            //Necessita de POST: nome,email,cpf,ddd,tipo,senha,telefone
            case "adicionar-admin":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); }

            //Necessita de POST: nome,email,cpf,ddd,tipo,telefone
            case "atualizar-administrador":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); }

            //Necessita de POST: usuario_id
            case "deletar-usuario":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;

            //Necessita de POST: bloqueado_id
            case "bloquear-usuario":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;

            //Opcional de POST: limite,inicio
            case "listar-todos-os-usuarios":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;
            
            //Necessita de POST: usuario_id
            case "listar-usuario":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;
            
            //Necessita de POST: senha,nova_senha
            case "solicitar-troca-de-senha":{ require_once(ROOT.'/back/api/services/usuario/'.$type.'.php'); } break;
            
            //Fim das funções do Usuario

            //Começo das funções dos Próprios

            //Necessita de POST: proprio_id
            case "listar-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
            //Necessita de POST: secretaria_id
            case "listar-proprios-por-secretaria":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
            //Opcional de POST: limite, inicio
            case "listar-todos-os-proprios":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
            //Necessita de POST: precisa de uma caralhada de campos se quiser saber olha lá na classe do proprio
            case "adicionar-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
            case "editar-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
            //Necessita de POST: precisa de uma caralhada de campos se quiser saber olha lá na classe do proprio
            case "deletar-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
           //Necessita de POST: usuario_id,proprio_id
            case "adicionar-usuario-ao-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
           //Necessita de POST: secretaria_id,proprio_id
            case "adicionar-secretaria-ao-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
           //Necessita de POST: cidade_id,proprio_id
            case "adicionar-cidade-ao-proprio":{ require_once(ROOT.'/back/api/services/proprio/'.$type.'.php'); } break;
           
            //Fim das funções dos Próprios

            //Começo das funções da Secretaria

            //Opcional de POST: limite,inicio
            case "listar-todas-as-secretarias":{ require_once(ROOT.'/back/api/services/secretaria/'.$type.'.php'); } break;
            
            //Necessita POST: nome,abreviatura, usuario_id
            case "adicionar-secretaria":{ require_once(ROOT.'/back/api/services/secretaria/'.$type.'.php'); } break;
           
            //Necessita POST: nome,abreviatura, usuario_id
            case "atualizar-secretaria":{ require_once(ROOT.'/back/api/services/secretaria/'.$type.'.php'); } break;
            
            //Necessita POST: secretaria_id
            case "deletar-secretaria":{ require_once(ROOT.'/back/api/services/secretaria/'.$type.'.php'); } break;

            //Necessita POST: secretaria_id
            case "listar-secretaria":{ require_once(ROOT.'/back/api/services/secretaria/'.$type.'.php'); } break;
           
            //Fim das funções da Secretaria

            //Começo das funções de Cidade
           
            //Opcional POST: limite, inicio
            case "listar-todas-as-cidades":{ require_once(ROOT.'/back/api/services/cidade/'.$type.'.php'); } break;
            
            //Necessita POST: nome,estado_id
            case "adicionar-cidade":{ require_once(ROOT.'/back/api/services/cidade/'.$type.'.php'); } break;
            
            //Necessita POST: cidade_id
            case "deletar-cidade":{ require_once(ROOT.'/back/api/services/cidade/'.$type.'.php'); } break;
                

            //Fim das funções de Cidade

            //Inicio das funções de Estado 

            //Necessita de POST, nome
            case "adicionar-estado":{ require_once(ROOT.'/back/api/services/estado/'.$type.'.php'); } break;
            
            //Necessita de POST, estado_id
            case "deletar-estado":{ require_once(ROOT.'/back/api/services/estado/'.$type.'.php'); } break;
            
            //Opcional de POST, limite, inicio
            case "listar-todos-os-estados":{ require_once(ROOT.'/back/api/services/estado/'.$type.'.php'); } break;
             
            //Necessario POST estado_id
            case "listar-estado":{ require_once(ROOT.'/back/api/services/estado/'.$type.'.php'); } break;
             

            //Fim das funções de Estado

            //Inicio das funções de Tercerizado

            //Opcional de POST limite, inicio
            case "listar-todos-tercerizados":{ require_once(ROOT.'/back/api/services/tercerizado/'.$type.'.php'); } break;
             
            //Necessita de POST tercerizado_id
            case "listar-tercerizado":{ require_once(ROOT.'/back/api/services/tercerizado/'.$type.'.php'); } break;
             
            //Necessita de POST nome,cnpj,ddd,telefone,email,usuario_id
            case "adicionar-tercerizado":{ require_once(ROOT.'/back/api/services/tercerizado/'.$type.'.php'); } break;
             
            //Necessita de POST nome,cnpj,ddd,telefone,email,usuario_id,tercerizado_id
            case "atualizar-tercerizado":{ require_once(ROOT.'/back/api/services/tercerizado/'.$type.'.php'); } break;
             
            //Necessita de POST tercerizado_id
            case "deletar-tercerizado":{ require_once(ROOT.'/back/api/services/tercerizado/'.$type.'.php'); } break;

            //Fim das funções de tercerizado

            //Inicio das funções de Contrato

            //Opcional de POST limite, inicio
            case "listar-todos-os-contratos":{ require_once(ROOT.'/back/api/services/contrato/'.$type.'.php'); } break;

            //Necessita de POST data_de_cotratacao,valor_principal,valor1,valor2,valor3,valor4,valor5,observacoes,chamado_id,tercerizada_id
            case "adicionar-contrato":{ require_once(ROOT.'/back/api/services/contrato/'.$type.'.php'); } break;
            
            //Necessita de POST contrato_id
            case "listar-contrato":{ require_once(ROOT.'/back/api/services/contrato/'.$type.'.php'); } break;
            
           //Necessita de POST data_de_cotratacao,valor_principal,valor1,valor2,valor3,valor4,valor5,observacoes,chamado_id,tercerizada_id
            case "atualizar-contrato":{ require_once(ROOT.'/back/api/services/contrato/'.$type.'.php'); } break;
           
           //Necessita de POST contrato_id
            case "deletar-contrato":{ require_once(ROOT.'/back/api/services/contrato/'.$type.'.php'); } break;

            //Fim das funções de Contrato

            //Inicio das funções de Item de chamado
            
            //Opcional de POST limite, inicio
            case "listar-todos-os-itens":{ require_once(ROOT.'/back/api/services/itens/'.$type.'.php'); } break;

            //Necessita de POST descricao,unidade,quantidade,observacao,chamado_id
            case "adicionar-item":{ require_once(ROOT.'/back/api/services/itens/'.$type.'.php'); } break;
            
            //Necessita de POST contrato_id
            case "listar-item":{ require_once(ROOT.'/back/api/services/itens/'.$type.'.php'); } break;

            //Necessita de POST descricao,unidade,quantidade,observacao,chamado_id, item_id
            case "atualizar-item":{ require_once(ROOT.'/back/api/services/itens/'.$type.'.php'); } break;

            //Fim das funções de Item de chamado

            //Inicio das funções de Chamado 

            //Necessita de POST atividade, observações, etc...
            case "abrir-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            //Necessita de POST proprio_id,chamado_id
            case "adicionar-proprio-ao-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Necessita de POST palavra_chave_id,chamado_id
            case "adicionar-palavra-chave-ao-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Opcional de POST limite,inicio
            case "listar-todos-os-chamados":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;

            //Necessita de POST palavra_chave_id,chamado_id
            case "adicionar-equipe-propria-ao-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Necessita de POST chamado_id
            case "listar-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Necessita de POST situacao,chamado_id,_proprio_id,palavra_chave_id,_observacoes,atividade,periodicidade,data_abertura
            case "atualizar-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Necessita de POST chamdo_id
            case "deletar-chamado":{ require_once(ROOT.'/back/api/services/chamado/'.$type.'.php'); } break;
            
            //Fim das funções de Chamado

            //Inicio das funções de Equie_propria
            
            //Opcional de POST limite, inicio
            case "listar-todas-as-equipes-proprias":{ require_once(ROOT.'/back/api/services/equipe-propria/'.$type.'.php'); } break;
            
            //Necessita de POST observacoes, chamado_id, equipe_id
            case "adicionar-equipe-propria":{ require_once(ROOT.'/back/api/services/equipe-propria/'.$type.'.php'); } break;
            
            //Necessita de POST observacoes, chamado_id, equipe_id
            case "listar-equipe-propria":{ require_once(ROOT.'/back/api/services/equipe-propria/'.$type.'.php'); } break;
            
            //Necessita de POST observacoes, chamado_id, equipe_id, equipe_propria_id
            case "atualizar-equipe-propria":{ require_once(ROOT.'/back/api/services/equipe-propria/'.$type.'.php'); } break;
            
            //Necessita de POST equipe_id
            case "deletar-equipe-propria":{ require_once(ROOT.'/back/api/services/equipe-propria/'.$type.'.php'); } break;
            
            //Fim das funções de Equie_propria
      
            //Inicio das funções de Equipe
            
            case "listar-todas-as-equipes":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
           
            case "listar-equipe":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
            
            case "listar-equipes-trabalhando-no-momento":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
            
            case "adicionar-equipe":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
            
            case "atualizar-equipe":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
            
            case "deletar-equipe":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;
            
            case "listar-equipe-por-secretaria":{ require_once(ROOT.'/back/api/services/equipe/'.$type.'.php'); } break;

            //Fim das funções de equipe

            //Inicio das funçoes de trabalhador
            case "listar-todos-os-trabalhadores":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            case "listar-trabalhador":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            case "alocar-trabalhador-para-equipe":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            case "adicionar-trabalhador":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            case "editar-trabalhador":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            case "excluir-trabalhador":{ require_once(ROOT.'/back/api/services/trabalhador/'.$type.'.php'); } break;
            //Fim das funçoes de trabalhador

            //inicio das funções de ocorrencia
            case "listar-todas-as-ocorrencias":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "adicionar-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "adicionar-foto-a-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "adicionar-palavra-chave-a-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "adicionar-proprio-a-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "listar-ocorrencia-por-secretaria":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "listar-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "atualizar-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
            
            case "deletar-ocorrencia":{ require_once(ROOT.'/back/api/services/ocorrencia/'.$type.'.php'); } break;
           
            //Fim das funções de ocorrencia

            //Inicio das funções de blacklist
            
            case "adicionar-blacklist":{ require_once(ROOT.'/back/api/services/blacklist/'.$type.'.php'); } break;
            
            case "remover-blacklist":{ require_once(ROOT.'/back/api/services/blacklist/'.$type.'.php'); } break;
            
            case "listar-tudo-blacklist":{ require_once(ROOT.'/back/api/services/blacklist/'.$type.'.php'); } break;
           
            //Fim das funções de blacklist

            // Inicio das estatisticas 
            
            case "todas-as-informacoes-estatisticas":{ require_once(ROOT.'/back/api/services/estatistica/'.$type.'.php'); } break;
           
          case "atrelar-ocorrencia-ao-chamado":{
            $o = new Ocorrencia();
            $o->setId($_POST["ocorrencia"] ?? null);
            if($o->adicionarChamado($_POST["chamado"])){
              echo json_encode(array('error'=>false));
            }else{
              echo json_encode(array('error'=>true));
            }
          } break;
            
            // Fim das estatisticas 

            default: {
                echo json_encode(array('error'=>true,'msg'=>'Função desconhecida!')); exit;
            } break;
        }
    }
?>