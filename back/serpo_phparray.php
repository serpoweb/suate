<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.9
 */

/**
 * Database `u361912491_serpo`
 */

/* `u361912491_serpo`.`blacklist` */
$blacklist = array(
);

/* `u361912491_serpo`.`chamados` */
$chamados = array(
);

/* `u361912491_serpo`.`cidades` */
$cidades = array(
  array('id' => '1','nome' => 'Sorocaba','estado_id' => '1')
);

/* `u361912491_serpo`.`contratos` */
$contratos = array(
);

/* `u361912491_serpo`.`equipes` */
$equipes = array(
  array('id' => '5','nome' => 'Equipe A','usuario_id' => NULL),
  array('id' => '6','nome' => 'Equipe C','usuario_id' => NULL),
  array('id' => '7','nome' => 'Equipe D','usuario_id' => NULL),
  array('id' => '8','nome' => 'Equipe E','usuario_id' => NULL),
  array('id' => '9','nome' => 'Equipe B','usuario_id' => NULL)
);

/* `u361912491_serpo`.`equipes_propria` */
$equipes_propria = array(
);

/* `u361912491_serpo`.`estados` */
$estados = array(
  array('id' => '1','nome' => 'São Paulo')
);

/* `u361912491_serpo`.`itens_chamado` */
$itens_chamado = array(
);

/* `u361912491_serpo`.`ocorrencias` */
$ocorrencias = array(
  array('id' => '1','situacao' => '2','cpf' => '77797616060','descricao' => 'Quebram a porta','foto' => NULL,'palavra_chave_id' => '3','chamado_id' => NULL,'proprio_id' => '14'),
  array('id' => '2','situacao' => '2','cpf' => '78383575009','descricao' => 'Tentaram arrombar o predio','foto' => NULL,'palavra_chave_id' => '3','chamado_id' => NULL,'proprio_id' => '14'),
  array('id' => '3','situacao' => '2','cpf' => '78383575009','descricao' => 'Tentaram invadir pra roubar os equipamentos','foto' => NULL,'palavra_chave_id' => '3','chamado_id' => NULL,'proprio_id' => '14'),
  array('id' => '4','situacao' => '1','cpf' => '85376427030','descricao' => 'Roubaram todos os fios e o alarme','foto' => NULL,'palavra_chave_id' => '2','chamado_id' => NULL,'proprio_id' => '5')
);

/* `u361912491_serpo`.`palavras_chave` */
$palavras_chave = array(
  array('id' => '1','descricao' => 'Roubo de Equipamentos'),
  array('id' => '2','descricao' => 'Roubo de Fiação'),
  array('id' => '3','descricao' => 'Vandalismo'),
  array('id' => '4','descricao' => 'Pichação'),
  array('id' => '5','descricao' => 'Manutenção'),
  array('id' => '6','descricao' => 'Enchente'),
  array('id' => '7','descricao' => 'Incêncio'),
  array('id' => '8','descricao' => 'Depredação'),
  array('id' => '9','descricao' => 'Corte de Grama'),
  array('id' => '10','descricao' => 'Manutenção na Iluminação')
);

/* `u361912491_serpo`.`proprios` */
$proprios = array(
  array('id' => '1','nome' => 'Cemitério da Saudade','endereco' => 'Praça Pedro de Toledo','numero' => 's/n','bairro' => 'Além Linha','cep' => '18060080','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32311402','latitude' => '-23.491130','longitude' => '-47.460599','pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '2','nome' => 'Cemitério da Aparecida','endereco' => 'Rua Quirino de Melo','numero' => 's/n','bairro' => 'Aparecidinha','cep' => '18087200','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32254382','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '3','nome' => 'Cemitério da Consolação','endereco' => 'Rua Alcindo Guanabara','numero' => 's/n','bairro' => 'Vila Haro','cep' => '18015225','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32279011','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '4','nome' => 'Cemitério Santo Antônio','endereco' => 'Rua Luiz Gabriotti','numero' => '871','bairro' => 'Wanel Ville','cep' => '18055089','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32214448','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '5','nome' => 'Centro Operacional I','endereco' => 'Rua Peru','numero' => '425','bairro' => 'Vila Barcelona','cep' => '18025290','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32271277','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '6','nome' => 'Centro Operacional II','endereco' => 'Rua Vidal de Araújo','numero' => '277','bairro' => 'Jardim do Paço','cep' => '18087085','horario_abert' => NULL,'horario_fech' => NULL,'ddd' => NULL,'telefone' => NULL,'latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '13','nome' => 'Casa do Cidadão Nogueira Padilha','endereco' => 'Avenida Nogueira Padilha','numero' => '1460','bairro' => 'Vila Hortência','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32377171','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '14','nome' => 'Casa do Cidadão Brigadeiro Tobias','endereco' => 'Avenida Bandeirantes','numero' => '4155','bairro' => 'Brigadeiro Tobias','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32364371','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '15','nome' => 'Casa do Cidadão Ipiranga','endereco' => 'Rua Estado de Israel','numero' => '424','bairro' => 'Jardim Ipiranga','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32292950','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '16','nome' => 'Casa do Cidadão Éden','endereco' => 'Rua Bonifácio de Oliveira Cassú','numero' => '180','bairro' => 'Éden','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '33353480','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '17','nome' => 'Casa do Cidadão Itavuvu','endereco' => 'Avenida Itavuvu','numero' => '3415','bairro' => 'Parque das Laranjeiras','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32269292','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '18','nome' => 'Casa do Cidadão Ipanema','endereco' => 'Avenida Ipanema','numero' => '3349','bairro' => 'Vila Helena','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '33139440','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL),
  array('id' => '19','nome' => 'Casa do Cidadão Paço Municipal','endereco' => 'Avenida Engenheiro Carlos Reinaldo Mendes','numero' => '3041','bairro' => 'Alto da Boa Vista','cep' => NULL,'horario_abert' => NULL,'horario_fech' => NULL,'ddd' => '15','telefone' => '32382646','latitude' => NULL,'longitude' => NULL,'pavimentos' => NULL,'data_construcao' => NULL,'area_const' => NULL,'validade_avcb' => NULL,'publico_alvo' => NULL,'benif_manha' => NULL,'benif_tarde' => NULL,'benif_noite' => NULL,'usuario_id' => NULL,'cidade_id' => '1','secretaria_id' => NULL)
);

/* `u361912491_serpo`.`secretarias` */
$secretarias = array(
  array('id' => '2','nome' => 'Secretaria de Conservação, Serviços Públicos e Obras','abreviatura' => 'SERPO','usuario_id' => NULL),
  array('id' => '3','nome' => 'Secretaria de Cidadania e Participação Popular','abreviatura' => 'SECID','usuario_id' => NULL)
);

/* `u361912491_serpo`.`terceirizadas` */
$terceirizadas = array(
);

/* `u361912491_serpo`.`trabalhadores` */
$trabalhadores = array(
  array('id' => '1','cpf' => '35284888081','nome' => 'Lucas da Silva e Silva','ddd' => '15','telefone' => '996542123','equipe_id' => '5'),
  array('id' => '2','cpf' => '49983428008','nome' => 'Samuel Torres','ddd' => '15','telefone' => '998652342','equipe_id' => '5'),
  array('id' => '3','cpf' => '41836514093','nome' => 'Juvenal Eloy','ddd' => '15','telefone' => '981457862','equipe_id' => '7'),
  array('id' => '4','cpf' => '91166988007','nome' => 'Bob Grey','ddd' => '11','telefone' => '981256348','equipe_id' => NULL),
  array('id' => '5','cpf' => '94623719030','nome' => 'João Cavalcante','ddd' => '15','telefone' => '998301147','equipe_id' => '7'),
  array('id' => '6','cpf' => '42628152002','nome' => 'José Cravo','ddd' => '15','telefone' => '986547215','equipe_id' => '7'),
  array('id' => '7','cpf' => '01177728010','nome' => 'Matheus Leite','ddd' => '15','telefone' => '996257841','equipe_id' => '5'),
  array('id' => '8','cpf' => '93903062057','nome' => 'Gabriel da Silva','ddd' => NULL,'telefone' => '994253210','equipe_id' => '7'),
  array('id' => '9','cpf' => '30975138090','nome' => 'Heitor Gomes','ddd' => '15','telefone' => '30315420','equipe_id' => NULL),
  array('id' => '10','cpf' => '28173029075','nome' => 'Aiton da Silva','ddd' => '11','telefone' => '981546774','equipe_id' => NULL),
  array('id' => '11','cpf' => '73613928060','nome' => 'Roberto Luz','ddd' => '15','telefone' => '30345012','equipe_id' => NULL)
);

/* `u361912491_serpo`.`usuarios` */
$usuarios = array(
);
