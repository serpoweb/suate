-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 04/09/2018 às 23:06
-- Versão do servidor: 10.2.16-MariaDB
-- Versão do PHP: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u361912491_serpo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(6) NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamados`
--

CREATE TABLE `chamados` (
  `id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `situacao` int(4) NOT NULL,
  `data_abertura` date NOT NULL,
  `data_finalizacao` date DEFAULT NULL,
  `class_manut` int(4) DEFAULT NULL,
  `periodicidade` int(4) DEFAULT NULL,
  `atividade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprio_id` int(6) NOT NULL,
  `palavra_chave_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cidades`
--

CREATE TABLE `cidades` (
  `id` int(6) NOT NULL,
  `nome` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cidades`
--

INSERT INTO `cidades` (`id`, `nome`, `estado_id`) VALUES
(1, 'Sorocaba', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contratos`
--

CREATE TABLE `contratos` (
  `id` int(6) NOT NULL,
  `data_contratacao` date NOT NULL,
  `valor_principal` decimal(11,2) NOT NULL,
  `valor1` decimal(11,2) DEFAULT NULL,
  `valor2` decimal(11,2) DEFAULT NULL,
  `valor3` decimal(11,2) DEFAULT NULL,
  `valor4` decimal(11,2) DEFAULT NULL,
  `valor5` decimal(11,2) DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `terceirizada_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipes`
--

CREATE TABLE `equipes` (
  `id` int(6) NOT NULL,
  `nome` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `equipes`
--

INSERT INTO `equipes` (`id`, `nome`, `usuario_id`) VALUES
(5, 'Equipe A', NULL),
(6, 'Equipe C', NULL),
(7, 'Equipe D', NULL),
(8, 'Equipe E', NULL),
(9, 'Equipe B', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipes_propria`
--

CREATE TABLE `equipes_propria` (
  `id` int(6) NOT NULL,
  `data` date DEFAULT NULL,
  `observacoes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipe_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `estados`
--

CREATE TABLE `estados` (
  `id` int(6) NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `estados`
--

INSERT INTO `estados` (`id`, `nome`) VALUES
(1, 'São Paulo');

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens_chamado`
--

CREATE TABLE `itens_chamado` (
  `id` int(8) NOT NULL,
  `descricao` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `unidade` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantidade` double DEFAULT NULL,
  `observacao` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ocorrencias`
--

CREATE TABLE `ocorrencias` (
  `id` int(12) NOT NULL,
  `situacao` int(4) DEFAULT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `palavra_chave_id` int(6) NOT NULL,
  `chamado_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprio_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `ocorrencias`
--

INSERT INTO `ocorrencias` (`id`, `situacao`, `cpf`, `descricao`, `foto`, `palavra_chave_id`, `chamado_id`, `proprio_id`) VALUES
(1, 2, '77797616060', 'Quebram a porta', NULL, 3, NULL, 14),
(2, 2, '78383575009', 'Tentaram arrombar o predio', NULL, 3, NULL, 14),
(3, 2, '78383575009', 'Tentaram invadir pra roubar os equipamentos', NULL, 3, NULL, 14),
(4, 1, '85376427030', 'Roubaram todos os fios e o alarme', NULL, 2, NULL, 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `palavras_chave`
--

CREATE TABLE `palavras_chave` (
  `id` int(6) NOT NULL,
  `descricao` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `palavras_chave`
--

INSERT INTO `palavras_chave` (`id`, `descricao`) VALUES
(1, 'Roubo de Equipamentos'),
(2, 'Roubo de Fiação'),
(3, 'Vandalismo'),
(4, 'Pichação'),
(5, 'Manutenção'),
(6, 'Enchente'),
(7, 'Incêncio'),
(8, 'Depredação'),
(9, 'Corte de Grama'),
(10, 'Manutenção na Iluminação');

-- --------------------------------------------------------

--
-- Estrutura para tabela `proprios`
--

CREATE TABLE `proprios` (
  `id` int(6) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `horario_abert` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `horario_fech` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `pavimentos` int(4) DEFAULT NULL,
  `data_construcao` date DEFAULT NULL,
  `area_const` double DEFAULT NULL,
  `validade_avcb` date DEFAULT NULL,
  `publico_alvo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benif_manha` int(8) DEFAULT NULL,
  `benif_tarde` int(8) DEFAULT NULL,
  `benif_noite` int(8) DEFAULT NULL,
  `usuario_id` int(6) DEFAULT NULL,
  `cidade_id` int(6) DEFAULT NULL,
  `secretaria_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `proprios`
--

INSERT INTO `proprios` (`id`, `nome`, `endereco`, `numero`, `bairro`, `cep`, `horario_abert`, `horario_fech`, `ddd`, `telefone`, `latitude`, `longitude`, `pavimentos`, `data_construcao`, `area_const`, `validade_avcb`, `publico_alvo`, `benif_manha`, `benif_tarde`, `benif_noite`, `usuario_id`, `cidade_id`, `secretaria_id`) VALUES
(1, 'Cemitério da Saudade', 'Praça Pedro de Toledo', 's/n', 'Além Linha', '18060080', NULL, NULL, '15', '32311402', '-23.491130', '-47.460599', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(2, 'Cemitério da Aparecida', 'Rua Quirino de Melo', 's/n', 'Aparecidinha', '18087200', NULL, NULL, '15', '32254382', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(3, 'Cemitério da Consolação', 'Rua Alcindo Guanabara', 's/n', 'Vila Haro', '18015225', NULL, NULL, '15', '32279011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(4, 'Cemitério Santo Antônio', 'Rua Luiz Gabriotti', '871', 'Wanel Ville', '18055089', NULL, NULL, '15', '32214448', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(5, 'Centro Operacional I', 'Rua Peru', '425', 'Vila Barcelona', '18025290', NULL, NULL, '15', '32271277', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(6, 'Centro Operacional II', 'Rua Vidal de Araújo', '277', 'Jardim do Paço', '18087085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(13, 'Casa do Cidadão Nogueira Padilha', 'Avenida Nogueira Padilha', '1460', 'Vila Hortência', NULL, NULL, NULL, '15', '32377171', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(14, 'Casa do Cidadão Brigadeiro Tobias', 'Avenida Bandeirantes', '4155', 'Brigadeiro Tobias', NULL, NULL, NULL, '15', '32364371', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(15, 'Casa do Cidadão Ipiranga', 'Rua Estado de Israel', '424', 'Jardim Ipiranga', NULL, NULL, NULL, '15', '32292950', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(16, 'Casa do Cidadão Éden', 'Rua Bonifácio de Oliveira Cassú', '180', 'Éden', NULL, NULL, NULL, '15', '33353480', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(17, 'Casa do Cidadão Itavuvu', 'Avenida Itavuvu', '3415', 'Parque das Laranjeiras', NULL, NULL, NULL, '15', '32269292', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(18, 'Casa do Cidadão Ipanema', 'Avenida Ipanema', '3349', 'Vila Helena', NULL, NULL, NULL, '15', '33139440', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(19, 'Casa do Cidadão Paço Municipal', 'Avenida Engenheiro Carlos Reinaldo Mendes', '3041', 'Alto da Boa Vista', NULL, NULL, NULL, '15', '32382646', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `secretarias`
--

CREATE TABLE `secretarias` (
  `id` int(6) NOT NULL,
  `nome` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `abreviatura` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `secretarias`
--

INSERT INTO `secretarias` (`id`, `nome`, `abreviatura`, `usuario_id`) VALUES
(2, 'Secretaria de Conservação, Serviços Públicos e Obras', 'SERPO', NULL),
(3, 'Secretaria de Cidadania e Participação Popular', 'SECID', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `terceirizadas`
--

CREATE TABLE `terceirizadas` (
  `id` int(6) NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `trabalhadores`
--

CREATE TABLE `trabalhadores` (
  `id` int(6) NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipe_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `trabalhadores`
--

INSERT INTO `trabalhadores` (`id`, `cpf`, `nome`, `ddd`, `telefone`, `equipe_id`) VALUES
(1, '35284888081', 'Lucas da Silva e Silva', '15', '996542123', 5),
(2, '49983428008', 'Samuel Torres', '15', '998652342', 5),
(3, '41836514093', 'Juvenal Eloy', '15', '981457862', 7),
(4, '91166988007', 'Bob Grey', '11', '981256348', NULL),
(5, '94623719030', 'João Cavalcante', '15', '998301147', 7),
(6, '42628152002', 'José Cravo', '15', '986547215', 7),
(7, '01177728010', 'Matheus Leite', '15', '996257841', 5),
(8, '93903062057', 'Gabriel da Silva', NULL, '994253210', 7),
(9, '30975138090', 'Heitor Gomes', '15', '30315420', NULL),
(10, '28173029075', 'Aiton da Silva', '11', '981546774', NULL),
(11, '73613928060', 'Roberto Luz', '15', '30345012', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(6) NOT NULL,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ddd` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` int(4) NOT NULL,
  `senha` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_chamados_proprios_idx` (`proprio_id`),
  ADD KEY `fk_chamados_palavras_chaves_idx` (`palavra_chave_id`);

--
-- Índices de tabela `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cidades_estados_idx` (`estado_id`);

--
-- Índices de tabela `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contratos_terceirizadas_idx` (`terceirizada_id`),
  ADD KEY `fk_contratos_chamados_idx` (`chamado_id`);

--
-- Índices de tabela `equipes`
--
ALTER TABLE `equipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipes_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `equipes_propria`
--
ALTER TABLE `equipes_propria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipes_propria_chamados_idx` (`chamado_id`),
  ADD KEY `fk_equipes_propria_equipes_idx` (`equipe_id`);

--
-- Índices de tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `itens_chamado`
--
ALTER TABLE `itens_chamado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_itens_realizados_chamados_idx` (`chamado_id`);

--
-- Índices de tabela `ocorrencias`
--
ALTER TABLE `ocorrencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ocorrencias_chamados_idx` (`chamado_id`),
  ADD KEY `fk_ocorrencias_palavras_chaves_idx` (`palavra_chave_id`),
  ADD KEY `fk_ocorrencias_proprios_idx` (`proprio_id`);

--
-- Índices de tabela `palavras_chave`
--
ALTER TABLE `palavras_chave`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `proprios`
--
ALTER TABLE `proprios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proprios_cidades_idx` (`cidade_id`),
  ADD KEY `fk_proprios_usuarios_idx` (`usuario_id`),
  ADD KEY `fk_proprios_secretarias_idx` (`secretaria_id`);

--
-- Índices de tabela `secretarias`
--
ALTER TABLE `secretarias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_secretarias_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `terceirizadas`
--
ALTER TABLE `terceirizadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_terceirizadas_usuarios_idx` (`usuario_id`);

--
-- Índices de tabela `trabalhadores`
--
ALTER TABLE `trabalhadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_trabalhadores_equipes_idx` (`equipe_id`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contratos`
--
ALTER TABLE `contratos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `equipes`
--
ALTER TABLE `equipes`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `equipes_propria`
--
ALTER TABLE `equipes_propria`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `itens_chamado`
--
ALTER TABLE `itens_chamado`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `ocorrencias`
--
ALTER TABLE `ocorrencias`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `palavras_chave`
--
ALTER TABLE `palavras_chave`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `proprios`
--
ALTER TABLE `proprios`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `secretarias`
--
ALTER TABLE `secretarias`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `terceirizadas`
--
ALTER TABLE `terceirizadas`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `trabalhadores`
--
ALTER TABLE `trabalhadores`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `chamados`
--
ALTER TABLE `chamados`
  ADD CONSTRAINT `fk_chamados_palavras_chaves` FOREIGN KEY (`palavra_chave_id`) REFERENCES `palavras_chave` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_chamados_proprios` FOREIGN KEY (`proprio_id`) REFERENCES `proprios` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Restrições para tabelas `cidades`
--
ALTER TABLE `cidades`
  ADD CONSTRAINT `fk_cidades_estados` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `fk_contratos_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos_resp_terc` FOREIGN KEY (`terceirizada_id`) REFERENCES `terceirizadas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `equipes`
--
ALTER TABLE `equipes`
  ADD CONSTRAINT `fk_equipes_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `equipes_propria`
--
ALTER TABLE `equipes_propria`
  ADD CONSTRAINT `fk_equipes_propria_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipes_propria_equipes` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `itens_chamado`
--
ALTER TABLE `itens_chamado`
  ADD CONSTRAINT `fk_itens_chamado_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `ocorrencias`
--
ALTER TABLE `ocorrencias`
  ADD CONSTRAINT `fk_ocorrencias_chamados` FOREIGN KEY (`chamado_id`) REFERENCES `chamados` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ocorrencias_palavras_chaves` FOREIGN KEY (`palavra_chave_id`) REFERENCES `palavras_chave` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ocorrencias_proprios` FOREIGN KEY (`proprio_id`) REFERENCES `proprios` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Restrições para tabelas `proprios`
--
ALTER TABLE `proprios`
  ADD CONSTRAINT `fk_proprios_cidades` FOREIGN KEY (`cidade_id`) REFERENCES `cidades` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proprios_secretarias` FOREIGN KEY (`secretaria_id`) REFERENCES `secretarias` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_proprios_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `secretarias`
--
ALTER TABLE `secretarias`
  ADD CONSTRAINT `fk_secretarias_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `terceirizadas`
--
ALTER TABLE `terceirizadas`
  ADD CONSTRAINT `fk_terceirizadas_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Restrições para tabelas `trabalhadores`
--
ALTER TABLE `trabalhadores`
  ADD CONSTRAINT `fk_trabalhadores_equipes` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
