-- Classe Usuarios
 
-- adicionarMunicipe()
-- O municipe não será um usuário do sistema
-- Temos 5 tipos de usuários
    
-- -- adicionarAdministrador()
-- INSERT INTO usuarios (nome, cpf, email, ddd, telefone, tipo, senha)
-- VALUES ('José da Silva', 'CPF Criptografado', 'jose.silva@gmail.com', '15', '998305462', 1, 'Senha Criptografada');

-- bloquearUsuario()
-- Como vai ser pra bloquear um usuário?

-- -- entrar(email, senha)
-- SELECT *
-- FROM usuarios
-- WHERE email = 'jose.silva@gmail.com' AND senha = 'Senha Criptografada';

-- solicitarTrocaDeSenha()
-- UPDATE usuarios
-- SET senha = 'Nova Senha Criptografada'
-- WHERE senha = 'Senha Criptografada' AND id = 11;

-- ocultar()

-- -- listarTudo(limite, inicio, fim)
-- -- Sem parâmetros:
-- SELECT *
-- FROM usuarios;

-- -- Com limite:
-- SELECT *
-- FROM usuarios
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM usuarios
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- listar(id)
-- SELECT *
-- FROM usuarios
-- WHERE id = 11;

-- Possíveis métodos para CRUD:
-- adicionar(usuario)
-- Igual ao adicionarAdministrador() só que muda o tipo de usuário conforme escolha.

-- atualizar(usuario)
-- UPDATE usuarios
-- SET nome = 'Ronaldo Cravo Correia', cpf = 'CPF Criptografado', email = 'ronaldo.correia@uol.com.br', ddd = '15', telefone = '996541020', tipo = 2
-- WHERE id = 16;

-- deletar(id)
-- DELETE FROM usuarios
-- WHERE id = 17;


-- Classe Secretaria

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM secretarias;

-- -- Com limite:
-- SELECT *
-- FROM secretarias
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM secretarias
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- adicionar(secretaria)
-- INSERT INTO secretarias (nome, abreviatura, usuario_id)
-- VALUES ('Secretaria da Educação', 'SECED', 12);

-- -- listar(id)
-- SELECT *
-- FROM secretarias
-- WHERE id = 4;

-- -- atualizar(secretaria)
-- UPDATE secretarias
-- SET nome = 'Secretaria de Educação', abreviatura = 'SECED', usuario_id = 12
-- WHERE id = 4;

-- -- deletar(secretaria)
-- DELETE FROM secretarias
-- WHERE id = 5





-- Classe Proprio

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM proprios;

-- -- Com limite:
-- SELECT *
-- FROM proprios
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM proprios
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- -- listar(proprio_id)
-- SELECT *
-- FROM proprios
-- WHERE id = 13;

-- listarPorSecretaria(id_secretaria)
-- SELECT *
-- FROM proprios
-- WHERE secretaria_id = 2;

-- listarPorSubGrupo(subgrupo)
-- Não tem mais

-- listarPorGrupo(grupo)
-- Não tem mais

-- adicionar()
-- INSERT INTO proprios (nome, endereco, numero, bairro, cep, horario_abert, horario_fech, ddd, telefone, latitude, longitude, pavimentos, data_construcao, area_const, validade_avcb, publico_alvo, benif_manha, benif_tarde, benif_noite, usuario_id, cidade_id, secretaria_id)
-- VALUES ('Praça Romero dos Canários Amarelos', 'Rua João Cristovão Campão', 's/n', 'Bairro Azul', '18078458', '00:00', '23:59', null, null, -23.474949, -47.448065, 0, '2006-09-16', 325.00, null, 'Todos os Municipes', 150, 30, 100, 11, 1, 2);

-- adicionarUsuario(usuario_id)
-- UPDATE proprios
-- SET usuario_id = 11
-- WHERE proprios.id = 6;

-- adicionarCategoriaDeSecretaria(cat_sec_id)
-- Agora foi trocado para só secretaria_id
-- UPDATE proprios
-- SET secretaria_id = 2
-- WHERE proprios.id = 6;

-- adicionarCidade(cidade_id)
-- UPDATE proprios
-- SET cidade_id = 1
-- WHERE proprios.id = 6;

-- ocultar(id)

-- Possíveis métodos para CRUD:
-- atualizar(proprio)
-- UPDATE proprios SET nome = :nome, endereco = :endereco, numero = :numero, bairro = :bairro, horario_abert = :horario_abert, horario_fech = :horario_fech, ddd = :ddd, telefone = :telefone, latitude = :latitude, longitude = :longitude, pavimentos = :pavimentos, data_construcao = :data_construcao, area_const = :area_const, validade_avcb = :validade_avcb, publico_alvo = :publico_alvo, benif_manha = :benif_manha, benif_tarde = :benif_tarde, benif_noite = :benif_noite, usuario_id = :usuario_id, cidade_id = :cidade_id, secretaria_id = :secretaria_id WHERE id = :p_id;

-- deletar(id)
-- DELETE FROM proprios
-- WHERE id = 15;




-- Classe Cidade

-- adicionar(estado_id)
-- INSERT INTO cidades (nome, estado_id)
-- VALUES ('Campinas', 1);

-- ocultar()

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM cidades;

-- -- Com limite:
-- SELECT *
-- FROM cidades
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM cidades
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- listar(id)
-- SELECT *
-- FROM cidades
-- WHERE id = 1;

-- atualizar(cidade)
-- UPDATE cidades
-- SET nome = 'Votorantim', estado_id = 1
-- WHERE id = 2;

-- deletar(id)
-- DELETE FROM cidades
-- WHERE id = 4;



-- Classe Estado

-- adicionar()
-- INSERT INTO estados (nome)
-- VALUES ('Rio de Janeiro');

-- ocultar()

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM estados;

-- -- Com limite:
-- SELECT *
-- FROM estados
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM estados
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- listar(id)
-- SELECT *
-- FROM estados
-- WHERE id = 1;

-- atualizar(estado)
-- UPDATE estados
-- SET nome = 'Mato Grosso'
-- WHERE id = 3;

-- deletar(id)
-- DELETE FROM estados
-- WHERE id = 4;




-- Classe Terceirizado

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM terceirizadas;

-- -- Com limite:
-- SELECT *
-- FROM terceirizadas
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM terceirizadas
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- adicionar(terceirizado)
-- INSERT INTO terceirizadas (nome, cnpj, ddd, telefone, email, usuario_id)
-- VALUES ('COMsTRUcom', 'CNPJ Criptogragado', '15', '30364545', 'relacoes@comstrucom.com.br', 15);

-- listar(id)
-- SELECT *
-- FROM terceirizadas
-- WHERE id = 3;

-- atualizar(terceirizado)
-- UPDATE terceirizadas
-- SET nome = 'COMsTRUcom S.A.', cnpj = 'CNPJ Criptografado', ddd = '15', telefone = '30364545', email = 'relacoes.cont@comstrucom.com.br', usuario_id = 15
-- WHERE id = 3;

-- deletar(id)
-- DELETE FROM terceirizadas
-- WHERE id = 4;




-- Classe Contrato

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM contratos;

-- -- Com limite:
-- SELECT *
-- FROM contratos
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM contratos
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- adicionar()
-- INSERT INTO contratos (data_contratacao, valor_principal, valor1, valor2, valor3, valor4, valor5, observacoes, chamado_id, terceirizada_id)
-- VALUES ('2018-09-05', 450.00, null, null, null, null, null, null, 'SMSERPO00001', 2);

-- Possíveis métodos para CRUD:
-- listar(id)
-- SELECT *
-- FROM contratos
-- WHERE id = 3;

-- atualizar(contrato)
-- UPDATE contratos
-- SET valor1 = 500.00, valor2 = null, valor3 = null, valor4 = null, valor5 = null, observacoes = 'Tivemos que trocar a fechadura do batente', chamado_id = 'SMSERPO00001', terceirizada_id = 2
-- WHERE id = 3;

-- deletar(contrato)
-- DELETE FROM contratos
-- WHERE id = 4;



-- Classe Item_chamado

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM itens_chamado;

-- -- Com limite:
-- SELECT *
-- FROM itens_chamado
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM itens_chamado
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- adicionar(item_chamado)
-- INSERT INTO itens_chamado (descricao, unidade, quantidade, observacao, chamado_id) VALUES (:descricao, :unidade, :qtd, :obs :c_id);

-- listar(id)
-- SELECT *
-- FROM itens_chamado
-- WHERE id = 5;

-- atualizar(terceirizado)
-- UPDATE itens_chamado SET descricao = :descricao, unidade = :unidade, quantidade = :qtd, observacao = :obs, chamado_id = :c_id WHERE id = :i_id;

-- deletar(id)
-- DELETE FROM itens_chamado WHERE id = :id;

-- Classe Chamado

-- abrir()
-- seria o adicionar?
-- INSERT INTO chamados (id, situacao, data_abertura, data_finalizacao, class_manut, periodicidade, atividade, observacoes, proprio_id, palavra_chave_id) VALUES (:c_id, :situacao, null, null, :class, :periodicidade, :atividade, :obs, :p_id, :pc_id);

-- adicionarProprio(proprio_id)
-- UPDATE chamados
-- SET proprio_id = 14
-- WHERE id = 'SMSECID00001';

-- adicionarPalavraChave(palavra_chave)
-- UPDATE chamados SET palavra_chave_id = :pc_id WHERE id = :c_id;

-- listarTudo(limite, inicio, fim) // talvez trocar por sigla
-- Sem parâmetros:
-- SELECT *
-- FROM chamados;

-- -- Com limite:
-- SELECT *
-- FROM chamados
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM chamados
-- WHERE id LIKE '%SERPO%'
-- LIMIT 20;

-- adicionarEquipePropria(equipe_propria_id)
-- INSERT INTO equipes_propria (data, observacoes, chamado_id, equipe_id)
-- VALUES ('2018-09-06', null, 'SMSECID00001', 5);

-- Possíveis métodos para CRUD:
-- listar(id)
-- SELECT *
-- FROM chamados
-- WHERE id = 'SMSECID00001';

-- atualizar(chamado)
-- UPDATE chamados SET situacao = :sit, data_abertura = :data_a, data_finalizacao = null, class_manut = :class, periodicidade = :peri, atividade = :atv , observacoes = :obs, proprio_id = :p_id, palavra_chave_id = :pc_id WHERE id = :id;

-- deletar(id)
-- DELETE FROM chamados WHERE id = :id;



-- Classe Equipe_propria

-- listarTudo(limite, inico, fim)
-- -- Sem parâmetros:
-- SELECT *
-- FROM equipes_propria;

-- -- Com limite:
-- SELECT *
-- FROM equipes_propria
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM equipes_propria
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- adicionar(equipe_propria)
-- INSERT INTO equipes_propria (`data`, observacoes, chamado_id, equipe_id) VALUES (:dat, :obs, :c_id, :e_id);

-- listar(id)
-- SELECT *
-- FROM equipes_propria
-- WHERE id = 1;

-- atualizar(equipe_propria)
-- UPDATE equipes_propria SET data = :dat, observacoes = :obs, chamado_id = :c_id, equipe_id = :e_id WHERE id = :ep_id;

-- deletar(id)
-- DELETE FROM equipes_propria WHERE id = :id;



-- Classe Equipe

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM equipes;

-- -- Com limite:
-- SELECT *
-- FROM equipes
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM equipes
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- listarPorSecretaria
-- SELECT eq.id, eq.nome, eq.usuario_id FROM equipes as eq, equipes_propria as eqp, chamados as c, proprios as p, secretarias as sec WHERE eqp.equipe_id = eq.id AND eqp.chamado_id = c.id AND c.proprio_id = p.id AND p.secretaria_id = sec.id AND sec.id = :id AND c.data_finalizacao IS NULL;

-- listar(equipe_id)
-- SELECT * FROM equipes WHERE id = :id;

-- listarTrabalhandoNoMomento()
-- SELECT eq.id, eq.nome, eq.usuario_id FROM equipes as eq, equipes_propria as eqp, chamados as c WHERE eqp.equipe_id = eq.id AND eqp.chamado_id = c.id AND c.data_finalizacao IS NULL;

-- Possíveis métodos para CRUD:
-- adicionar(equipe)
-- INSERT INTO equipes (nome, usuario_id) VALUES (:nome, :u_id);

-- atualizar(equipe)
-- UPDATE equipes SET nome = 'Equipe F', usuario_id = :u_id WHERE id = :e_id;

-- deletar(id)
-- DELETE FROM equipes WHERE id = :id;



-- Classe Trabalhador

-- alocarParaEquipe(trabalhador_id) // Acho que vai ter que informar o id da equipe também
-- UPDATE trabalhadores SET equipe_id = :e_id WHERE id = :t_id;

-- listarTudo(limite, inicio, fim)
-- -- Sem parâmetros:
-- SELECT *
-- FROM trabalhadores;

-- -- Com limite:
-- SELECT *
-- FROM trabalhadores
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM trabalhadores
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- Possíveis métodos para CRUD:
-- adicionar(trabalhador)
-- INSERT INTO trabalhadores (nome, cpf, ddd, telefone, equipe_id) VALUES (:nome, :cpf, :ddd, :telefone, :e_id);

-- listar(id)
-- SELECT * FROM trabalhadores WHERE id = :id;

-- atualizar(trabalhador)
-- UPDATE trabalhadores SET nome = :nome, cpf = :cpf, ddd = :ddd, telefone = :telefone, equipe_id = :e_id WHERE id = :t_id;

-- deletar(id)
-- DELETE FROM trabalhadores WHERE id = :id;



-- Classe Ocorrencia

-- adicionar()
-- INSERT INTO ocorrencias (situacao, cpf, descricao, foto, palavra_chave_id, chamado_id, proprio_id) VALUES (:situacao, :cpf, :descricao, :foto, :pc_id, :c_id, :p_id);

-- adicionarFoto()
-- UPDATE ocorrencias SET foto = :foto WHERE id = :id;

-- adicionarPalavraChave(paralavra_chave_id)
-- UPDATE ocorrencias SET palavra_chave_id = :pc_id WHERE id = :id;

-- adicionarProprio(proprio_id)
-- UPDATE ocorrencias SET proprio_id = :p_id WHERE id = :id;

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM ocorrencias;

-- Com limite:
-- SELECT *
-- FROM ocorrencias
-- LIMIT 20;

-- Com tudo:
-- SELECT *
-- FROM ocorrencias
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- listarPorSecretaria
-- SELECT o.id, o.situacao, o.cpf, o.descricao, o.foto, o.palavra_chave_id, o.chamado_id, o.proprio_id FROM ocorrencias as o, proprios as p, secretarias as s WHERE o.proprio_id = p.id AND p.secretaria_id = s.id AND s.id = :id;

-- listar(ocorrencia_id)
-- SELECT * FROM ocorrencias WHERE id = :id;

-- listarPorCPF(cpf)
SELECT *
FROM ocorrencias
WHERE cpf = 'CPF Criptografado';

-- gerarChamado()
-- Talvez incluir o chamado e depois...
-- UPDATE ocorrencias SET chamado_id = :c_id WHERE id = :o_id;

-- listarTudo()

-- Possíveis métodos para CRUD:
-- atualizar(ocorrencia)
-- UPDATE ocorrencias SET situacao = :situacao, cpf = :cpf, descricao = :descricao, palavra_chave_id = :pc_id, chamado_id = :c_id, proprio_id = :p_id WHERE id = :id;

-- deletar(id)
-- DELETE FROM ocorrencias WHERE id = :id;



-- Classe Blacklist

-- adicionar(cpf)
-- INSERT INTO blacklist (cpf)
-- VALUES ('CPF Criptografado');

-- listarTudo(limite, inicio, fim)
-- Sem parâmetros:
-- SELECT *
-- FROM blacklist;

-- -- Com limite:
-- SELECT *
-- FROM blacklist
-- LIMIT 20;

-- -- Com tudo:
-- SELECT *
-- FROM blacklist
-- WHERE id BETWEEN 1 AND 100
-- LIMIT 20;

-- remover(cpf)
-- Só deu pra remover pelo id no modo seguro do sgbd, entao talvez tenha que procurar antes pelo id
-- DELETE FROM blacklist
-- WHERE id = 2;

-- -- Ele até deleta assim, mas da uns erros:
-- DELETE FROM blacklist
-- WHERE id = (SELECT id FROM blacklist WHERE cpf = 'CPF Bloq Criptografado');

-- -- procurar(cpf)
-- SELECT *
-- FROM blacklist
-- WHERE cpf = 'CPF Criptografado';



-- Estatisticas
-- manutencoes feitas
-- SELECT COUNT(*)
-- FROM chamados
-- WHERE data_finalizacao IS NOT NULL;
-- tambem da pra usar o numero da situacao no lugar da data de finalizacao

-- manu nao feita
-- SELECT COUNT(*) FROM chamados WHERE data_finalizacao IS NULL;

-- manu total
-- SELECT COUNT(*) FROM chamados;

-- ocorrencia total
-- SELECT COUNT(*) FROM ocorrencias

-- ocorrencia falsas
-- SELECT COUNT(*) FROM ocorrencias WHERE situacao = 5;

-- ocorrencia atendidas
-- SELECT COUNT(*) FROM ocorrencias as o, chamados as c WHERE o.chamado_id = c.id AND data_finalizacao IS NOT NULL;

-- ocorrencia nao atendidas
-- SELECT COUNT(*) FROM ocorrencias as o, chamados as c WHERE o.chamado_id = c.id AND data_finalizacao IS NULL;


-- demandaAtendida()

-- acompanhamento()