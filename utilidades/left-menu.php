<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li>
                <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
            </li>
            <?php if($NDA <= 4){ ?>
            <li>
                <a href="mapa.php"><i class="fa fa-bus"></i> Mapa</a>
            </li>
            <?php } ?>
            <?php if($NDA <= 4){ ?>
            <li><a><i class="fa fa-edit"></i> Cadastros <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="novo_chamado.php">Chamados</a></li>
                  <li><a href="novo_proprio.php">Próprios</a></li>
                  <li><a href="nova_equipe.php">Equipe e Trabalhador</a></li>
                  <li><a href="novo_usuario.php">Usuários</a></li>
                  <li><a href="novo_contrato.php">Contratos</a></li>
                  <li><a href="nova_secretaria.php">Secretarias</a></li>
<!--                   <li><a href="nova_cidade.php">Cidades</a></li> -->
                  <li><a href="novo_terceirizado.php">Terceirizado</a></li>
                </ul>
              </li>
            <?php } ?>
            <?php if($NDA <= 4){ ?>
            <li>
                <a href="ocorrencias.php"><i class="fa fa-pencil"></i> Ocorrencias</a>
            </li>
            <?php } ?>
            <?php if($NDA <= 2){ ?>
            <li>
                <a href="lista_chamados.php"><i class="fa fa-list-ol"></i> Lista de chamados </a>
            </li>
            <?php } ?>
            <?php if($NDA <= 2){ ?>
            <li>
                <a href="lista_proprios.php"><i class="fa fa-building-o"></i> Lista de próprios </a>
            </li>
            <?php } ?>
            <?php if($NDA <= 4){ ?>
            <li>
                <a href="equipe_obras.php"><i class="fa fa-group"></i> Equipe de obras </a>
            </li>
            <?php } ?>
            <?php if($NDA <= 2){ ?>
            <li>
                <a href="est2.php"><i class="fa fa-bar-chart-o"></i> Estatisticas </a>
            </li>
            <?php } ?>
            <?php if($NDA <= 1){ ?>
            <li>
                <a href="usuarios.php"><i class="fa fa-edit"></i>Usuários</a>
            </li>
            <?php } ?>
            <?php if($NDA <= 2){ ?>
            <li>
                <a href="contratos.php"><i class="fa fa-file"></i>Contratos</a>
            </li>
            <?php } ?>
            <?php if($NDA <= 2){ ?>
            <li>
                <a href="secretarias.php"><i class="fa fa-university"></i>Secretaria</a>
            </li>
            <?php } ?>
            <?php if($NDA <= 1){ ?>
<!--             <li>
                <a href="cidades.php"><i class="fa fa-taxi"></i>Cidades</a>
            </li>  -->
            <?php } ?>
            <?php if($NDA <= 5){ ?>
            <li>
                <a href="terceirizados.php"><i class="fa fa-users"></i>Terceirizado</a>
            </li> 
            <?php } ?>
        </ul>
    </div>
</div>