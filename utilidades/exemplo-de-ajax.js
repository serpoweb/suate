 var post = {
          type: 'admin-login',
          email: email,
          senha:senha
        };
        $.ajax({
          type: "post",
          url: "../../back/api/api.php",
          data: post,
          success: function (response) {
            console.log(response);
            var res = JSON.parse(response);
            if(res['error']){
              swal('ops',res['msg'], 'error');
              return;
            }else{
              swal({
                title: 'Sucesso',
                text: res['msg'],
                type: 'success',
                onClose: () =>{
                  location.reload();
                }
              });
            }
          },error: function (err){
            swal('Erro','Verifique sua conexão','error');
            return;
          }
        });