<?php


require("conn.php");

// Select all the rows in the markers table
$query = "SELECT 
proprios.nome, 
proprios.id,
proprios.endereco,
proprios.latitude,
proprios.longitude, 
proprios.numero,
proprios.secretaria_id,
secretarias.abreviatura
FROM proprios 
INNER JOIN secretarias 
ON secretarias.id = proprios.secretaria_id";

$result = mysqli_query($conn, $query);
if (!$result) {
    die('Invalid query: ');
}



while ($row = @mysqli_fetch_assoc($result)) {
    
    $proprios[] = array(
       
        'id' => $row['id'],
        'nome' => ($row['nome']),
        'endereco' => ($row['endereco']),
        'lat' => $row['latitude'],
        'long' => $row['longitude'],
        'numero' => $row['numero'],
        'sec_id' => $row['secretaria_id'],
        'abreviatura' => $row['abreviatura']
    );
       

    
}

echo json_encode($proprios);
mysqli_close($conn);

?>