<?php
require("conn.php");

$proprios = array();
$desc = $_GET["nome"];
// Seleciona proprios relacionados com a pesquisa no banco
$query = "SELECT 
proprios.nome, 
proprios.id,
proprios.endereco,
proprios.latitude,
proprios.longitude, 
proprios.numero,
proprios.secretaria_id,
secretarias.abreviatura
FROM proprios 
INNER JOIN secretarias 
ON secretarias.id = proprios.secretaria_id
WHERE proprios.nome LIKE '%$desc%' ";

//echo($query);


$result = mysqli_query($conn, $query);
if (!$result) {
    die('Invalid query: ');
}



while($row = @mysqli_fetch_assoc($result)) {
    
    $proprios[] = array(
       
        'id' => $row['id'],
        'nome' => ($row['nome']),
        'endereco' => ($row['endereco']),
        'lat' => $row['latitude'],
        'long' => $row['longitude'],
        'numero' => $row['numero'],
        'sec_id' => $row['secretaria_id'],
        'abreviatura' => $row['abreviatura']
    );
       

    
}


if(!$proprios){
    echo null;
}else{
echo json_encode($proprios);
}
mysqli_close($conn);

?>