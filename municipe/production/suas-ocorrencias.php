<?php require_once('../../back/api/api.php'); ?>
<!DOCTYPE html>
<html lang="pt">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SUATE Ocorrencias</title>
    <meta name="description" content="SUATE">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!--     <link href="../production/css/estilo.css" rel="stylesheet"> -->
   
    <?php require_once('../../utilidades/html-import.php');?>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <style>
    .table-dark {
        color: #fff;
        font-family: cursive;
        background-color: #1b232e;
    }
  </style>
</head>

<body style="background-color: #6c6c6c;">
  <div class="col-md-12 col-xd-12 col-sm-12 col-lg-12">
     <center>
        <h1 style="color: #ffffff; font-weight: 600; font-family: cursive; font-size: xx-large;">
          Ocorrências relatadas
         </h1>
     </center>
      <div class="col-md-8 col-sm-12 offset-md-2">
        <center>
          <form id="suas-ocorrencias" action="" method="post">
            <input id="cpf" name='cpf' type="text" placeholder="Digite aqui seu cpf" style="margin:10px;height: 50px; border-radius: 25px; padding: 0 30px 0 50px; display: inline-block; width: 300px; background: #e6e6e6; font-family: cursive; font-size: 15px; line-height: 1.5; color: #000; outline: none; border: none;">
            <input type="submit" value="Visualizar Ocorrencias" style="width: 200px%; height: 50px; border-radius: 25px; background: #1b232e; font-family: cursive; font-size: 15px; line-height: 1.5; color: #fff; display: -webkit-box; display: -webkit-flex; display: -moz-box; display: -ms-flexbox; display: inline-block; justify-content: center; align-items: center; padding: 0 25px; -webkit-transition: all 0.4s; -o-transition: all 0.4s; -moz-transition: all 0.4s; transition: all 0.4s; }">
          </form>
          <?php if(isset($_POST['cpf']) && $_POST['cpf'] != ""){ ?>
          <table class="table table-dark">
            <thead>
              <tr>
                <th scope="col">Descrição</th>
                <th scope="col">Próprio</th>
                <th scope="col">Situação</th>
                <th scope="col">Foto</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $cpf = $_POST["cpf"];
                $ocorrencia = new Ocorrencia();
                $proprio = new Proprio();
                $ocorrencias = $ocorrencia->listarPorCpf($cpf);
                $ocorrencias == false ? exit : null;
                for($i = 0, $tam = count($ocorrencias); $i < $tam; $i++){  
              ?>
              <tr>
                <th scope="row"><?= $ocorrencias[$i]['descricao']; ?></th>
                <td><?= $proprio->listar($ocorrencias[$i]['proprio_id'])['nome']; ?></td>
                <td><?= verSituacaoMunicipe($ocorrencias[$i]['situacao']); ?></td>
                <td><img width="50" src="../../uploads/<?= $ocorrencias[$i]['foto']; ?>"></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php } ?>
        </center>
      </div>
  </div>
   <center><button class="btn btn-primary" style="margin-bottom: 3%; margin-top: 20px; background-color: #1b232e; border-color: #fff; font-family: cursive; padding: 11px;" onclick="window.location.href='municipe.php';">
         Voltar ao mapa
       </button></center>
    
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>
      $("#suas-ocorrencias").submit((e)=>{
        var cpf = $("#cpf").val().trim();
        if(cpf.length == 0){
          swal('Atenção','Preencha seu CPF por favor','info');
          e.preventDefault();
          return;
        }
      });
    </script>
</body>
</html>