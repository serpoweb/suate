var map;

var markers = [];
var minZoomLevel = 3;
var styleMap = [

    {
        featureType: 'administrative.province',

        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'transit.station.bus',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'transit.station.airport',
        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'administrative.locality',

        stylers: [{
            visibility: 'off'
        }]
    },
    {
        featureType: 'poi',
        stylers: [{
            visibility: 'off'
        }]
    },
];


function CenterControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click para retornar ao padrão';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Centralizar';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function () {
        initMap();
    });

}

function initMap() {

    var Sorocaba = new google.maps.LatLng(-23.465000, -47.461012);


    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 12,
        minZoom: 12,
        center: Sorocaba,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        disableDefaultUI: true,
        styles: styleMap


    });

    // Create the DIV to hold the control and call the CenterControl()
    // constructor passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map);

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv)

    if (location.protocol == 'https:') {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setZoom(15);
                map.setCenter(pos);
                //console.log(pos);
            });
        }
    } else {
        //alert("Não foi possivel encontrar sua localização!");
    }






    $.getJSON('php/api_json.php', function (pontos) {

        console.log(pontos);
        $.each(pontos, function (index, ponto) {


            var marker = new google.maps.Marker({
                zIndex: parseInt(ponto.id),
                position: new google.maps.LatLng(ponto.lat, ponto.long),
                title: ponto.nome,
                clickable: true,
                icon: {
                    url: "makers/" + ponto.abreviatura + ".png",
                    scaledSize: new google.maps.Size(24, 32)
                },
                map: map

            });
            marker.addListener('click', function () {
                console.log("click ok" + marker.zIndex);

                document.getElementById("nome-proprio").textContent = marker.title;
                document.getElementById("nome-proprio").setAttribute("data-id", ponto.id);
                console.log(ponto.nome,ponto.id);
                document.getElementById("proprio-descricao").textContent = "Endereço: " + ponto.endereco;
                document.getElementById("numero-proprio").textContent = "Numero: " + ponto.numero;
                openProp();
            });
            markers.push(marker);
        });


    });



}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function buscarProprio() {
    var descricao = document.getElementById("txtbusca").value;
    console.log(descricao);

    $.getJSON('php/search_Proprios.php?nome=' + descricao, function (pontos) {
        console.log(pontos);

        //exclui os pontos atuais
        setMapOnAll(null);
        marker = [];

        //Adciona os novos pontos
        $.each(pontos, function (index, ponto) {

            var marker = new google.maps.Marker({
                zIndex: parseInt(ponto.id),
                position: new google.maps.LatLng(ponto.lat, ponto.long),
                title: ponto.nome,
                clickable: true,
                icon: {
                    url: "makers/" + ponto.abreviatura + ".png",
                    scaledSize: new google.maps.Size(24, 32)
                },
                map: map


            });

            marker.addListener('click', function () {
                console.log("click ok" + marker.zIndex);
                //document.getElementById("imagem").src = "makers/" + ponto.abreviatura + ".PNG";
                document.getElementById("nome-proprio").textContent = marker.title;
                document.getElementById("nome-proprio").setAttribute("data-id", ponto.id);
                document.getElementById("proprio-descricao").textContent = "Endereço: " + ponto.endereco;
                document.getElementById("numero-proprio").textContent = "Numero: " + ponto.numero;
                openProp();
            });
            markers.push(marker);
        });


    });




}