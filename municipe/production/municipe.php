<!DOCTYPE html>
<html lang="pt">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SUATE</title>
    <meta name="description" content="COLOCAR AQUI TEXT">


    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../production/css/estilo.css" rel="stylesheet">
    <style>
        #map{
            /*background-image: url("images/mapa.jpg");*/
            z-index: -1;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat, repeat;
            position: absolute;
            height: 1070px;
            top: 0;
            width: 100%;
        }   
        
    </style>
    <?php require_once('../../utilidades/html-import.php');?>
    <script>
        var palavraCahave = 0;
        function select(id,name){
            palavraCahave = id;
            document.getElementById('btnDropDown01').innerText = name;
        }
    </script>
</head>

<body>
    <div id="loader" style="display:none;position:fixed;width:100%;z-index:999999;height:100%;background:#fff;top:0;left:0;">
        <center>
            <img style="position:relative;margin-top:calc(15%);" src="../../loader.gif">
        </center>
    </div>
    <!--mapa-->
    <div class="menu" id="ativa">
        <img src="../../admin/production/images/logo.png" width="100%" style="padding:15px;box-shadow: .3px .3px .3px .3px grey;" onclick="closeNav();">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav();">Mapa</a>
        <a href="suas-ocorrencias.php">Ocorências Relatadas</a>
        <a href="ajuda.php">Ajuda</a>
        <a href="../../admin/production/login.php">Entrar</a>
    </div>
    <div id="main">
        <span style="font-size:30% ;cursor:pointer;" onclick="openNav();"><img src="../production/images/menu.png" class="icon"
                ></span>
        <input type="search" id ="txtbusca" placeholder="Buscar no Mapa..." >  
        <span  style="font-size:30% ;cursor:pointer;" onclick="buscarProprio()"> <img src="../production/images/busca.png" class="icon"
                ></span>
    </div>
    <div class="container" id="prop">
        <a href="#" onclick="closeProp();location.reload();" class="btn btn-default btn-danger" style="position: absolute;">X</a>
        <div class="foto row">
            <img src="../production/images/placeholder.jpg" width="100%" style="border-radius: 5px;">
        </div>
        <div class="row">
        <div class="col-xs-12 col-md-12">
                 <h1 id="nome-proprio"></h1>
                 <p id="proprio-descricao"></p>
                 <p id="numero-proprio"></p>
                 <a href="#" onclick="openCard()" class="btn btn-default btn-block">Relatar um Problema</a>
                 <br>
        </div>
        </div>
    </div>
    <div class="container problema" id="prob">
        <br>
        <a href="#" onclick="closeCard();"><img src="../production/images/ic_close_black_36dp.png" width="5%"></a>
        <h4>Relatar um Problema</h4>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="dropdown">
                    <div class="dropdown">

                        <button class="btn btn-default dropdown-toggle" type="button" id="btnDropDown01" data-toggle="dropdown"
                            style="width: 100%;">
                            Escolha uma categoria
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu" role="menu" aria-labelledby="btnDropDown01" style="width: 225px;" id="ocorrencia_palavra_chave">
                            <?php require_once('../../back/classes/Database.php'); ?>
                            <?php require_once('../../back/classes/Proprio.php'); ?>
                            <?php $p = new Proprio(); $pc = $p->listarTodasPalavrasChave(); ?>
                            <?php for($i=0;$i<count($pc);$i++){ ?>
                            <li role="presentation" style="position:relative;width:100%;">
                                <a onclick="select(<?= $pc[$i]['id']; ?>,'<?= $pc[$i]['descricao']; ?>');"><?= $pc[$i]['descricao']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-xs-12 col-md-12">
                <div class="input-group" style="width: 100%; margin-top: 5%;">
                    <input type="text" class="form-control" placeholder="Descreva o Problema..." required id="ocorrencia_descricao" />
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="input-group" style="width: 100%; margin-top: 5%;display: table;">
                    <input type="file" id="ocorrencia_foto" class="form-control" accept="image/*" placeholder="Anexar uma Foto..." required />
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="input-group" style="width: 100%; margin-top: 5%;">
                    <input id="ocorrencia_cpf" type="text" class="form-control" placeholder="Insira seu CPF..." required/>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <br>
                <div class="btn-group" style=" width: 100%; text-align: center;">
                    <button class="btn btn-default btn-block btn-danger" id="ocorrencia">
                        Enviar Problema
                    </button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <br>
    <div id="map" style="width=100% !important;"> </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdyPhBNG8vxfr5lFv_AT0lwKslCVIDNws&callback=initMap"
        async defer></script>
    <script src="js/mapa.js"></script>
    <script src="js/controleCard.js"></script>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>
        $(function () {
            $("#ocorrencia").click(function (e) { 
                e.preventDefault();
                if(palavraCahave == 0){
                    swal('atenção','Preencha todos os campos','info');
                    return;
                }
                var descricao = $("#ocorrencia_descricao").val();
                var fatos = $("#ocorrencia_foto").prop('files');
                var cpf = $("#ocorrencia_cpf").val();
                if(!fatos[0] || descricao.trim().length == 0 || cpf.trim().length == 0){
                    swal('atenção','Preencha todos os campos','info');
                    return;
                }
                var formData = new FormData();
                formData.append("descricao",descricao);
                
                formData.append("foto",fatos[0]);
                formData.append("cpf",cpf);
                formData.append("situacao",1);
                formData.append("palavra_chave_id",palavraCahave);
                formData.append("type","adicionar-ocorrencia");
                formData.append("proprio_id",$("#nome-proprio").attr('data-id'));
                
                 $.ajax({
                    type:'POST',
                    url: '../../back/api/api.php',
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        $("#loader").show();
                    },
                    success:function(data){
                        $("#loader").hide();
                        console.log("success");
                        data = JSON.parse(data);
                        if(data.error){
                            swal('Erro',data.msg,'error');
                            return;
                        }else{
                            swal({
                                title:'Sucesso',
                                text: data.msg,
                                type: 'success',
                                onClose: ()=>{
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function(data){
                        console.log("error");
                        console.log(data);
                    }
                });
            });
        });
    </script>
</body>
</html>