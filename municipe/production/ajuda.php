<!DOCTYPE html>
<html lang="pt">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SUATE AJUDA</title>
    <meta name="description" content="SUATE">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!--     <link href="../production/css/estilo.css" rel="stylesheet"> -->
   
    <?php require_once('../../utilidades/html-import.php');?>
</head>

<body style="background-color: #6c6c6c;">
  <div class="col-md-12 col-xd-12 col-sm-12 col-lg-12">
     <center>
        <h1 style="color: #ffffff; font-weight: 600; font-family: cursive; font-size: xx-large;">
          Guia SUATE
         </h1>
     </center>
      <img src="images/help-municipe1.jpg" style="width:90%;position:relative;margin-left:5%;margin-right:5%;margin-top:1%;margin-bottom:1%;">
    <img src="images/help-municipe-2.jpg" style="width:90%;position:relative;margin-left:5%;margin-right:5%;margin-top:1%;margin-bottom:1%;">
      <center><button class="btn btn-primary" style="margin-bottom:3%;background-color: #1b232e; border-color: #fff; font-family: cursive; padding: 11px;" onclick="window.location.href='municipe.php';">
         Voltar ao mapa
       </button></center>
  </div>
    
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
   
</body>
</html>